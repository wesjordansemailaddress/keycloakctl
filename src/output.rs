use keycloak_client::models::{ClientRepresentation, GroupRepresentation, ClientScopeRepresentation, RoleRepresentation};
use std::str::FromStr;
use crate::resource::{Resource, ResourceKind, ResourceSpec};

#[derive(Debug, Copy, Clone)]
pub enum OutputFormat {
    YAML,
    JSON,
    Short,
    Long,
    Id,
}

impl FromStr for OutputFormat {
    type Err = &'static str;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use OutputFormat::{*};
        match s.to_lowercase().as_str() {
            "yaml" => Ok(YAML),
            "json" => Ok(JSON),
            "short" => Ok(Short),
            "long" => Ok(Long),
            "id" => Ok(Id),
            _ => Err("Not an OutputFormat. Must be one of (yaml, json, short, long).")
        }
    }
}

fn display_opt_bool(opt: &Option<bool>) -> &'static str {
    match opt {
        Some(true) => "yes",
        Some(false) => "no",
        None => "<unknown>"
    }
}

fn display_opt_str<'a>(opt: &'a Option<String>, def: &'a str) -> &'a str {
    opt.as_deref().unwrap_or(def)
}

macro_rules! force {
    ($it:expr, $variant:path) => {
        if let $variant(zzzzz) = $it { zzzzz } else { unreachable!() };
    }
}

fn dump_yaml<S: Into<Resource>>(items: S) {
    let resources = items.into();
    println!("{}", serde_yaml::to_string(resources.try_view_flatten()).expect("BUG encoding yaml"))
}

fn dump_json<S: Into<Resource>>(items: S) {
    let resources = items.into();
    println!("{}", serde_json::to_string(resources.try_view_flatten()).expect("BUG encoding json"))
}

pub fn output_clients(_: &str, items: Vec<ClientRepresentation>, of: OutputFormat) {
    match of {
        OutputFormat::YAML => dump_yaml(items),
        OutputFormat::JSON => dump_json(items),
        OutputFormat::Short | OutputFormat::Long => {
            println!(
                "{0: <30} {1: <20} {2: <10} {3: <10}",
                "CLIENT ID", "PROTOCOL", "ENABLED", "ID"
            );
            for client in items {
                println!("{0: <30} {1: <20} {2: <10} {3: <10}",
                         display_opt_str(&client.client_id, "<none>"),
                         display_opt_str(&client.protocol, "<none>"),
                         display_opt_bool(&client.enabled),
                         display_opt_str(&client.id, "<none>"));
            }
        }
        OutputFormat::Id => {
            for item in items {
                println!("{}", display_opt_str(&item.id, "<none>"))
            }
        }
    }
}

pub fn output_groups(realm: &str, items: Vec<GroupRepresentation>, of: OutputFormat) {
    match of {
        OutputFormat::Short | OutputFormat::Long => {
            println!(
                "{0: <30} {1: <20} {2: <20}",
                "NAME", "PATH", "ID"
            );
            for group in items {
                println!("{0: <30} {1: <20} {2: <20}",
                         display_opt_str(&group.name, "<none>"),
                         display_opt_str(&group.path, "<none>"),
                         display_opt_str(&group.id, "<none>"));
            }
        },
        OutputFormat::YAML => dump_yaml(items),
        OutputFormat::JSON => dump_json(items),
        OutputFormat::Id => {
            for item in items {
                println!("{}", display_opt_str(&item.id, "<none>"))
            }
        }
    }
}

pub fn output_client_scopes(_: &str, items: Vec<ClientScopeRepresentation>, of: OutputFormat) {
    match of {
        OutputFormat::YAML => dump_yaml(items),
        OutputFormat::JSON => dump_json(items),
        OutputFormat::Short | OutputFormat::Long => {
            println!(
                "{0: <30} {1: <20} {2: <20}",
                "NAME", "PROTOCOL", "ID"
            );
            for scope in items {
                println!("{0: <30} {1: <20} {2: <20}",
                         display_opt_str(&scope.name, "<none>"),
                         display_opt_str(&scope.protocol, "<none>"),
                         display_opt_str(&scope.id, "<none>"));
            }
        },
        OutputFormat::Id => {
            for item in items {
                println!("{}", display_opt_str(&item.id, "<none>"))
            }
        }
    }
}

pub fn output_roles(_: &str, items: Vec<RoleRepresentation>, of: OutputFormat) {
    match of {
        OutputFormat::YAML => dump_yaml(items),
        OutputFormat::JSON => dump_json(items),
        OutputFormat::Short | OutputFormat::Long => {
            println!(
                "{0: <30} {1: <11} {2: <10} {3: <20}",
                "NAME", "CLIENT ROLE", "COMPOSITE", "ID"
            );
            for role in items {
                println!("{0: <30} {1: <11} {2: <10} {3: <20}",
                         display_opt_str(&role.name, "<none>"),
                         display_opt_bool(&role.client_role),
                         display_opt_bool(&role.composite),
                         display_opt_str(&role.id, "<none>"));
            }
        },
        OutputFormat::Id => {
            for item in items {
                println!("{}", display_opt_str(&item.id, "<none>"))
            }
        }
    }
}