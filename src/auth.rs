use crate::config::KeycloakConfig;

use serde::{Serialize, Deserialize};

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Authorization {
    pub access_token: String,
    pub expires_in: i64,
    pub refresh_expires_in: i64,
    pub refresh_token: String,
    pub token_type: String,
    #[serde(rename = "not-before-policy")]
    pub not_before_policy: i64,
    pub session_state: String,
    pub scope: String,
}

impl Authorization {
    pub fn bearer_token(&self) -> String {
        format!("Bearer {}", self.access_token)
    }
}

pub fn keycloak_login(config: &KeycloakConfig) -> Authorization {
    let realm = config.server.realm.as_deref().unwrap_or("master");
    let client = config.server.client.as_deref().unwrap_or("admin-cli");

    let login_url = format!("{}/realms/{}/protocol/openid-connect/token", &config.server.server, realm);
    debug!("{}", login_url);

    let params = [("username", config.user.user.username.as_str()),
        ("password", config.user.user.password.as_str()),
        ("grant_type", "password"),
        ("client_id", client)];

    debug!("{:#?}", &params);

    let mut login = reqwest::Client::new()
        .post(&login_url)
        .form(&params)
        .send().expect("Failed to send login request.");

    match login.status() {
        reqwest::StatusCode::OK => {
            let access_token = login.json().expect("response could not parse");
            access_token
        }
        _ => {
            panic!("Login failed. (HTTP {})")
        }
    }
}