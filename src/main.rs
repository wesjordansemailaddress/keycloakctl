#[macro_use] extern crate log;
#[macro_use] extern crate env_logger;

mod auth;
mod cli;
mod config;
mod output;
mod resource;

use clap::Clap;

use keycloak_client::apis::configuration::{Configuration, ApiKey, BasicAuth};
use keycloak_client::apis::client::APIClient;

use crate::config::KeycloakConfig;

use cli::{Opts, Command};
use crate::resource::{ResourceKind, Resource, ResourceSpec};
use crate::output::OutputFormat;
use std::collections::HashMap;
use keycloak_client::models::{RoleRepresentation, ClientRepresentation};
use crate::cli::{GetOpts, CreateOpts};
use std::io::BufReader;
use std::fs::File;


fn main() {
    env_logger::init();

    let opts: cli::Opts = cli::Opts::parse();
    debug!("{:#?}", opts);

    let config = KeycloakConfig::from_opts(&opts).expect("Failed to parse config.");
    let auth = auth::keycloak_login(&config);

    let mut client_config = Configuration::new();
    client_config.base_path = format!("{}/admin/realms", config.server.server);
    client_config.bearer_access_token = Some(auth.access_token);
    client_config.user_agent = Some("keycloakctl/1/rust".to_owned());

    let keycloak = APIClient::new(client_config);

    match opts.cmd {
        Command::Get(ref get_opts) =>
            get_resource(&keycloak, &opts, get_opts),
        Command::Create(ref create_opts) =>
            create_resource(&keycloak, &opts, create_opts)
    }
}


fn get_resource(keycloak: &APIClient, opts: &Opts, get_opts: &GetOpts) {
    let id = get_opts.id.as_deref();
    let params = cli::extra_flags(&get_opts.params);
    let realm = opts.realm.as_str();
    let of = get_opts.output;

    info!("{:?}", params);
    match get_opts.kind {
        ResourceKind::Client => {
            let search = params.get("search");
            let clients = keycloak.clients_api()
                .realm_clients_get(realm, id, None, None, None, None)
                .expect("can't get clients");

            output::output_clients(realm, clients, of);
        }
        ResourceKind::Group => {
            let groups = if let Some(id) = id {
                let group = keycloak.groups_api()
                    .realm_groups_id_get(realm, id)
                    .expect("Can't get group");
                vec![group]
            } else {
                keycloak.groups_api()
                    .realm_groups_get(realm, None, None, None, None)
                    .expect("can't get groups")
            };

            output::output_groups(realm, groups, of);
        }
        ResourceKind::ClientScope => {
            let scopes = keycloak.client_scopes_api()
                .realm_client_scopes_get(realm)
                .expect("can't get client scopes");

            output::output_client_scopes(realm, scopes, of);
        }
        ResourceKind::Role => {
            let roles = if let Some(client_id) = params.get("client") {
                keycloak.roles_api()
                    .realm_clients_id_roles_get(realm, client_id, Some(false), None, None, None)
            } else {
                keycloak.roles_api()
                    .realm_roles_get(realm, Some(false), None, None, None)
            }.expect("can't get roles");

            let roles = serde_json::from_value(roles).expect("Can't read roles");
            output::output_roles(realm, roles, of);
        }
        _ => {
            eprintln!("Not implemented yet.");
        }
    }
}

fn create_resource(keycloak: &APIClient, opts: &Opts, create_opts: &CreateOpts) {
    let realm = opts.realm.as_str();
    let resources = if let Some(ref files) = create_opts.file {
        files.iter().map(|path| {
            debug!("Reading manifest file from '{}'", path);
            let docs = std::fs::read_to_string(path).expect("Could not open file");
            debug!("Read manifest ok:\n{}", docs);
            docs.split("---\n")
                .map(|doc| {
                    debug!("Reading doc: \n`{}`", doc);
                    let r = serde_yaml::from_str(doc).expect("Could not parse YAML manifest");
                    debug!("yaml OK");
                    r
                })
                .collect::<Vec<Resource>>()
        }).flatten().collect::<Vec<Resource>>()
    } else if let Some(ref jsons) = create_opts.json {
        jsons.iter()
            .map(|json| {
                debug!("Reading manifest file from command: {}", json);
                serde_json::from_str(json.as_str()).expect("Failed to parse command-line json")
            })
            .collect::<Vec<Resource>>()
    } else {
        panic!("Stdin reading not implemented yet. Use -f or -j.")
    };

    debug!("{:#?}", resources);

    for resource in resources {
        match resource.spec {
            ResourceSpec::Client(client) => {
                keycloak.clients_api().realm_clients_post(realm, client)
                    .expect("Failed to create Client");
                eprintln!("Client created.");
            }
            ResourceSpec::Role(role) => {
                keycloak.roles_api().realm_roles_post(realm, role)
                    .expect("Failed to create Role");
                eprintln!("Role created.");
            }
            _ => {
                eprintln!("Not implemented yet.");
            }
        }
    }
}
