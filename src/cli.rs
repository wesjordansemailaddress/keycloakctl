use clap::{Clap,Subcommand};

use crate::resource::{ResourceKind, Resource, ResourceSpec};
use std::str::FromStr;
use crate::output::OutputFormat;
use std::collections::HashMap;

#[derive(Clap,Debug)]
#[clap(name = "keycloakctl", version = "0.1", author = "Wes Jordan <wes@wesj.org>", about = "A kubectl-like interface for Keycloak.")]
pub struct Opts {
    #[clap(short, long = "config", env = "KEYCLOAK_CONFIG")]
    pub config_path: Option<String>,
    #[clap(default_value = "master", short, long, env = "KEYCLOAK_REALM")]
    pub realm: String,
    #[clap(subcommand)]
    pub cmd: Command,
}

#[derive(Clap,Debug)]
pub enum Command {
    Get(GetOpts),
    Create(CreateOpts),
    //Delete {},
    //Edit {},
    //Apply {}
}

#[derive(Clap, Debug)]
pub struct GetOpts {
    pub kind: ResourceKind,
    pub id: Option<String>,
    #[clap(default_value="short", short, long)]
    pub output: OutputFormat,
    #[clap(multiple=true, short, long)]
    pub params: Vec<String>
}

#[derive(Clap,Debug)]
pub struct CreateOpts {
    #[clap(multiple=true, short, long)]
    pub file: Option<Vec<String>>,
    #[clap(multiple=true, short, long)]
    pub json: Option<Vec<String>>,
    #[clap(short, long)]
    pub idempotent: bool
}

pub fn extra_flags(params: &Vec<String>) -> HashMap<String, String> {
     params.iter().map(|p| {
        let mut ps = p.split('=');
        let key = ps.next().expect("Could not parse param");
        let value = ps.next().expect("Could not parse param");
        (key.to_owned(), value.to_owned())
    }).collect()
}

/*
impl OutputFormat {
    pub fn output(&self, res: &Resource) {
        use OutputFormat::{*};
        use ResourceKind::{*};
        match self {
            YAML => { println!("{}", serde_yaml::to_string(res).unwrap()); }
            JSON => { println!("{}", serde_json::to_string(res).unwrap()); }
            Short | Long => {
                match res.rep.items() {
                    Some(items) => {
                        if items.is_empty() {
                            println!("Empty set");
                            return
                        }
                        let internal_kind = items.first().expect("BUG: Resource list shouldn't be empty!").kind;
                        match internal_kind {
                            Client => {
                                println!(
                                    "{0: <30} {1: <20} {2: <10} {3: <10}",
                                    "CLIENT ID", "PROTOCOL", "ENABLED", "ID"
                                );
                                for res in items {
                                    let client = force!(&res.rep, ResourceRep::Client);//if let ResourceRep::Client(client) = &res.rep { client } else { unreachable!() };
                                    println!("{0: <30} {1: <20} {2: <10} {3: <10}",
                                             display_opt_str(&client.client_id, "<none>"),
                                             display_opt_str(&client.protocol, "<none>"),
                                             display_opt_bool(&client.enabled),
                                             display_opt_str(&client.id, "<none>"));
                                }
                            }
                            ClientScope => {
                                println!(
                                    "{0: <30} {1: <20} {2: <20}",
                                    "NAME", "PROTOCOL", "ID"
                                );
                                for res in items {
                                    let scope = force!(&res.rep, ResourceRep::ClientScope);
                                    println!("{0: <30} {1: <20} {2: <20}",
                                             display_opt_str(&scope.name, "<none>"),
                                             display_opt_str(&scope.protocol, "<none>"),
                                             display_opt_str(&scope.id, "<none>"));
                                }
                            }
                            Group => {
                                println!(
                                    "{0: <30} {1: <20} {2: <20}",
                                    "NAME", "PATH", "ID"
                                );
                                for res in items {
                                    let group = force!(&res.rep, ResourceRep::Group);
                                    println!("{0: <30} {1: <20} {2: <20}",
                                             display_opt_str(&group.name, "<none>"),
                                             display_opt_str(&group.path, "<none>"),
                                             display_opt_str(&group.id, "<none>"));
                                }
                            }
                            IdentityProvider => {}
                            Role => {
                                println!(
                                    "{0: <30} {1: <11} {2: <10} {3: <20}",
                                    "NAME", "CLIENT ROLE", "COMPOSITE", "ID"
                                );
                                for res in items {
                                    let role = force!(&res.rep, ResourceRep::Role);
                                    println!("{0: <30} {1: <11} {2: <10} {3: <20}",
                                             display_opt_str(&role.name, "<none>"),
                                             display_opt_bool(&role.client_role),
                                             display_opt_bool(&role.composite),
                                             display_opt_str(&role.id, "<none>"));
                                }
                            }
                            User => {
                                println!(
                                    "{0: <20} {1: <20} {2: <10} {3: <20}",
                                    "USERNAME", "EMAIL", "ORIGIN", "ID"
                                );
                                for res in items {
                                    let user = force!(&res.rep, ResourceRep::User);
                                    println!("{0: <20} {1: <20} {2: <10} {3: <20}",
                                             display_opt_str(&user.username, "<none>"),
                                             display_opt_str(&user.email, "<none>"),
                                             display_opt_str(&user.origin, "<none>"),
                                             display_opt_str(&user.id, "<none>"));
                                }
                            },
                            Realm => {
                                println!(
                                    "{0: <10} {1: <8} {2: <10}",
                                    "ID", "ENABLED", "DISPLAY NAME"
                                );
                                for res in items {
                                    let realm = force!(&res.rep, ResourceRep::Realm);
                                    println!("{0: <10} {1: <8} {2: <10}",
                                             display_opt_str(&realm.id, "<none>"),
                                             display_opt_bool(&realm.enabled),
                                             display_opt_str(&realm.display_name, "<none>"));
                                }
                            }
                            List => { unreachable!() }
                        }
                    }
                    None => {
                        println!("{}", res.id.as_deref().expect("BUG: Resource id missing!"))
                    }
                }
            }
        }
    }
}*/