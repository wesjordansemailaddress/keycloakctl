use serde::{Serialize,Deserialize};
use std::collections::HashMap;

use super::scope::Scope;

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ClientScope {
    pub attributes: Option<HashMap<String, String>>,
    pub description: Option<String>,
    pub id: Option<String>,
    pub name: Option<String>,
    pub protocol: Option<String>,
    pub protocol_mappers: Option<Vec<ProtocolMapper>>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Client {
    pub access: Option<HashMap<String, bool>>,
    pub admin_url: Option<String>,
    pub always_display_in_console: Option<bool>,
    pub attributes: Option<HashMap<String, String>>,
    pub authentication_flow_binding_overrides: Option<HashMap<String, String>>,
    pub authorization_services_enabled: Option<bool>,
    pub authorization_services: Option<ResourceServer>,
    pub base_url: Option<String>,
    pub bearer_only: Option<bool>,
    pub client_authenticator_type: Option<String>,
    pub client_id: Option<String>,
    pub consent_required: Option<bool>,
    pub default_client_scopes: Option<Vec<String>>,
    pub default_roles: Option<Vec<String>>,
    pub description: Option<String>,
    pub direct_access_grants_enabled: Option<bool>,
    pub enabled: Option<bool>,
    pub frontchannel_logout: Option<bool>,
    pub full_scope_allowed: Option<bool>,
    pub id: Option<String>,
    pub implicit_flow_enabled: Option<bool>,
    pub name: Option<String>,
    pub node_re_registration_timeout: Option<i32>,
    pub not_before: Option<i32>,
    pub optional_client_scopes: Option<Vec<String>>,
    pub origin: Option<String>,
    pub protocol: Option<String>,
    pub protocol_mappers: Option<Vec<ProtocolMapper>>,
    pub public_client: Option<bool>,
    pub redirect_uris: Option<Vec<String>>,
    pub registered_nodes: Option<HashMap<String, String>>,
    pub registration_access_token: Option<String>,
    pub root_url: Option<String>,
    pub secret: Option<String>,
    pub service_accounts_enabled: Option<bool>,
    pub standard_flow_enabled: Option<bool>,
    pub surrogate_auth_required: Option<bool>,
    pub web_origins: Option<Vec<String>>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ProtocolMapper {
    pub config: Option<HashMap<String, String>>,
    pub id: Option<String>,
    pub name: Option<String>,
    pub protocol: Option<String>,
    pub protocol_mapper: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ResourceServer {
    pub allow_remote_resource_management: Option<bool>,
    pub client_id: Option<String>,
    pub decision_strategy: Option<DecisionStrategy>,
    pub id: Option<String>,
    pub name: Option<String>,
    pub policies: Option<Vec<Policy>>,
    pub policy_enforcement_mode: Option<PolicyEnforcementMode>,
    pub resources: Option<Vec<Resource>>,
    pub scopes: Option<Vec<Scope>>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Policy {
    pub config: Option<HashMap<String, String>>,
    pub decision_strategy: Option<DecisionStrategy>,
    pub description: Option<String>,
    pub id: Option<String>,
    pub logic: Option<PolicyLogic>,
    pub name: Option<String>,
    pub owner: Option<String>,
    pub policies: Option<Vec<String>>,
    pub resources: Option<Vec<String>>,
    pub resources_data: Option<Vec<Resource>>,
    pub scopes: Option<Vec<String>>,
    pub scopes_data: Option<Vec<Scope>>,
    #[serde(rename = "type")]
    pub policy_type: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Resource {
    pub id: Option<String>,
    pub attributes: Option<HashMap<String, String>>,
    pub display_name: Option<String>,
    pub icon_uri: Option<String>,
    pub name: Option<String>,
    pub owner_managed_access: Option<bool>,
    pub scopes: Option<Vec<Scope>>,
    #[serde(rename = "type")]
    pub resource_type: Option<String>,
    pub uris: Option<Vec<String>>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub enum DecisionStrategy {
    AFFIRMATIVE, UNANIMOUS, CONSENSUS
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub enum PolicyEnforcementMode {
    ENFORCING, PERMISSIVE, DISABLED
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub enum PolicyLogic {
    POSITIVE, NEGATIVE
}
