use serde::{Serialize,Deserialize};
use std::collections::HashMap;

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct IdentityProviderMapper {
    pub config: Option<HashMap<String, String>>,
    pub id: Option<String>,
    pub identity_provider_alias: Option<String>,
    pub identity_provider_mapper: Option<String>,
    pub name: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct IdentityProvider {
    pub add_read_token_role_on_create: Option<bool>,
    pub alias: Option<String>,
    pub config: Option<HashMap<String, String>>,
    pub display_name: Option<String>,
    pub enabled: Option<bool>,
    pub first_broker_login_flow_alias: Option<String>,
    pub internal_id: Option<String>,
    pub link_only: Option<bool>,
    pub post_broker_login_flow_alias: Option<String>,
    pub provider_id: Option<String>,
    pub store_token: Option<bool>,
    pub trust_email: Option<bool>,
}