use serde::{Serialize,Deserialize};
use std::collections::HashMap;

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Group {
    pub access: Option<HashMap<String, String>>,
    pub attributes: Option<HashMap<String, String>>,
    pub client_roles: Option<HashMap<String, Vec<String>>>,
    pub id: Option<String>,
    pub name: Option<String>,
    pub path: Option<String>,
    pub realm_roles: Option<Vec<String>>,
    pub sub_groups: Option<Vec<Group>>,
}