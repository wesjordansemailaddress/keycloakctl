use serde::{Serialize,Deserialize};
use super::client::{Policy, Resource};

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ScopeMapping {}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Scope {
    pub display_name: Option<String>,
    pub icon_uri: Option<String>,
    pub id: Option<String>,
    pub name: Option<String>,
    pub policies: Option<Vec<Policy>>,
    pub resources: Option<Vec<Resource>>,
}

