use serde::{Serialize,Deserialize};
use std::collections::HashMap;

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticationFlow {
    pub alias: Option<String>,
    pub authentication_executions: Option<Vec<AuthenticatorExecutionExport>>,
    pub built_in: Option<bool>,
    pub description: Option<String>,
    pub id: Option<String>,
    pub provider_id: Option<String>,
    pub top_level: Option<bool>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticatorConfig {
    pub alias: Option<String>,
    pub config: Option<HashMap<String, AuthenticatorConfigInfo>>,
    pub id: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticatorConfigInfo {
    pub help_text: Option<String>,
    pub name: Option<String>,
    pub properties: Option<Vec<ConfigProperty>>,
    pub provider_id: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticatorExecutionExport {
    pub authenticator: Option<String>,
    pub authenticator_config: Option<String>,
    pub authenticator_flow: Option<String>,
    pub flow_alias: Option<String>,
    pub priority: Option<i32>,
    pub requirement: Option<String>,
    pub user_setup_allowed: Option<bool>
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticationExecutionInfo {
    pub alias: Option<String>,
    pub authentication_config: Option<String>,
    pub authentication_flow: Option<String>,
    pub configurable: Option<bool>,
    pub display_name: Option<String>,
    pub flow_id: Option<String>,
    pub id: Option<String>,
    pub index: Option<i32>,
    pub level: Option<i32>,
    pub provider_id: Option<String>,
    pub requirement: Option<String>,
    pub requirement_choices: Option<Vec<String>>
}


#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct AuthenticationExecution {
    pub authenticator: Option<String>,
    pub authenticator_config: Option<String>,
    pub authenticator_flow: Option<bool>,
    pub flow_id: Option<String>,
    pub id: Option<String>,
    pub parent_flow: Option<String>,
    pub priority: Option<i32>,
    pub requirement: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct ConfigProperty {

}

