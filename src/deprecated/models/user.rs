use serde::{Serialize,Deserialize};
use std::collections::HashMap;

use super::federation::FederatedIdentity;

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct User {
    pub access: Option<HashMap<String, bool>>,
    pub attributes: Option<HashMap<String, String>>,
    pub client_consents: Option<Vec<UserConsent>>,
    pub client_roles: Option<HashMap<String, Vec<String>>>,
    pub created_timestamp: Option<i64>,
    pub credentials: Option<Vec<Credential>>,
    pub disableable_credential_types: Option<Vec<String>>,
    pub email: Option<String>,
    pub email_verified: Option<bool>,
    pub enabled: Option<bool>,
    pub federated_identities: Option<Vec<FederatedIdentity>>,
    pub federation_link: Option<Vec<String>>,
    pub first_name: Option<String>,
    pub groups: Option<Vec<String>>,
    pub id: Option<String>,
    pub last_name: Option<String>,
    pub not_before: Option<i32>,
    pub origin: Option<String>,
    pub realm_roles: Option<Vec<String>>,
    pub required_actions: Option<Vec<String>>,
    #[serde(rename = "self")]
    pub user_self : Option<String>,
    pub service_account_client_id: Option<String>,
    pub username: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct UserConsent {
    pub client_id: Option<String>,
    pub created_date: Option<i64>,
    pub granted_client_scopes: Option<Vec<String>>,
    pub last_updated_date: Option<i64>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Credential {
    pub created_date: Option<i64>,
    pub credential_data: Option<String>,
    pub id: Option<String>,
    pub priority: Option<i32>,
    pub secret_data: Option<String>,
    pub temporary: Option<bool>,
    #[serde(rename = "type")]
    pub credential_type: Option<String>,
    pub user_label: Option<String>,
    pub value: Option<String>,
}


