use serde::{Serialize,Deserialize};

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RequiredAction {}