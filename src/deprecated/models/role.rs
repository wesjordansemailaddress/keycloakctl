use std::collections::HashMap;
use serde::{Serialize,Deserialize};


#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RealmRoles {
    pub realm: Option<Vec<Role>>,
    pub client: Option<HashMap<String, Vec<Role>>>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct Role {
    pub attributes: Option<HashMap<String, String>>,
    pub client_role: Option<bool>,
    pub composite: Option<bool>,
    pub composites: Option<RoleComposite>,
    pub container_id: Option<String>,
    pub description: Option<String>,
    pub id: Option<String>,
    pub name: Option<String>,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
#[serde(rename_all = "camelCase")]
pub struct RoleComposite {
    pub realm: Option<Vec<String>>,
    pub client: HashMap<String, Vec<String>>,
}