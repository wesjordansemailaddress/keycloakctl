use serde::{Serialize,Deserialize};

use std::fs::File;
use crate::cli::Opts;

pub struct KeycloakConfig {
    pub server: KeycloakServer,
    pub user: KeycloakUser
}

impl KeycloakConfig {
    pub fn from_spec(spec: KeycloakConfigFile) -> Result<KeycloakConfig, KeycloakConfigError> {
        fn err(msg: &str) -> KeycloakConfigError {
            KeycloakConfigError::ReferenceError(msg.to_string())
        }

        let current_context = spec.current_context.ok_or_else(|| err("current_context not defined!"))?;
        let contexts = spec.contexts.ok_or_else(|| err("contexts not defined!"))?;
        let servers = spec.servers.ok_or_else(|| err("servers not defined!"))?;
        let users = spec.users.ok_or_else(|| err("users not defined"))?;

        let current_context = contexts.into_iter()
            .find(|it| &it.name == &current_context)
            .ok_or_else(|| err("could not find context in file."))?;

        let current_server = servers.into_iter()
            .find(|it| &it.name == &current_context.server)
            .ok_or_else(|| err("could not find server in file."))?;

        let current_user = users.into_iter()
            .find(|it| &it.name == &current_context.user)
            .ok_or_else(|| err("could not find user in file."))?;

        Ok(KeycloakConfig {
            server: current_server,
            user: current_user
        })
    }

    pub fn from_file(path: &str) -> Result<KeycloakConfig, KeycloakConfigError> {
        let file = KeycloakConfigFile::from_file(path)?;
        KeycloakConfig::from_spec(file)
    }

    pub fn from_opts(opts: &Opts) -> Result<KeycloakConfig, KeycloakConfigError> {
        let env = std::env::var("KEYCLOAK_CONFIG").ok();
        let filepath = opts.config_path.as_deref()
            .or(env.as_deref())
            .ok_or_else(|| KeycloakConfigError::NotSet(
                "Could not find config file. --config or KEYCLOAK_CONFIG must be set".to_string()))?;

        let file = KeycloakConfigFile::from_file(&filepath)?;
        KeycloakConfig::from_spec(file)
    }
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub struct KeycloakConfigFile {
    pub servers: Option<Vec<KeycloakServer>>,
    pub users: Option<Vec<KeycloakUser>>,
    pub contexts: Option<Vec<KeycloakContext>>,
    pub current_context: Option<String>,
}

impl KeycloakConfigFile {
    pub fn from_file(path: &str) -> Result<KeycloakConfigFile, KeycloakConfigError> {
        debug!("Reading config file from '{}'", path);
        let config_file = File::open(path)?;
        let config = serde_yaml::from_reader(config_file)?;
        debug!("Config read OK");
        Ok(config)
    }
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub struct KeycloakServer {
    pub server: String,
    pub realm: Option<String>,
    pub client: Option<String>,
    pub name: String,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub struct KeycloakContext {
    pub server: String,
    pub user: String,
    pub name: String,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub struct KeycloakUser {
    pub name: String,
    pub user: KeycloakUserCredentials,
}

#[derive(Serialize,Deserialize, Clone, Debug)]
pub struct KeycloakUserCredentials {
    pub username: String,
    pub password: String,
}

#[derive(Debug)]
pub enum KeycloakConfigError {
    NotSet(String),
    FileError(std::io::Error),
    YamlError(serde_yaml::Error),
    ReferenceError(String)
}

impl From<std::io::Error> for KeycloakConfigError {
    fn from(e: std::io::Error) -> Self {
        Self::FileError(e)
    }
}

impl From<serde_yaml::Error> for KeycloakConfigError {
    fn from(e: serde_yaml::Error) -> Self {
        Self::YamlError(e)
    }
}