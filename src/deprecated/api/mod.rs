pub mod config;
pub mod resource;

use serde::{Serialize, Deserialize};
use reqwest::header;
use url::Url;
use paste::paste;

use crate::api::config::KeycloakConfig;
use crate::models::client::Client;
use crate::models::role::Role;
use crate::models::group::Group;
use crate::models::user::User;
use crate::models::client::ClientScope;

use resource::{ResourceKind, Resource, ResourceRep};
use crate::api::KeycloakApiError::NotSupportedYet;
use crate::models::realm::Realm;

pub struct KeycloakApi {
    pub host: Url,
    pub auth: Authorization,
    client: reqwest::Client,
}

macro_rules! rest_resource {
    ($plural:ident: $type:ty) => {
        paste!{
            pub async fn [<get_$plural>](&self, realm: &str) -> Result<Vec<$type>> {
                let url = self.host.join(&format!("realms/{}/{}", realm, stringify!($plural)))?;
                debug!("GET {}", url);
                let response = self.client.get(url).send().await?;
                let status = response.status();
                if status == reqwest::StatusCode::OK {
                    Ok(response.json().await?)
                } else {
                    Err(KeycloakApiError::Response(status, response))
                }
            }

            pub async fn [<get_ $plural _by_id>](&self, realm: &str, id: &str) -> Result<$type> {
                let url = self.host.join(&format!("realms/{}/{}/{}", realm, stringify!($plural), id))?;
                debug!("GET {}", url);
                let response = self.client.get(url).send().await?;
                let status = response.status();
                if status == reqwest::StatusCode::OK {
                    Ok(response.json().await?)
                } else {
                    Err(KeycloakApiError::Response(status, response))
                }
            }
        }
    };
    ($plural:ident: $type:ty [name=$name:literal]) => {
        paste!{
            pub async fn [<get_$plural>](&self, realm: &str) -> Result<Vec<$type>> {
                let url = self.host.join(&format!("realms/{}/{}", realm, $name))?;
                debug!("GET {}", url);
                let response = self.client.get(url).send().await?;
                let status = response.status();
                if status == reqwest::StatusCode::OK {
                    Ok(response.json().await?)
                } else {
                    Err(KeycloakApiError::Response(status, response))
                }
            }

            pub async fn [<get_ $plural _by_id>](&self, realm: &str, id: &str) -> Result<$type> {
                let url = self.host.join(&format!("realms/{}/{}/{}", realm, $name, id))?;
                debug!("GET {}", url);
                let response = self.client.get(url).send().await?;
                let status = response.status();
                if status == reqwest::StatusCode::OK {
                    Ok(response.json().await?)
                } else {
                    Err(KeycloakApiError::Response(status, response))
                }
            }
        }
    };
}

impl KeycloakApi {
    pub fn from_token(host: &str, auth: Authorization) -> Result<KeycloakApi> {
        let mut headers = header::HeaderMap::new();
        headers.insert(header::AUTHORIZATION, header::HeaderValue::from_str(&auth.bearer_token()).unwrap());

        let client = reqwest::Client::builder()
            .default_headers(headers)
            .build()?;

        Ok(KeycloakApi {
            host: Url::parse(host)?.join("auth/admin/")?,
            auth,
            client,
        })
    }

    rest_resource!(clients: Client);
    rest_resource!(groups: Group);
    rest_resource!(roles: Role);
    rest_resource!(users: User);
    rest_resource!(client_scopes: ClientScope [name="client-scopes"]);

    pub async fn get_realms(&self) -> Result<Vec<Realm>> {
        let url = self.host.join("realms")?;
        debug!("GET {}", url);
        let response = self.client.get(url).send().await?;
        let status = response.status();
        if status == reqwest::StatusCode::OK {
            //Ok(response.json().await?)
            let body = response.text().await?;
            info!("{}", &body);
            Ok(serde_json::from_str(&body)?)
        } else {
            Err(KeycloakApiError::Response(status, response))
        }
    }

    pub async fn get_realm_by_id(&self, name: &str) -> Result<Realm> {
        let url = self.host.join(&format!("realms/{}", name))?;
        debug!("GET {}", url);
        let response = self.client.get(url).send().await?;
        let status = response.status();
        if status == reqwest::StatusCode::OK {
            Ok(response.json().await?)
        } else {
            Err(KeycloakApiError::Response(status, response))
        }
    }

    pub async fn list_resource(&self, realm: &str, kind: ResourceKind) -> Result<Resource> {
        macro_rules! map_resource {
            ($plural:ident: $kind:ty) => {
                paste!{
                    Ok(self.[<get_ $plural>](realm).await?
                        .into_iter().map(|it| Resource {
                        id: None,
                        kind: ResourceKind::$kind,
                        realm: realm.to_string(),
                        rep: ResourceRep::$kind(it)
                    }).collect())
                }
            }
        }
        let items = match kind {
            ResourceKind::Client => map_resource!(clients: Client),
            ResourceKind::ClientScope => map_resource!(client_scopes: ClientScope),
            ResourceKind::Group => map_resource!(groups: Group),
            ResourceKind::Role => map_resource!(roles: Role),
            ResourceKind::User => map_resource!(users: User),
            ResourceKind::Realm => {
                Ok(self.get_realms().await?
                    .into_iter().map(|it| Resource {
                    id: None,
                    kind: ResourceKind::Realm,
                    realm: "master".to_string(),
                    rep: ResourceRep::Realm(it)
                }).collect())
            }
            _ => Err(NotSupportedYet)
        }?;

        Ok(Resource {
            id: None,
            kind: ResourceKind::List,
            realm: realm.to_string(),
            rep: ResourceRep::List(items),
        })
    }

    pub async fn get_resource(&self, realm: &str, kind: ResourceKind, id: &str) -> Result<Resource> {
        match kind {
            ResourceKind::Client => self.get_clients_by_id(realm, id).await.map(|it| Resource {
                id: Some(id.to_string()),
                kind,
                realm: realm.to_string(),
                rep: ResourceRep::Client(it)
            }),
            ResourceKind::Realm => self.get_realm_by_id(id).await.map(|it| Resource {
                id: Some(id.to_string()),
                kind,
                realm: "master".to_string(),
                rep: ResourceRep::Realm(it)
            }),
            _ => Err(NotSupportedYet)
        }
    }

}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Authorization {
    pub access_token: String,
    pub expires_in: i64,
    pub refresh_expires_in: i64,
    pub refresh_token: String,
    pub token_type: String,
    #[serde(rename = "not-before-policy")]
    pub not_before_policy: i64,
    pub session_state: String,
    pub scope: String,
}

impl Authorization {
    pub fn bearer_token(&self) -> String {
        format!("Bearer {}", self.access_token)
    }
}

pub async fn keycloak_login(config: &KeycloakConfig) -> Result<Authorization> {
    let realm = config.server.realm.as_deref().unwrap_or("master");
    let client = config.server.client.as_deref().unwrap_or("admin-cli");

    let login_url = format!("{}/auth/realms/{}/protocol/openid-connect/token", &config.server.server, realm);
    debug!("{}", login_url);

    let params = [("username", config.user.user.username.as_str()),
        ("password", config.user.user.password.as_str()),
        ("grant_type", "password"),
        ("client_id", client)];

    debug!("{:#?}", &params);

    let login = reqwest::Client::default()
        .post(&login_url)
        .form(&params)
        .send()
        .await?;

    match login.status() {
        reqwest::StatusCode::OK => {
            let access_token = login.json().await?;
            Ok(access_token)
        }
        status => {
            Err(KeycloakApiError::LoginFailure(format!("Login returned status code {}", status)))
        }
    }
}

type Result<T> = std::result::Result<T, KeycloakApiError>;

macro_rules! err_mapping {
    ($fromtype:path => $totype:ty, $astype:ident) => {
        impl From<$fromtype> for $totype {
            fn from(e: $fromtype) -> Self {
                Self::$astype(e)
            }
        }
    }
}

#[derive(Debug)]
pub enum KeycloakApiError {
    Request(reqwest::Error),
    Url(url::ParseError),
    Response(reqwest::StatusCode, reqwest::Response),
    Yaml(serde_yaml::Error),
    Json(serde_json::Error),
    LoginFailure(String),
    NotSupportedYet,
}
err_mapping!(reqwest::Error => KeycloakApiError, Request);
err_mapping!(url::ParseError => KeycloakApiError, Url);
err_mapping!(serde_yaml::Error => KeycloakApiError, Yaml);
err_mapping!(serde_json::Error => KeycloakApiError, Json);