use crate::models;
use serde::{Serialize, Deserialize};
use std::str::FromStr;

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub enum ResourceKind {
    Client, ClientScope, Group, IdentityProvider, Realm, Role, User,
    List
}

impl FromStr for ResourceKind {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ResourceKind::{*};
        match s.to_lowercase().as_str() {
            "role" | "roles" => Ok(Role),
            "client" | "clients" => Ok(Client),
            "clientscope" | "clientscopes" | "cs" => Ok(ClientScope),
            "user" | "users" => Ok(User),
            "group" | "groups" => Ok(Group),
            "identityprovider" | "identityproviders" | "ip" => Ok(IdentityProvider),
            "realm" | "realms" => Ok(Realm),
            other => Err(format!("{} is not a ResourceKind", other))
        }
    }
}

impl ToString for ResourceKind {
    fn to_string(&self) -> String {
        use ResourceKind::{*};
        match self {
            Client => "client",
            ClientScope => "clientscope",
            Group => "group",
            IdentityProvider => "identityprovider",
            Realm => "realm",
            Role => "role",
            User => "user",
            List => "list"
        }.to_string()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "kind", content = "rep")]
pub enum ResourceRep {
    Role(models::role::Role),
    Client(models::client::Client),
    ClientScope(models::client::ClientScope),
    User(models::user::User),
    Group(models::group::Group),
    IdentityProvider(models::identity_provider::IdentityProvider),
    Realm(models::realm::Realm),
    DefaultClientScope(),
    DefaultGroups(),
    DefaultOptionalClientScopes(),
    RealmEventsConfig(),
    UserManagementPermissions(),
    List(Vec<Resource>)
}

impl ResourceRep {
    pub fn items(&self) -> Option<&[Resource]> {
        match self {
            ResourceRep::List(items) => Some(items.as_slice()),
            _ => None
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Resource {
    pub id: Option<String>,
    pub kind: ResourceKind,
    pub realm: String,
    #[serde(flatten)]
    pub rep: ResourceRep
}