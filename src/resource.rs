use keycloak_client::models;

use serde::{Serialize, Deserialize};
use std::str::FromStr;
use keycloak_client::models::{ClientRepresentation, GroupRepresentation, ClientScopeRepresentation, RoleRepresentation};

#[derive(Serialize, Deserialize, Debug, Copy, Clone)]
pub enum ResourceKind {
    Client, ClientScope, Group, IdentityProvider, ProtocolMapper, Realm, Role, User,
    List
}

impl FromStr for ResourceKind {
    type Err = String;
    fn from_str(s: &str) -> Result<Self, Self::Err> {
        use ResourceKind::{*};
        match s.to_lowercase().as_str() {
            "role" | "roles" => Ok(Role),
            "client" | "clients" => Ok(Client),
            "clientscope" | "clientscopes" | "cs" => Ok(ClientScope),
            "user" | "users" => Ok(User),
            "group" | "groups" => Ok(Group),
            "identityprovider" | "identityproviders" | "ip" => Ok(IdentityProvider),
            "protocolmapper" | "protocolmappers" | "pm" => Ok(ProtocolMapper),
            "realm" | "realms" => Ok(Realm),
            other => Err(format!("{} is not a ResourceKind", other))
        }
    }
}

impl ToString for ResourceKind {
    fn to_string(&self) -> String {
        use ResourceKind::{*};
        match self {
            Client => "client",
            ClientScope => "clientscope",
            Group => "group",
            IdentityProvider => "identityprovider",
            ProtocolMapper => "protocolmapper",
            Realm => "realm",
            Role => "role",
            User => "user",
            List => "list"
        }.to_string()
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
#[serde(tag = "kind", content = "spec")]
//#[serde(untagged)]
pub enum ResourceSpec {
    Role(models::RoleRepresentation),
    Client(models::ClientRepresentation),
    ClientScope(models::ClientScopeRepresentation),
    User(models::UserRepresentation),
    Group(models::GroupRepresentation),
    IdentityProvider(models::IdentityProviderRepresentation),
    Realm(models::RealmRepresentation),
    ProtocolMapper(models::ProtocolMapperRepresentation),
    List(Vec<Resource>)
}

impl ResourceSpec {
    pub fn items(&self) -> Option<&[Resource]> {
        match self {
            ResourceSpec::List(items) => Some(items.as_slice()),
            _ => None
        }
    }

    pub fn kind(&self) -> ResourceKind {
        match self {
            ResourceSpec::Client(_) => ResourceKind::Client,
            ResourceSpec::ClientScope(_) => ResourceKind::ClientScope,
            ResourceSpec::Group(_) => ResourceKind::Group,
            ResourceSpec::IdentityProvider(_) => ResourceKind::IdentityProvider,
            ResourceSpec::ProtocolMapper(_) => ResourceKind::ProtocolMapper,
            ResourceSpec::Realm(_) => ResourceKind::Realm,
            ResourceSpec::Role(_) => ResourceKind::Role,
            ResourceSpec::User(_) => ResourceKind::User,
            ResourceSpec::List(_) => ResourceKind::List
        }
    }
}

#[derive(Serialize, Deserialize, Clone, Debug)]
pub struct Resource {
    pub id: Option<String>,
    //kind: ResourceKind <-- implied field
    pub realm: Option<String>,
    #[serde(flatten)]
    pub spec: ResourceSpec
}

impl Resource {
    pub fn try_view_flatten(&self) -> &Resource {
        if let ResourceSpec::List(v) = &self.spec {
            if v.len() == 1 {
                return v.first().unwrap();
            }
        }

        return self;
    }

    pub fn kind(&self) -> ResourceKind {
        self.spec.kind()
    }
}

impl From<ClientRepresentation> for Resource {
    fn from(c: ClientRepresentation) -> Self {
        Resource {
            id: c.id.clone(),
            //kind: ResourceKind::Client,
            realm: None,
            spec: ResourceSpec::Client(c)
        }
    }
}

impl From<GroupRepresentation> for Resource {
    fn from(g: GroupRepresentation) -> Self {
        Resource {
            id: g.id.clone(),
            //kind: ResourceKind::Group,
            realm: None,
            spec: ResourceSpec::Group(g)
        }
    }
}

impl From<ClientScopeRepresentation> for Resource {
    fn from(g: ClientScopeRepresentation) -> Self {
        Resource {
            id: g.id.clone(),
            //kind: ResourceKind::ClientScope,
            realm: None,
            spec: ResourceSpec::ClientScope(g)
        }
    }
}

impl From<RoleRepresentation> for Resource {
    fn from(g: RoleRepresentation) -> Self {
        Resource {
            id: g.id.clone(),
            //kind: ResourceKind::Role,
            realm: None,
            spec: ResourceSpec::Role(g)
        }
    }
}

impl <I> From<Vec<I>> for Resource where I: Into<Resource> {
    fn from(items: Vec<I>) -> Self {
        let q = items.into_iter().map(|z| z.into()).collect();
        Resource {
            id: None,
            //kind: ResourceKind::List,
            realm: None,
            spec: ResourceSpec::List(q)
        }
    }
}