# \RealmsAdminApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_admin_events_delete**](RealmsAdminApi.md#realm_admin_events_delete) | **delete** /{realm}/admin-events | Delete all admin events
[**realm_admin_events_get**](RealmsAdminApi.md#realm_admin_events_get) | **get** /{realm}/admin-events | Get admin events   Returns all admin events, or filters events based on URL query parameters listed here
[**realm_clear_keys_cache_post**](RealmsAdminApi.md#realm_clear_keys_cache_post) | **post** /{realm}/clear-keys-cache | Clear cache of external public keys (Public keys of clients or Identity providers)
[**realm_clear_realm_cache_post**](RealmsAdminApi.md#realm_clear_realm_cache_post) | **post** /{realm}/clear-realm-cache | Clear realm cache
[**realm_clear_user_cache_post**](RealmsAdminApi.md#realm_clear_user_cache_post) | **post** /{realm}/clear-user-cache | Clear user cache
[**realm_client_description_converter_post**](RealmsAdminApi.md#realm_client_description_converter_post) | **post** /{realm}/client-description-converter | Base path for importing clients under this realm.
[**realm_client_session_stats_get**](RealmsAdminApi.md#realm_client_session_stats_get) | **get** /{realm}/client-session-stats | Get client session stats   Returns a JSON map.
[**realm_credential_registrators_get**](RealmsAdminApi.md#realm_credential_registrators_get) | **get** /{realm}/credential-registrators | 
[**realm_default_default_client_scopes_client_scope_id_delete**](RealmsAdminApi.md#realm_default_default_client_scopes_client_scope_id_delete) | **delete** /{realm}/default-default-client-scopes/{clientScopeId} | 
[**realm_default_default_client_scopes_client_scope_id_put**](RealmsAdminApi.md#realm_default_default_client_scopes_client_scope_id_put) | **put** /{realm}/default-default-client-scopes/{clientScopeId} | 
[**realm_default_default_client_scopes_get**](RealmsAdminApi.md#realm_default_default_client_scopes_get) | **get** /{realm}/default-default-client-scopes | Get realm default client scopes.
[**realm_default_groups_get**](RealmsAdminApi.md#realm_default_groups_get) | **get** /{realm}/default-groups | Get group hierarchy.
[**realm_default_groups_group_id_delete**](RealmsAdminApi.md#realm_default_groups_group_id_delete) | **delete** /{realm}/default-groups/{groupId} | 
[**realm_default_groups_group_id_put**](RealmsAdminApi.md#realm_default_groups_group_id_put) | **put** /{realm}/default-groups/{groupId} | 
[**realm_default_optional_client_scopes_client_scope_id_delete**](RealmsAdminApi.md#realm_default_optional_client_scopes_client_scope_id_delete) | **delete** /{realm}/default-optional-client-scopes/{clientScopeId} | 
[**realm_default_optional_client_scopes_client_scope_id_put**](RealmsAdminApi.md#realm_default_optional_client_scopes_client_scope_id_put) | **put** /{realm}/default-optional-client-scopes/{clientScopeId} | 
[**realm_default_optional_client_scopes_get**](RealmsAdminApi.md#realm_default_optional_client_scopes_get) | **get** /{realm}/default-optional-client-scopes | Get realm optional client scopes.
[**realm_delete**](RealmsAdminApi.md#realm_delete) | **delete** /{realm} | Delete the realm
[**realm_events_config_get**](RealmsAdminApi.md#realm_events_config_get) | **get** /{realm}/events/config | Get the events provider configuration   Returns JSON object with events provider configuration
[**realm_events_config_put**](RealmsAdminApi.md#realm_events_config_put) | **put** /{realm}/events/config | Update the events provider   Change the events provider and/or its configuration
[**realm_events_delete**](RealmsAdminApi.md#realm_events_delete) | **delete** /{realm}/events | Delete all events
[**realm_events_get**](RealmsAdminApi.md#realm_events_get) | **get** /{realm}/events | Get events   Returns all events, or filters them based on URL query parameters listed here
[**realm_get**](RealmsAdminApi.md#realm_get) | **get** /{realm} | Get the top-level representation of the realm   It will not include nested information like User and Client representations.
[**realm_group_by_path_path_get**](RealmsAdminApi.md#realm_group_by_path_path_get) | **get** /{realm}/group-by-path/{path} | 
[**realm_ldap_server_capabilities_post**](RealmsAdminApi.md#realm_ldap_server_capabilities_post) | **post** /{realm}/ldap-server-capabilities | Get LDAP supported extensions.
[**realm_logout_all_post**](RealmsAdminApi.md#realm_logout_all_post) | **post** /{realm}/logout-all | Removes all user sessions.
[**realm_partial_export_post**](RealmsAdminApi.md#realm_partial_export_post) | **post** /{realm}/partial-export | Partial export of existing realm into a JSON file.
[**realm_partial_import_post**](RealmsAdminApi.md#realm_partial_import_post) | **post** /{realm}/partialImport | Partial import from a JSON file to an existing realm.
[**realm_push_revocation_post**](RealmsAdminApi.md#realm_push_revocation_post) | **post** /{realm}/push-revocation | Push the realm’s revocation policy to any client that has an admin url associated with it.
[**realm_put**](RealmsAdminApi.md#realm_put) | **put** /{realm} | Update the top-level information of the realm   Any user, roles or client information in the representation  will be ignored.
[**realm_sessions_session_delete**](RealmsAdminApi.md#realm_sessions_session_delete) | **delete** /{realm}/sessions/{session} | Remove a specific user session.
[**realm_test_ldap_connection_post**](RealmsAdminApi.md#realm_test_ldap_connection_post) | **post** /{realm}/testLDAPConnection | Test LDAP connection
[**realm_test_smtp_connection_post**](RealmsAdminApi.md#realm_test_smtp_connection_post) | **post** /{realm}/testSMTPConnection | 
[**realm_users_management_permissions_get**](RealmsAdminApi.md#realm_users_management_permissions_get) | **get** /{realm}/users-management-permissions | 
[**realm_users_management_permissions_put**](RealmsAdminApi.md#realm_users_management_permissions_put) | **put** /{realm}/users-management-permissions | 
[**root_post**](RealmsAdminApi.md#root_post) | **post** / | Import a realm   Imports a realm from a full representation of that realm.



## realm_admin_events_delete

> realm_admin_events_delete(realm)
Delete all admin events

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_admin_events_get

> Vec<crate::models::AdminEventRepresentation> realm_admin_events_get(realm, auth_client, auth_ip_address, auth_realm, auth_user, date_from, date_to, first, max, operation_types, resource_path, resource_types)
Get admin events   Returns all admin events, or filters events based on URL query parameters listed here

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**auth_client** | Option<**String**> |  |  |
**auth_ip_address** | Option<**String**> |  |  |
**auth_realm** | Option<**String**> |  |  |
**auth_user** | Option<**String**> | user id |  |
**date_from** | Option<**String**> |  |  |
**date_to** | Option<**String**> |  |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |
**operation_types** | Option<[**Vec<String>**](String.md)> |  |  |
**resource_path** | Option<**String**> |  |  |
**resource_types** | Option<[**Vec<String>**](String.md)> |  |  |

### Return type

[**Vec<crate::models::AdminEventRepresentation>**](AdminEventRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clear_keys_cache_post

> realm_clear_keys_cache_post(realm)
Clear cache of external public keys (Public keys of clients or Identity providers)

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clear_realm_cache_post

> realm_clear_realm_cache_post(realm)
Clear realm cache

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clear_user_cache_post

> realm_clear_user_cache_post(realm)
Clear user cache

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_description_converter_post

> crate::models::ClientRepresentation realm_client_description_converter_post(realm, body)
Base path for importing clients under this realm.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**body** | **String** |  | [required] |

### Return type

[**crate::models::ClientRepresentation**](ClientRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: text/plain
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_session_stats_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_client_session_stats_get(realm)
Get client session stats   Returns a JSON map.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_credential_registrators_get

> Vec<String> realm_credential_registrators_get(realm)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

**Vec<String>**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_default_client_scopes_client_scope_id_delete

> realm_default_default_client_scopes_client_scope_id_delete(realm, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_default_client_scopes_client_scope_id_put

> realm_default_default_client_scopes_client_scope_id_put(realm, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_default_client_scopes_get

> Vec<crate::models::ClientScopeRepresentation> realm_default_default_client_scopes_get(realm)
Get realm default client scopes.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::ClientScopeRepresentation>**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_groups_get

> Vec<crate::models::GroupRepresentation> realm_default_groups_get(realm)
Get group hierarchy.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_groups_group_id_delete

> realm_default_groups_group_id_delete(realm, group_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**group_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_groups_group_id_put

> realm_default_groups_group_id_put(realm, group_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**group_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_optional_client_scopes_client_scope_id_delete

> realm_default_optional_client_scopes_client_scope_id_delete(realm, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_optional_client_scopes_client_scope_id_put

> realm_default_optional_client_scopes_client_scope_id_put(realm, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_default_optional_client_scopes_get

> Vec<crate::models::ClientScopeRepresentation> realm_default_optional_client_scopes_get(realm)
Get realm optional client scopes.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::ClientScopeRepresentation>**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_delete

> realm_delete(realm)
Delete the realm

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_events_config_get

> crate::models::RealmEventsConfigRepresentation realm_events_config_get(realm)
Get the events provider configuration   Returns JSON object with events provider configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**crate::models::RealmEventsConfigRepresentation**](RealmEventsConfigRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_events_config_put

> realm_events_config_put(realm, realm_events_config_representation)
Update the events provider   Change the events provider and/or its configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**realm_events_config_representation** | [**RealmEventsConfigRepresentation**](RealmEventsConfigRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_events_delete

> realm_events_delete(realm)
Delete all events

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_events_get

> Vec<crate::models::EventRepresentation> realm_events_get(realm, client, date_from, date_to, first, ip_address, max, _type, user)
Get events   Returns all events, or filters them based on URL query parameters listed here

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client** | Option<**String**> | App or oauth client name |  |
**date_from** | Option<**String**> | From date |  |
**date_to** | Option<**String**> | To date |  |
**first** | Option<**i32**> | Paging offset |  |
**ip_address** | Option<**String**> | IP address |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |
**_type** | Option<[**Vec<String>**](String.md)> | The types of events to return |  |
**user** | Option<**String**> | User id |  |

### Return type

[**Vec<crate::models::EventRepresentation>**](EventRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_get

> crate::models::RealmRepresentation realm_get(realm)
Get the top-level representation of the realm   It will not include nested information like User and Client representations.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**crate::models::RealmRepresentation**](RealmRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_group_by_path_path_get

> crate::models::GroupRepresentation realm_group_by_path_path_get(realm, path)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**path** | **String** |  | [required] |

### Return type

[**crate::models::GroupRepresentation**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_ldap_server_capabilities_post

> realm_ldap_server_capabilities_post(realm, test_ldap_connection_representation)
Get LDAP supported extensions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**test_ldap_connection_representation** | [**TestLdapConnectionRepresentation**](TestLdapConnectionRepresentation.md) | LDAP configuration | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_logout_all_post

> realm_logout_all_post(realm)
Removes all user sessions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_partial_export_post

> crate::models::RealmRepresentation realm_partial_export_post(realm, export_clients, export_groups_and_roles)
Partial export of existing realm into a JSON file.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**export_clients** | Option<**bool**> |  |  |
**export_groups_and_roles** | Option<**bool**> |  |  |

### Return type

[**crate::models::RealmRepresentation**](RealmRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_partial_import_post

> realm_partial_import_post(realm, partial_import_representation)
Partial import from a JSON file to an existing realm.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**partial_import_representation** | [**PartialImportRepresentation**](PartialImportRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_push_revocation_post

> realm_push_revocation_post(realm)
Push the realm’s revocation policy to any client that has an admin url associated with it.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_put

> realm_put(realm, realm_representation)
Update the top-level information of the realm   Any user, roles or client information in the representation  will be ignored.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**realm_representation** | [**RealmRepresentation**](RealmRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_sessions_session_delete

> realm_sessions_session_delete(realm, session)
Remove a specific user session.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**session** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_test_ldap_connection_post

> realm_test_ldap_connection_post(realm, test_ldap_connection_representation)
Test LDAP connection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**test_ldap_connection_representation** | [**TestLdapConnectionRepresentation**](TestLdapConnectionRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_test_smtp_connection_post

> realm_test_smtp_connection_post(realm, request_body)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_management_permissions_get

> crate::models::ManagementPermissionReference realm_users_management_permissions_get(realm)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_management_permissions_put

> crate::models::ManagementPermissionReference realm_users_management_permissions_put(realm, management_permission_reference)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## root_post

> root_post(realm_representation)
Import a realm   Imports a realm from a full representation of that realm.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm_representation** | [**RealmRepresentation**](RealmRepresentation.md) | JSON representation of the realm | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

