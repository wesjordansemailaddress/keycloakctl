# \RoleMapperApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_groups_id_role_mappings_get**](RoleMapperApi.md#realm_groups_id_role_mappings_get) | **get** /{realm}/groups/{id}/role-mappings | Get role mappings
[**realm_groups_id_role_mappings_realm_available_get**](RoleMapperApi.md#realm_groups_id_role_mappings_realm_available_get) | **get** /{realm}/groups/{id}/role-mappings/realm/available | Get realm-level roles that can be mapped
[**realm_groups_id_role_mappings_realm_composite_get**](RoleMapperApi.md#realm_groups_id_role_mappings_realm_composite_get) | **get** /{realm}/groups/{id}/role-mappings/realm/composite | Get effective realm-level role mappings   This will recurse all composite roles to get the result.
[**realm_groups_id_role_mappings_realm_delete**](RoleMapperApi.md#realm_groups_id_role_mappings_realm_delete) | **delete** /{realm}/groups/{id}/role-mappings/realm | Delete realm-level role mappings
[**realm_groups_id_role_mappings_realm_get**](RoleMapperApi.md#realm_groups_id_role_mappings_realm_get) | **get** /{realm}/groups/{id}/role-mappings/realm | Get realm-level role mappings
[**realm_groups_id_role_mappings_realm_post**](RoleMapperApi.md#realm_groups_id_role_mappings_realm_post) | **post** /{realm}/groups/{id}/role-mappings/realm | Add realm-level role mappings to the user
[**realm_users_id_role_mappings_get**](RoleMapperApi.md#realm_users_id_role_mappings_get) | **get** /{realm}/users/{id}/role-mappings | Get role mappings
[**realm_users_id_role_mappings_realm_available_get**](RoleMapperApi.md#realm_users_id_role_mappings_realm_available_get) | **get** /{realm}/users/{id}/role-mappings/realm/available | Get realm-level roles that can be mapped
[**realm_users_id_role_mappings_realm_composite_get**](RoleMapperApi.md#realm_users_id_role_mappings_realm_composite_get) | **get** /{realm}/users/{id}/role-mappings/realm/composite | Get effective realm-level role mappings   This will recurse all composite roles to get the result.
[**realm_users_id_role_mappings_realm_delete**](RoleMapperApi.md#realm_users_id_role_mappings_realm_delete) | **delete** /{realm}/users/{id}/role-mappings/realm | Delete realm-level role mappings
[**realm_users_id_role_mappings_realm_get**](RoleMapperApi.md#realm_users_id_role_mappings_realm_get) | **get** /{realm}/users/{id}/role-mappings/realm | Get realm-level role mappings
[**realm_users_id_role_mappings_realm_post**](RoleMapperApi.md#realm_users_id_role_mappings_realm_post) | **post** /{realm}/users/{id}/role-mappings/realm | Add realm-level role mappings to the user



## realm_groups_id_role_mappings_get

> crate::models::MappingsRepresentation realm_groups_id_role_mappings_get(realm, id)
Get role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**crate::models::MappingsRepresentation**](MappingsRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_realm_available_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_realm_available_get(realm, id)
Get realm-level roles that can be mapped

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_realm_composite_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_realm_composite_get(realm, id, brief_representation)
Get effective realm-level role mappings   This will recurse all composite roles to get the result.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_realm_delete

> realm_groups_id_role_mappings_realm_delete(realm, id, role_representation)
Delete realm-level role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_realm_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_realm_get(realm, id)
Get realm-level role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_realm_post

> realm_groups_id_role_mappings_realm_post(realm, id, role_representation)
Add realm-level role mappings to the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) | Roles to add | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_get

> crate::models::MappingsRepresentation realm_users_id_role_mappings_get(realm, id)
Get role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**crate::models::MappingsRepresentation**](MappingsRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_realm_available_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_realm_available_get(realm, id)
Get realm-level roles that can be mapped

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_realm_composite_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_realm_composite_get(realm, id, brief_representation)
Get effective realm-level role mappings   This will recurse all composite roles to get the result.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_realm_delete

> realm_users_id_role_mappings_realm_delete(realm, id, role_representation)
Delete realm-level role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_realm_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_realm_get(realm, id)
Get realm-level role mappings

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_realm_post

> realm_users_id_role_mappings_realm_post(realm, id, role_representation)
Add realm-level role mappings to the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) | Roles to add | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

