# PartialImportRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clients** | Option<[**Vec<crate::models::ClientRepresentation>**](ClientRepresentation.md)> |  | [optional]
**groups** | Option<[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)> |  | [optional]
**identity_providers** | Option<[**Vec<crate::models::IdentityProviderRepresentation>**](IdentityProviderRepresentation.md)> |  | [optional]
**if_resource_exists** | Option<**String**> |  | [optional]
**policy** | Option<**String**> |  | [optional]
**roles** | Option<[**crate::models::RolesRepresentation**](RolesRepresentation.md)> |  | [optional]
**users** | Option<[**Vec<crate::models::UserRepresentation>**](UserRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


