# AuthenticationExecutionExportRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**authenticator** | Option<**String**> |  | [optional]
**authenticator_config** | Option<**String**> |  | [optional]
**authenticator_flow** | Option<**bool**> |  | [optional]
**autheticator_flow** | Option<**bool**> |  | [optional]
**flow_alias** | Option<**String**> |  | [optional]
**priority** | Option<**i32**> |  | [optional]
**requirement** | Option<**String**> |  | [optional]
**user_setup_allowed** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


