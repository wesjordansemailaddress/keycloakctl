# ScopeRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**display_name** | Option<**String**> |  | [optional]
**icon_uri** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**policies** | Option<[**Vec<crate::models::PolicyRepresentation>**](PolicyRepresentation.md)> |  | [optional]
**resources** | Option<[**Vec<crate::models::ResourceRepresentation>**](ResourceRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


