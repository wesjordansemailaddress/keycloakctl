# ComponentTypeRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**help_text** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**metadata** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**properties** | Option<[**Vec<crate::models::ConfigPropertyRepresentation>**](ConfigPropertyRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


