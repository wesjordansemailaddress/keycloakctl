# KeyStoreConfig

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**format** | Option<**String**> |  | [optional]
**key_alias** | Option<**String**> |  | [optional]
**key_password** | Option<**String**> |  | [optional]
**realm_alias** | Option<**String**> |  | [optional]
**realm_certificate** | Option<**bool**> |  | [optional]
**store_password** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


