# RequiredActionProviderRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alias** | Option<**String**> |  | [optional]
**config** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**default_action** | Option<**bool**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**priority** | Option<**i32**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


