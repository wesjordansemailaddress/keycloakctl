# ConfigPropertyRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**default_value** | Option<[**serde_json::Value**](.md)> |  | [optional]
**help_text** | Option<**String**> |  | [optional]
**label** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**options** | Option<**Vec<String>**> |  | [optional]
**secret** | Option<**bool**> |  | [optional]
**_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


