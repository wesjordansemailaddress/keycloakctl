# UserSessionRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**clients** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**id** | Option<**String**> |  | [optional]
**ip_address** | Option<**String**> |  | [optional]
**last_access** | Option<**i64**> |  | [optional]
**start** | Option<**i64**> |  | [optional]
**user_id** | Option<**String**> |  | [optional]
**username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


