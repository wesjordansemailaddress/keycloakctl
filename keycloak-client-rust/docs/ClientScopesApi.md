# \ClientScopesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_client_scopes_get**](ClientScopesApi.md#realm_client_scopes_get) | **get** /{realm}/client-scopes | Get client scopes belonging to the realm   Returns a list of client scopes belonging to the realm
[**realm_client_scopes_id_delete**](ClientScopesApi.md#realm_client_scopes_id_delete) | **delete** /{realm}/client-scopes/{id} | Delete the client scope
[**realm_client_scopes_id_get**](ClientScopesApi.md#realm_client_scopes_id_get) | **get** /{realm}/client-scopes/{id} | Get representation of the client scope
[**realm_client_scopes_id_put**](ClientScopesApi.md#realm_client_scopes_id_put) | **put** /{realm}/client-scopes/{id} | Update the client scope
[**realm_client_scopes_post**](ClientScopesApi.md#realm_client_scopes_post) | **post** /{realm}/client-scopes | Create a new client scope   Client Scope’s name must be unique!



## realm_client_scopes_get

> Vec<crate::models::ClientScopeRepresentation> realm_client_scopes_get(realm)
Get client scopes belonging to the realm   Returns a list of client scopes belonging to the realm

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::ClientScopeRepresentation>**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_delete

> realm_client_scopes_id_delete(realm, id)
Delete the client scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_get

> crate::models::ClientScopeRepresentation realm_client_scopes_id_get(realm, id)
Get representation of the client scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

[**crate::models::ClientScopeRepresentation**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_put

> realm_client_scopes_id_put(realm, id, client_scope_representation)
Update the client scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client_scope_representation** | [**ClientScopeRepresentation**](ClientScopeRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_post

> realm_client_scopes_post(realm, client_scope_representation)
Create a new client scope   Client Scope’s name must be unique!

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_scope_representation** | [**ClientScopeRepresentation**](ClientScopeRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

