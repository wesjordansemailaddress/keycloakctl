# ComponentExportRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | Option<[**crate::models::MultivaluedHashMap**](MultivaluedHashMap.md)> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]
**sub_components** | Option<[**crate::models::MultivaluedHashMap**](MultivaluedHashMap.md)> |  | [optional]
**sub_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


