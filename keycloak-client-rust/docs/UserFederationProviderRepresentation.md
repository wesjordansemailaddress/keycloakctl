# UserFederationProviderRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**changed_sync_period** | Option<**i32**> |  | [optional]
**config** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**full_sync_period** | Option<**i32**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**last_sync** | Option<**i32**> |  | [optional]
**priority** | Option<**i32**> |  | [optional]
**provider_name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


