# AccessTokenCertConf

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**x5t_s256** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


