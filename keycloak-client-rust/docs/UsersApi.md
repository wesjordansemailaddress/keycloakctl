# \UsersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_users_count_get**](UsersApi.md#realm_users_count_get) | **get** /{realm}/users/count | Returns the number of users that match the given criteria.
[**realm_users_get**](UsersApi.md#realm_users_get) | **get** /{realm}/users | Get users   Returns a list of users, filtered according to query parameters
[**realm_users_id_configured_user_storage_credential_types_get**](UsersApi.md#realm_users_id_configured_user_storage_credential_types_get) | **get** /{realm}/users/{id}/configured-user-storage-credential-types | Return credential types, which are provided by the user storage where user is stored.
[**realm_users_id_consents_client_delete**](UsersApi.md#realm_users_id_consents_client_delete) | **delete** /{realm}/users/{id}/consents/{client} | Revoke consent and offline tokens for particular client from user
[**realm_users_id_consents_get**](UsersApi.md#realm_users_id_consents_get) | **get** /{realm}/users/{id}/consents | Get consents granted by the user
[**realm_users_id_credentials_credential_id_delete**](UsersApi.md#realm_users_id_credentials_credential_id_delete) | **delete** /{realm}/users/{id}/credentials/{credentialId} | Remove a credential for a user
[**realm_users_id_credentials_credential_id_move_after_new_previous_credential_id_post**](UsersApi.md#realm_users_id_credentials_credential_id_move_after_new_previous_credential_id_post) | **post** /{realm}/users/{id}/credentials/{credentialId}/moveAfter/{newPreviousCredentialId} | Move a credential to a position behind another credential
[**realm_users_id_credentials_credential_id_move_to_first_post**](UsersApi.md#realm_users_id_credentials_credential_id_move_to_first_post) | **post** /{realm}/users/{id}/credentials/{credentialId}/moveToFirst | Move a credential to a first position in the credentials list of the user
[**realm_users_id_credentials_credential_id_user_label_put**](UsersApi.md#realm_users_id_credentials_credential_id_user_label_put) | **put** /{realm}/users/{id}/credentials/{credentialId}/userLabel | Update a credential label for a user
[**realm_users_id_credentials_get**](UsersApi.md#realm_users_id_credentials_get) | **get** /{realm}/users/{id}/credentials | 
[**realm_users_id_delete**](UsersApi.md#realm_users_id_delete) | **delete** /{realm}/users/{id} | Delete the user
[**realm_users_id_disable_credential_types_put**](UsersApi.md#realm_users_id_disable_credential_types_put) | **put** /{realm}/users/{id}/disable-credential-types | Disable all credentials for a user of a specific type
[**realm_users_id_execute_actions_email_put**](UsersApi.md#realm_users_id_execute_actions_email_put) | **put** /{realm}/users/{id}/execute-actions-email | Send a update account email to the user   An email contains a link the user can click to perform a set of required actions.
[**realm_users_id_federated_identity_get**](UsersApi.md#realm_users_id_federated_identity_get) | **get** /{realm}/users/{id}/federated-identity | Get social logins associated with the user
[**realm_users_id_federated_identity_provider_delete**](UsersApi.md#realm_users_id_federated_identity_provider_delete) | **delete** /{realm}/users/{id}/federated-identity/{provider} | Remove a social login provider from user
[**realm_users_id_federated_identity_provider_post**](UsersApi.md#realm_users_id_federated_identity_provider_post) | **post** /{realm}/users/{id}/federated-identity/{provider} | Add a social login provider to the user
[**realm_users_id_get**](UsersApi.md#realm_users_id_get) | **get** /{realm}/users/{id} | Get representation of the user
[**realm_users_id_groups_count_get**](UsersApi.md#realm_users_id_groups_count_get) | **get** /{realm}/users/{id}/groups/count | 
[**realm_users_id_groups_get**](UsersApi.md#realm_users_id_groups_get) | **get** /{realm}/users/{id}/groups | 
[**realm_users_id_groups_group_id_delete**](UsersApi.md#realm_users_id_groups_group_id_delete) | **delete** /{realm}/users/{id}/groups/{groupId} | 
[**realm_users_id_groups_group_id_put**](UsersApi.md#realm_users_id_groups_group_id_put) | **put** /{realm}/users/{id}/groups/{groupId} | 
[**realm_users_id_impersonation_post**](UsersApi.md#realm_users_id_impersonation_post) | **post** /{realm}/users/{id}/impersonation | Impersonate the user
[**realm_users_id_logout_post**](UsersApi.md#realm_users_id_logout_post) | **post** /{realm}/users/{id}/logout | Remove all user sessions associated with the user   Also send notification to all clients that have an admin URL to invalidate the sessions for the particular user.
[**realm_users_id_offline_sessions_client_uuid_get**](UsersApi.md#realm_users_id_offline_sessions_client_uuid_get) | **get** /{realm}/users/{id}/offline-sessions/{clientUuid} | Get offline sessions associated with the user and client
[**realm_users_id_put**](UsersApi.md#realm_users_id_put) | **put** /{realm}/users/{id} | Update the user
[**realm_users_id_reset_password_put**](UsersApi.md#realm_users_id_reset_password_put) | **put** /{realm}/users/{id}/reset-password | Set up a new password for the user.
[**realm_users_id_send_verify_email_put**](UsersApi.md#realm_users_id_send_verify_email_put) | **put** /{realm}/users/{id}/send-verify-email | Send an email-verification email to the user   An email contains a link the user can click to verify their email address.
[**realm_users_id_sessions_get**](UsersApi.md#realm_users_id_sessions_get) | **get** /{realm}/users/{id}/sessions | Get sessions associated with the user
[**realm_users_post**](UsersApi.md#realm_users_post) | **post** /{realm}/users | Create a new user   Username must be unique.



## realm_users_count_get

> i32 realm_users_count_get(realm, email, first_name, last_name, search, username)
Returns the number of users that match the given criteria.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**email** | Option<**String**> | email filter |  |
**first_name** | Option<**String**> | first name filter |  |
**last_name** | Option<**String**> | last name filter |  |
**search** | Option<**String**> | arbitrary search string for all the fields below |  |
**username** | Option<**String**> | username filter |  |

### Return type

**i32**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_get

> Vec<crate::models::UserRepresentation> realm_users_get(realm, brief_representation, email, enabled, exact, first, first_name, last_name, max, search, username)
Get users   Returns a list of users, filtered according to query parameters

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**brief_representation** | Option<**bool**> |  |  |
**email** | Option<**String**> |  |  |
**enabled** | Option<**bool**> | Boolean representing if user is enabled or not |  |
**exact** | Option<**bool**> |  |  |
**first** | Option<**i32**> |  |  |
**first_name** | Option<**String**> |  |  |
**last_name** | Option<**String**> |  |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |
**search** | Option<**String**> | A String contained in username, first or last name, or email |  |
**username** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::UserRepresentation>**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_configured_user_storage_credential_types_get

> Vec<String> realm_users_id_configured_user_storage_credential_types_get(realm, id)
Return credential types, which are provided by the user storage where user is stored.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

**Vec<String>**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_consents_client_delete

> realm_users_id_consents_client_delete(realm, id, client)
Revoke consent and offline tokens for particular client from user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** | Client id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_consents_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_users_id_consents_get(realm, id)
Get consents granted by the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_credentials_credential_id_delete

> realm_users_id_credentials_credential_id_delete(realm, id, credential_id)
Remove a credential for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**credential_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_credentials_credential_id_move_after_new_previous_credential_id_post

> realm_users_id_credentials_credential_id_move_after_new_previous_credential_id_post(realm, id, credential_id, new_previous_credential_id)
Move a credential to a position behind another credential

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**credential_id** | **String** | The credential to move | [required] |
**new_previous_credential_id** | **String** | The credential that will be the previous element in the list. If set to null, the moved credential will be the first element in the list. | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_credentials_credential_id_move_to_first_post

> realm_users_id_credentials_credential_id_move_to_first_post(realm, id, credential_id)
Move a credential to a first position in the credentials list of the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**credential_id** | **String** | The credential to move | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_credentials_credential_id_user_label_put

> realm_users_id_credentials_credential_id_user_label_put(realm, id, credential_id, body)
Update a credential label for a user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**credential_id** | **String** |  | [required] |
**body** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: text/plain
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_credentials_get

> Vec<crate::models::CredentialRepresentation> realm_users_id_credentials_get(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<crate::models::CredentialRepresentation>**](CredentialRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_delete

> realm_users_id_delete(realm, id)
Delete the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_disable_credential_types_put

> realm_users_id_disable_credential_types_put(realm, id, request_body)
Disable all credentials for a user of a specific type

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**request_body** | [**Vec<String>**](String.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_execute_actions_email_put

> realm_users_id_execute_actions_email_put(realm, id, request_body, client_id, lifespan, redirect_uri)
Send a update account email to the user   An email contains a link the user can click to perform a set of required actions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**request_body** | [**Vec<String>**](String.md) | required actions the user needs to complete | [required] |
**client_id** | Option<**String**> | Client id |  |
**lifespan** | Option<**i32**> | Number of seconds after which the generated token expires |  |
**redirect_uri** | Option<**String**> | Redirect uri |  |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_federated_identity_get

> Vec<crate::models::FederatedIdentityRepresentation> realm_users_id_federated_identity_get(realm, id)
Get social logins associated with the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<crate::models::FederatedIdentityRepresentation>**](FederatedIdentityRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_federated_identity_provider_delete

> realm_users_id_federated_identity_provider_delete(realm, id, provider)
Remove a social login provider from user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**provider** | **String** | Social login provider id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_federated_identity_provider_post

> realm_users_id_federated_identity_provider_post(realm, id, provider, federated_identity_representation)
Add a social login provider to the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**provider** | **String** | Social login provider id | [required] |
**federated_identity_representation** | [**FederatedIdentityRepresentation**](FederatedIdentityRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_get

> crate::models::UserRepresentation realm_users_id_get(realm, id)
Get representation of the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**crate::models::UserRepresentation**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_groups_count_get

> ::std::collections::HashMap<String, serde_json::Value> realm_users_id_groups_count_get(realm, id, search)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**search** | Option<**String**> |  |  |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_groups_get

> Vec<crate::models::GroupRepresentation> realm_users_id_groups_get(realm, id, brief_representation, first, max, search)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**brief_representation** | Option<**bool**> |  |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |
**search** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_groups_group_id_delete

> realm_users_id_groups_group_id_delete(realm, id, group_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**group_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_groups_group_id_put

> realm_users_id_groups_group_id_put(realm, id, group_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**group_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_impersonation_post

> ::std::collections::HashMap<String, serde_json::Value> realm_users_id_impersonation_post(realm, id)
Impersonate the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_logout_post

> realm_users_id_logout_post(realm, id)
Remove all user sessions associated with the user   Also send notification to all clients that have an admin URL to invalidate the sessions for the particular user.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_offline_sessions_client_uuid_get

> Vec<crate::models::UserSessionRepresentation> realm_users_id_offline_sessions_client_uuid_get(realm, id, client_uuid)
Get offline sessions associated with the user and client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client_uuid** | **String** |  | [required] |

### Return type

[**Vec<crate::models::UserSessionRepresentation>**](UserSessionRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_put

> realm_users_id_put(realm, id, user_representation)
Update the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**user_representation** | [**UserRepresentation**](UserRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_reset_password_put

> realm_users_id_reset_password_put(realm, id, credential_representation)
Set up a new password for the user.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**credential_representation** | [**CredentialRepresentation**](CredentialRepresentation.md) | The representation must contain a rawPassword with the plain-text password | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_send_verify_email_put

> realm_users_id_send_verify_email_put(realm, id, client_id, redirect_uri)
Send an email-verification email to the user   An email contains a link the user can click to verify their email address.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client_id** | Option<**String**> | Client id |  |
**redirect_uri** | Option<**String**> | Redirect uri |  |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_sessions_get

> Vec<crate::models::UserSessionRepresentation> realm_users_id_sessions_get(realm, id)
Get sessions associated with the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |

### Return type

[**Vec<crate::models::UserSessionRepresentation>**](UserSessionRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_post

> realm_users_post(realm, user_representation)
Create a new user   Username must be unique.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**user_representation** | [**UserRepresentation**](UserRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

