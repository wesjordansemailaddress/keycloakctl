# PolicyRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**decision_strategy** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**logic** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner** | Option<**String**> |  | [optional]
**policies** | Option<**Vec<String>**> |  | [optional]
**resources** | Option<**Vec<String>**> |  | [optional]
**resources_data** | Option<[**Vec<crate::models::ResourceRepresentation>**](ResourceRepresentation.md)> |  | [optional]
**scopes** | Option<**Vec<String>**> |  | [optional]
**scopes_data** | Option<[**Vec<crate::models::ScopeRepresentation>**](ScopeRepresentation.md)> |  | [optional]
**_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


