# RealmEventsConfigRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**admin_events_details_enabled** | Option<**bool**> |  | [optional]
**admin_events_enabled** | Option<**bool**> |  | [optional]
**enabled_event_types** | Option<**Vec<String>**> |  | [optional]
**events_enabled** | Option<**bool**> |  | [optional]
**events_expiration** | Option<**i64**> |  | [optional]
**events_listeners** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


