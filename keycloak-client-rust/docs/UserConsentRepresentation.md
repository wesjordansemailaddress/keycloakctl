# UserConsentRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | Option<**String**> |  | [optional]
**created_date** | Option<**i64**> |  | [optional]
**granted_client_scopes** | Option<**Vec<String>**> |  | [optional]
**last_updated_date** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


