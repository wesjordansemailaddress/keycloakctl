# ClientMappingsRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**mappings** | Option<[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


