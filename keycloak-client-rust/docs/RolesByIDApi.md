# \RolesByIDApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_roles_by_id_role_id_composites_clients_client_uuid_get**](RolesByIDApi.md#realm_roles_by_id_role_id_composites_clients_client_uuid_get) | **get** /{realm}/roles-by-id/{role-id}/composites/clients/{clientUuid} | Get client-level roles for the client that are in the role’s composite
[**realm_roles_by_id_role_id_composites_delete**](RolesByIDApi.md#realm_roles_by_id_role_id_composites_delete) | **delete** /{realm}/roles-by-id/{role-id}/composites | Remove a set of roles from the role’s composite
[**realm_roles_by_id_role_id_composites_get**](RolesByIDApi.md#realm_roles_by_id_role_id_composites_get) | **get** /{realm}/roles-by-id/{role-id}/composites | Get role’s children   Returns a set of role’s children provided the role is a composite.
[**realm_roles_by_id_role_id_composites_post**](RolesByIDApi.md#realm_roles_by_id_role_id_composites_post) | **post** /{realm}/roles-by-id/{role-id}/composites | Make the role a composite role by associating some child roles
[**realm_roles_by_id_role_id_composites_realm_get**](RolesByIDApi.md#realm_roles_by_id_role_id_composites_realm_get) | **get** /{realm}/roles-by-id/{role-id}/composites/realm | Get realm-level roles that are in the role’s composite
[**realm_roles_by_id_role_id_delete**](RolesByIDApi.md#realm_roles_by_id_role_id_delete) | **delete** /{realm}/roles-by-id/{role-id} | Delete the role
[**realm_roles_by_id_role_id_get**](RolesByIDApi.md#realm_roles_by_id_role_id_get) | **get** /{realm}/roles-by-id/{role-id} | Get a specific role’s representation
[**realm_roles_by_id_role_id_management_permissions_get**](RolesByIDApi.md#realm_roles_by_id_role_id_management_permissions_get) | **get** /{realm}/roles-by-id/{role-id}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_roles_by_id_role_id_management_permissions_put**](RolesByIDApi.md#realm_roles_by_id_role_id_management_permissions_put) | **put** /{realm}/roles-by-id/{role-id}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_roles_by_id_role_id_put**](RolesByIDApi.md#realm_roles_by_id_role_id_put) | **put** /{realm}/roles-by-id/{role-id} | Update the role



## realm_roles_by_id_role_id_composites_clients_client_uuid_get

> Vec<crate::models::RoleRepresentation> realm_roles_by_id_role_id_composites_clients_client_uuid_get(realm, role_id, client_uuid)
Get client-level roles for the client that are in the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** |  | [required] |
**client_uuid** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_composites_delete

> realm_roles_by_id_role_id_composites_delete(realm, role_id, role_representation)
Remove a set of roles from the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | Role id | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) | A set of roles to be removed | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_composites_get

> Vec<crate::models::RoleRepresentation> realm_roles_by_id_role_id_composites_get(realm, role_id)
Get role’s children   Returns a set of role’s children provided the role is a composite.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | Role id | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_composites_post

> realm_roles_by_id_role_id_composites_post(realm, role_id, role_representation)
Make the role a composite role by associating some child roles

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | Role id | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_composites_realm_get

> Vec<crate::models::RoleRepresentation> realm_roles_by_id_role_id_composites_realm_get(realm, role_id)
Get realm-level roles that are in the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_delete

> realm_roles_by_id_role_id_delete(realm, role_id)
Delete the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | id of role | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_get

> crate::models::RoleRepresentation realm_roles_by_id_role_id_get(realm, role_id)
Get a specific role’s representation

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | id of role | [required] |

### Return type

[**crate::models::RoleRepresentation**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_management_permissions_get

> crate::models::ManagementPermissionReference realm_roles_by_id_role_id_management_permissions_get(realm, role_id)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_management_permissions_put

> crate::models::ManagementPermissionReference realm_roles_by_id_role_id_management_permissions_put(realm, role_id, management_permission_reference)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** |  | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_by_id_role_id_put

> realm_roles_by_id_role_id_put(realm, role_id, role_representation)
Update the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_id** | **String** | id of role | [required] |
**role_representation** | [**RoleRepresentation**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

