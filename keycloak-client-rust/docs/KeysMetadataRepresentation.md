# KeysMetadataRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**active** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**keys** | Option<[**Vec<crate::models::KeysMetadataRepresentationKeyMetadataRepresentation>**](KeysMetadataRepresentation-KeyMetadataRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


