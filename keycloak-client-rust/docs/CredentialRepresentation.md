# CredentialRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**created_date** | Option<**i64**> |  | [optional]
**credential_data** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**priority** | Option<**i32**> |  | [optional]
**secret_data** | Option<**String**> |  | [optional]
**temporary** | Option<**bool**> |  | [optional]
**_type** | Option<**String**> |  | [optional]
**user_label** | Option<**String**> |  | [optional]
**value** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


