# KeysMetadataRepresentationKeyMetadataRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**algorithm** | Option<**String**> |  | [optional]
**certificate** | Option<**String**> |  | [optional]
**kid** | Option<**String**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]
**provider_priority** | Option<**i64**> |  | [optional]
**public_key** | Option<**String**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**_type** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


