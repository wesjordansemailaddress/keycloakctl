# AccessToken

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**acr** | Option<**String**> |  | [optional]
**address** | Option<[**crate::models::AddressClaimSet**](AddressClaimSet.md)> |  | [optional]
**allowed_origins** | Option<**Vec<String>**> |  | [optional]
**at_hash** | Option<**String**> |  | [optional]
**auth_time** | Option<**i64**> |  | [optional]
**authorization** | Option<[**crate::models::AccessTokenAuthorization**](AccessToken-Authorization.md)> |  | [optional]
**azp** | Option<**String**> |  | [optional]
**birthdate** | Option<**String**> |  | [optional]
**c_hash** | Option<**String**> |  | [optional]
**category** | Option<**String**> |  | [optional]
**claims_locales** | Option<**String**> |  | [optional]
**cnf** | Option<[**crate::models::AccessTokenCertConf**](AccessToken-CertConf.md)> |  | [optional]
**email** | Option<**String**> |  | [optional]
**email_verified** | Option<**bool**> |  | [optional]
**exp** | Option<**i64**> |  | [optional]
**family_name** | Option<**String**> |  | [optional]
**gender** | Option<**String**> |  | [optional]
**given_name** | Option<**String**> |  | [optional]
**iat** | Option<**i64**> |  | [optional]
**iss** | Option<**String**> |  | [optional]
**jti** | Option<**String**> |  | [optional]
**locale** | Option<**String**> |  | [optional]
**middle_name** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**nbf** | Option<**i64**> |  | [optional]
**nickname** | Option<**String**> |  | [optional]
**nonce** | Option<**String**> |  | [optional]
**other_claims** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**phone_number** | Option<**String**> |  | [optional]
**phone_number_verified** | Option<**bool**> |  | [optional]
**picture** | Option<**String**> |  | [optional]
**preferred_username** | Option<**String**> |  | [optional]
**profile** | Option<**String**> |  | [optional]
**realm_access** | Option<[**crate::models::AccessTokenAccess**](AccessToken-Access.md)> |  | [optional]
**s_hash** | Option<**String**> |  | [optional]
**scope** | Option<**String**> |  | [optional]
**session_state** | Option<**String**> |  | [optional]
**sub** | Option<**String**> |  | [optional]
**trusted_certs** | Option<**Vec<String>**> |  | [optional]
**typ** | Option<**String**> |  | [optional]
**updated_at** | Option<**i64**> |  | [optional]
**website** | Option<**String**> |  | [optional]
**zoneinfo** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


