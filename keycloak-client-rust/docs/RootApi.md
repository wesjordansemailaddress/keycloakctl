# \RootApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**root_get**](RootApi.md#root_get) | **get** / | Get themes, social providers, auth providers, and event listeners available on this server



## root_get

> crate::models::ServerInfoRepresentation root_get()
Get themes, social providers, auth providers, and event listeners available on this server

### Parameters

This endpoint does not need any parameter.

### Return type

[**crate::models::ServerInfoRepresentation**](ServerInfoRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

