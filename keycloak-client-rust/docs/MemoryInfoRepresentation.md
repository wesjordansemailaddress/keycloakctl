# MemoryInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**free** | Option<**i64**> |  | [optional]
**free_formated** | Option<**String**> |  | [optional]
**free_percentage** | Option<**i64**> |  | [optional]
**total** | Option<**i64**> |  | [optional]
**total_formated** | Option<**String**> |  | [optional]
**used** | Option<**i64**> |  | [optional]
**used_formated** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


