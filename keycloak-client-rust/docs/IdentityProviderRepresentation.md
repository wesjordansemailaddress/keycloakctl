# IdentityProviderRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**add_read_token_role_on_create** | Option<**bool**> |  | [optional]
**alias** | Option<**String**> |  | [optional]
**config** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**first_broker_login_flow_alias** | Option<**String**> |  | [optional]
**internal_id** | Option<**String**> |  | [optional]
**link_only** | Option<**bool**> |  | [optional]
**post_broker_login_flow_alias** | Option<**String**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]
**store_token** | Option<**bool**> |  | [optional]
**trust_email** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


