# \ProtocolMappersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_client_scopes_id1_protocol_mappers_models_id2_delete**](ProtocolMappersApi.md#realm_client_scopes_id1_protocol_mappers_models_id2_delete) | **delete** /{realm}/client-scopes/{id1}/protocol-mappers/models/{id2} | Delete the mapper
[**realm_client_scopes_id1_protocol_mappers_models_id2_get**](ProtocolMappersApi.md#realm_client_scopes_id1_protocol_mappers_models_id2_get) | **get** /{realm}/client-scopes/{id1}/protocol-mappers/models/{id2} | Get mapper by id
[**realm_client_scopes_id1_protocol_mappers_models_id2_put**](ProtocolMappersApi.md#realm_client_scopes_id1_protocol_mappers_models_id2_put) | **put** /{realm}/client-scopes/{id1}/protocol-mappers/models/{id2} | Update the mapper
[**realm_client_scopes_id_protocol_mappers_add_models_post**](ProtocolMappersApi.md#realm_client_scopes_id_protocol_mappers_add_models_post) | **post** /{realm}/client-scopes/{id}/protocol-mappers/add-models | Create multiple mappers
[**realm_client_scopes_id_protocol_mappers_models_get**](ProtocolMappersApi.md#realm_client_scopes_id_protocol_mappers_models_get) | **get** /{realm}/client-scopes/{id}/protocol-mappers/models | Get mappers
[**realm_client_scopes_id_protocol_mappers_models_post**](ProtocolMappersApi.md#realm_client_scopes_id_protocol_mappers_models_post) | **post** /{realm}/client-scopes/{id}/protocol-mappers/models | Create a mapper
[**realm_client_scopes_id_protocol_mappers_protocol_protocol_get**](ProtocolMappersApi.md#realm_client_scopes_id_protocol_mappers_protocol_protocol_get) | **get** /{realm}/client-scopes/{id}/protocol-mappers/protocol/{protocol} | Get mappers by name for a specific protocol
[**realm_clients_id1_protocol_mappers_models_id2_delete**](ProtocolMappersApi.md#realm_clients_id1_protocol_mappers_models_id2_delete) | **delete** /{realm}/clients/{id1}/protocol-mappers/models/{id2} | Delete the mapper
[**realm_clients_id1_protocol_mappers_models_id2_get**](ProtocolMappersApi.md#realm_clients_id1_protocol_mappers_models_id2_get) | **get** /{realm}/clients/{id1}/protocol-mappers/models/{id2} | Get mapper by id
[**realm_clients_id1_protocol_mappers_models_id2_put**](ProtocolMappersApi.md#realm_clients_id1_protocol_mappers_models_id2_put) | **put** /{realm}/clients/{id1}/protocol-mappers/models/{id2} | Update the mapper
[**realm_clients_id_protocol_mappers_add_models_post**](ProtocolMappersApi.md#realm_clients_id_protocol_mappers_add_models_post) | **post** /{realm}/clients/{id}/protocol-mappers/add-models | Create multiple mappers
[**realm_clients_id_protocol_mappers_models_get**](ProtocolMappersApi.md#realm_clients_id_protocol_mappers_models_get) | **get** /{realm}/clients/{id}/protocol-mappers/models | Get mappers
[**realm_clients_id_protocol_mappers_models_post**](ProtocolMappersApi.md#realm_clients_id_protocol_mappers_models_post) | **post** /{realm}/clients/{id}/protocol-mappers/models | Create a mapper
[**realm_clients_id_protocol_mappers_protocol_protocol_get**](ProtocolMappersApi.md#realm_clients_id_protocol_mappers_protocol_protocol_get) | **get** /{realm}/clients/{id}/protocol-mappers/protocol/{protocol} | Get mappers by name for a specific protocol



## realm_client_scopes_id1_protocol_mappers_models_id2_delete

> realm_client_scopes_id1_protocol_mappers_models_id2_delete(realm, id1, id2)
Delete the mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id1_protocol_mappers_models_id2_get

> crate::models::ProtocolMapperRepresentation realm_client_scopes_id1_protocol_mappers_models_id2_get(realm, id1, id2)
Get mapper by id

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |

### Return type

[**crate::models::ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id1_protocol_mappers_models_id2_put

> realm_client_scopes_id1_protocol_mappers_models_id2_put(realm, id1, id2, protocol_mapper_representation)
Update the mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |
**protocol_mapper_representation** | [**ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_protocol_mappers_add_models_post

> realm_client_scopes_id_protocol_mappers_add_models_post(realm, id, protocol_mapper_representation)
Create multiple mappers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**protocol_mapper_representation** | [**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_protocol_mappers_models_get

> Vec<crate::models::ProtocolMapperRepresentation> realm_client_scopes_id_protocol_mappers_models_get(realm, id)
Get mappers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

[**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_protocol_mappers_models_post

> realm_client_scopes_id_protocol_mappers_models_post(realm, id, protocol_mapper_representation)
Create a mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**protocol_mapper_representation** | [**ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_protocol_mappers_protocol_protocol_get

> Vec<crate::models::ProtocolMapperRepresentation> realm_client_scopes_id_protocol_mappers_protocol_protocol_get(realm, id, protocol)
Get mappers by name for a specific protocol

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**protocol** | **String** |  | [required] |

### Return type

[**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id1_protocol_mappers_models_id2_delete

> realm_clients_id1_protocol_mappers_models_id2_delete(realm, id1, id2)
Delete the mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id1_protocol_mappers_models_id2_get

> crate::models::ProtocolMapperRepresentation realm_clients_id1_protocol_mappers_models_id2_get(realm, id1, id2)
Get mapper by id

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |

### Return type

[**crate::models::ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id1_protocol_mappers_models_id2_put

> realm_clients_id1_protocol_mappers_models_id2_put(realm, id1, id2, protocol_mapper_representation)
Update the mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id1** | **String** |  | [required] |
**id2** | **String** |  | [required] |
**protocol_mapper_representation** | [**ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_protocol_mappers_add_models_post

> realm_clients_id_protocol_mappers_add_models_post(realm, id, protocol_mapper_representation)
Create multiple mappers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**protocol_mapper_representation** | [**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_protocol_mappers_models_get

> Vec<crate::models::ProtocolMapperRepresentation> realm_clients_id_protocol_mappers_models_get(realm, id)
Get mappers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_protocol_mappers_models_post

> realm_clients_id_protocol_mappers_models_post(realm, id, protocol_mapper_representation)
Create a mapper

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**protocol_mapper_representation** | [**ProtocolMapperRepresentation**](ProtocolMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_protocol_mappers_protocol_protocol_get

> Vec<crate::models::ProtocolMapperRepresentation> realm_clients_id_protocol_mappers_protocol_protocol_get(realm, id, protocol)
Get mappers by name for a specific protocol

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**protocol** | **String** |  | [required] |

### Return type

[**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

