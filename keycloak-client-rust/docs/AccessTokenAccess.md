# AccessTokenAccess

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**roles** | Option<**Vec<String>**> |  | [optional]
**verify_caller** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


