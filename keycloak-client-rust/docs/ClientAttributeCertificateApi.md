# \ClientAttributeCertificateApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_clients_id_certificates_attr_download_post**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_download_post) | **post** /{realm}/clients/{id}/certificates/{attr}/download | Get a keystore file for the client, containing private key and public certificate
[**realm_clients_id_certificates_attr_generate_and_download_post**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_generate_and_download_post) | **post** /{realm}/clients/{id}/certificates/{attr}/generate-and-download | Generate a new keypair and certificate, and get the private key file   Generates a keypair and certificate and serves the private key in a specified keystore format.
[**realm_clients_id_certificates_attr_generate_post**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_generate_post) | **post** /{realm}/clients/{id}/certificates/{attr}/generate | Generate a new certificate with new key pair
[**realm_clients_id_certificates_attr_get**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_get) | **get** /{realm}/clients/{id}/certificates/{attr} | Get key info
[**realm_clients_id_certificates_attr_upload_certificate_post**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_upload_certificate_post) | **post** /{realm}/clients/{id}/certificates/{attr}/upload-certificate | Upload only certificate, not private key
[**realm_clients_id_certificates_attr_upload_post**](ClientAttributeCertificateApi.md#realm_clients_id_certificates_attr_upload_post) | **post** /{realm}/clients/{id}/certificates/{attr}/upload | Upload certificate and eventually private key



## realm_clients_id_certificates_attr_download_post

> String realm_clients_id_certificates_attr_download_post(realm, id, attr, key_store_config)
Get a keystore file for the client, containing private key and public certificate

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |
**key_store_config** | [**KeyStoreConfig**](KeyStoreConfig.md) | Keystore configuration as JSON | [required] |

### Return type

**String**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_certificates_attr_generate_and_download_post

> String realm_clients_id_certificates_attr_generate_and_download_post(realm, id, attr, key_store_config)
Generate a new keypair and certificate, and get the private key file   Generates a keypair and certificate and serves the private key in a specified keystore format.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |
**key_store_config** | [**KeyStoreConfig**](KeyStoreConfig.md) | Keystore configuration as JSON | [required] |

### Return type

**String**

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/octet-stream

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_certificates_attr_generate_post

> crate::models::CertificateRepresentation realm_clients_id_certificates_attr_generate_post(realm, id, attr)
Generate a new certificate with new key pair

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |

### Return type

[**crate::models::CertificateRepresentation**](CertificateRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_certificates_attr_get

> crate::models::CertificateRepresentation realm_clients_id_certificates_attr_get(realm, id, attr)
Get key info

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |

### Return type

[**crate::models::CertificateRepresentation**](CertificateRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_certificates_attr_upload_certificate_post

> crate::models::CertificateRepresentation realm_clients_id_certificates_attr_upload_certificate_post(realm, id, attr)
Upload only certificate, not private key

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |

### Return type

[**crate::models::CertificateRepresentation**](CertificateRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_certificates_attr_upload_post

> crate::models::CertificateRepresentation realm_clients_id_certificates_attr_upload_post(realm, id, attr)
Upload certificate and eventually private key

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**attr** | **String** |  | [required] |

### Return type

[**crate::models::CertificateRepresentation**](CertificateRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

