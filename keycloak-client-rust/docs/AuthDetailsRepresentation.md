# AuthDetailsRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | Option<**String**> |  | [optional]
**ip_address** | Option<**String**> |  | [optional]
**realm_id** | Option<**String**> |  | [optional]
**user_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


