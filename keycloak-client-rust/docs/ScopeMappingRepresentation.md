# ScopeMappingRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client** | Option<**String**> |  | [optional]
**client_scope** | Option<**String**> |  | [optional]
**roles** | Option<**Vec<String>**> |  | [optional]
**_self** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


