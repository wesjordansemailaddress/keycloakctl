# GlobalRequestResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**failed_requests** | Option<**Vec<String>**> |  | [optional]
**success_requests** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


