# \IdentityProvidersApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_identity_provider_import_config_post**](IdentityProvidersApi.md#realm_identity_provider_import_config_post) | **post** /{realm}/identity-provider/import-config | Import identity provider from uploaded JSON file
[**realm_identity_provider_instances_alias_delete**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_delete) | **delete** /{realm}/identity-provider/instances/{alias} | Delete the identity provider
[**realm_identity_provider_instances_alias_export_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_export_get) | **get** /{realm}/identity-provider/instances/{alias}/export | Export public broker configuration for identity provider
[**realm_identity_provider_instances_alias_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_get) | **get** /{realm}/identity-provider/instances/{alias} | Get the identity provider
[**realm_identity_provider_instances_alias_management_permissions_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_management_permissions_get) | **get** /{realm}/identity-provider/instances/{alias}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_identity_provider_instances_alias_management_permissions_put**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_management_permissions_put) | **put** /{realm}/identity-provider/instances/{alias}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_identity_provider_instances_alias_mapper_types_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mapper_types_get) | **get** /{realm}/identity-provider/instances/{alias}/mapper-types | Get mapper types for identity provider
[**realm_identity_provider_instances_alias_mappers_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mappers_get) | **get** /{realm}/identity-provider/instances/{alias}/mappers | Get mappers for identity provider
[**realm_identity_provider_instances_alias_mappers_id_delete**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mappers_id_delete) | **delete** /{realm}/identity-provider/instances/{alias}/mappers/{id} | Delete a mapper for the identity provider
[**realm_identity_provider_instances_alias_mappers_id_get**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mappers_id_get) | **get** /{realm}/identity-provider/instances/{alias}/mappers/{id} | Get mapper by id for the identity provider
[**realm_identity_provider_instances_alias_mappers_id_put**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mappers_id_put) | **put** /{realm}/identity-provider/instances/{alias}/mappers/{id} | Update a mapper for the identity provider
[**realm_identity_provider_instances_alias_mappers_post**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_mappers_post) | **post** /{realm}/identity-provider/instances/{alias}/mappers | Add a mapper to identity provider
[**realm_identity_provider_instances_alias_put**](IdentityProvidersApi.md#realm_identity_provider_instances_alias_put) | **put** /{realm}/identity-provider/instances/{alias} | Update the identity provider
[**realm_identity_provider_instances_get**](IdentityProvidersApi.md#realm_identity_provider_instances_get) | **get** /{realm}/identity-provider/instances | Get identity providers
[**realm_identity_provider_instances_post**](IdentityProvidersApi.md#realm_identity_provider_instances_post) | **post** /{realm}/identity-provider/instances | Create a new identity provider
[**realm_identity_provider_providers_provider_id_get**](IdentityProvidersApi.md#realm_identity_provider_providers_provider_id_get) | **get** /{realm}/identity-provider/providers/{provider_id} | Get identity providers



## realm_identity_provider_import_config_post

> ::std::collections::HashMap<String, serde_json::Value> realm_identity_provider_import_config_post(realm)
Import identity provider from uploaded JSON file

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_delete

> realm_identity_provider_instances_alias_delete(realm, alias)
Delete the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_export_get

> realm_identity_provider_instances_alias_export_get(realm, alias, format)
Export public broker configuration for identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**format** | Option<**String**> | Format to use |  |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_get

> crate::models::IdentityProviderRepresentation realm_identity_provider_instances_alias_get(realm, alias)
Get the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |

### Return type

[**crate::models::IdentityProviderRepresentation**](IdentityProviderRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_management_permissions_get

> crate::models::ManagementPermissionReference realm_identity_provider_instances_alias_management_permissions_get(realm, alias)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_management_permissions_put

> crate::models::ManagementPermissionReference realm_identity_provider_instances_alias_management_permissions_put(realm, alias, management_permission_reference)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mapper_types_get

> realm_identity_provider_instances_alias_mapper_types_get(realm, alias)
Get mapper types for identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mappers_get

> Vec<crate::models::IdentityProviderMapperRepresentation> realm_identity_provider_instances_alias_mappers_get(realm, alias)
Get mappers for identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |

### Return type

[**Vec<crate::models::IdentityProviderMapperRepresentation>**](IdentityProviderMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mappers_id_delete

> realm_identity_provider_instances_alias_mappers_id_delete(realm, alias, id)
Delete a mapper for the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**id** | **String** | Mapper id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mappers_id_get

> crate::models::IdentityProviderMapperRepresentation realm_identity_provider_instances_alias_mappers_id_get(realm, alias, id)
Get mapper by id for the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**id** | **String** | Mapper id | [required] |

### Return type

[**crate::models::IdentityProviderMapperRepresentation**](IdentityProviderMapperRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mappers_id_put

> realm_identity_provider_instances_alias_mappers_id_put(realm, alias, id, identity_provider_mapper_representation)
Update a mapper for the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**id** | **String** | Mapper id | [required] |
**identity_provider_mapper_representation** | [**IdentityProviderMapperRepresentation**](IdentityProviderMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_mappers_post

> realm_identity_provider_instances_alias_mappers_post(realm, alias, identity_provider_mapper_representation)
Add a mapper to identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**identity_provider_mapper_representation** | [**IdentityProviderMapperRepresentation**](IdentityProviderMapperRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_alias_put

> realm_identity_provider_instances_alias_put(realm, alias, identity_provider_representation)
Update the identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** |  | [required] |
**identity_provider_representation** | [**IdentityProviderRepresentation**](IdentityProviderRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_get

> Vec<crate::models::IdentityProviderRepresentation> realm_identity_provider_instances_get(realm)
Get identity providers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::IdentityProviderRepresentation>**](IdentityProviderRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_instances_post

> realm_identity_provider_instances_post(realm, identity_provider_representation)
Create a new identity provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**identity_provider_representation** | [**IdentityProviderRepresentation**](IdentityProviderRepresentation.md) | JSON body | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_identity_provider_providers_provider_id_get

> realm_identity_provider_providers_provider_id_get(realm, provider_id)
Get identity providers

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**provider_id** | **String** | Provider id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

