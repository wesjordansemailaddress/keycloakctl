# AuthenticationFlowRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alias** | Option<**String**> |  | [optional]
**authentication_executions** | Option<[**Vec<crate::models::AuthenticationExecutionExportRepresentation>**](AuthenticationExecutionExportRepresentation.md)> |  | [optional]
**built_in** | Option<**bool**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]
**top_level** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


