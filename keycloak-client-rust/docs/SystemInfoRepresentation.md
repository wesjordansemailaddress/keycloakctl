# SystemInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**file_encoding** | Option<**String**> |  | [optional]
**java_home** | Option<**String**> |  | [optional]
**java_runtime** | Option<**String**> |  | [optional]
**java_vendor** | Option<**String**> |  | [optional]
**java_version** | Option<**String**> |  | [optional]
**java_vm** | Option<**String**> |  | [optional]
**java_vm_version** | Option<**String**> |  | [optional]
**os_architecture** | Option<**String**> |  | [optional]
**os_name** | Option<**String**> |  | [optional]
**os_version** | Option<**String**> |  | [optional]
**server_time** | Option<**String**> |  | [optional]
**uptime** | Option<**String**> |  | [optional]
**uptime_millis** | Option<**i64**> |  | [optional]
**user_dir** | Option<**String**> |  | [optional]
**user_locale** | Option<**String**> |  | [optional]
**user_name** | Option<**String**> |  | [optional]
**user_timezone** | Option<**String**> |  | [optional]
**version** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


