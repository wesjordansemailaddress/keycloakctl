# \GroupsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_groups_count_get**](GroupsApi.md#realm_groups_count_get) | **get** /{realm}/groups/count | Returns the groups counts.
[**realm_groups_get**](GroupsApi.md#realm_groups_get) | **get** /{realm}/groups | Get group hierarchy.
[**realm_groups_id_children_post**](GroupsApi.md#realm_groups_id_children_post) | **post** /{realm}/groups/{id}/children | Set or create child.
[**realm_groups_id_delete**](GroupsApi.md#realm_groups_id_delete) | **delete** /{realm}/groups/{id} | 
[**realm_groups_id_get**](GroupsApi.md#realm_groups_id_get) | **get** /{realm}/groups/{id} | 
[**realm_groups_id_management_permissions_get**](GroupsApi.md#realm_groups_id_management_permissions_get) | **get** /{realm}/groups/{id}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_groups_id_management_permissions_put**](GroupsApi.md#realm_groups_id_management_permissions_put) | **put** /{realm}/groups/{id}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_groups_id_members_get**](GroupsApi.md#realm_groups_id_members_get) | **get** /{realm}/groups/{id}/members | Get users   Returns a list of users, filtered according to query parameters
[**realm_groups_id_put**](GroupsApi.md#realm_groups_id_put) | **put** /{realm}/groups/{id} | Update group, ignores subgroups.
[**realm_groups_post**](GroupsApi.md#realm_groups_post) | **post** /{realm}/groups | create or add a top level realm groupSet or create child.



## realm_groups_count_get

> ::std::collections::HashMap<String, serde_json::Value> realm_groups_count_get(realm, search, top)
Returns the groups counts.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**search** | Option<**String**> |  |  |
**top** | Option<**bool**> |  |  |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_get

> Vec<crate::models::GroupRepresentation> realm_groups_get(realm, brief_representation, first, max, search)
Get group hierarchy.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**brief_representation** | Option<**bool**> |  |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |
**search** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_children_post

> realm_groups_id_children_post(realm, id, group_representation)
Set or create child.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**group_representation** | [**GroupRepresentation**](GroupRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_delete

> realm_groups_id_delete(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_get

> crate::models::GroupRepresentation realm_groups_id_get(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**crate::models::GroupRepresentation**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_management_permissions_get

> crate::models::ManagementPermissionReference realm_groups_id_management_permissions_get(realm, id)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_management_permissions_put

> crate::models::ManagementPermissionReference realm_groups_id_management_permissions_put(realm, id, management_permission_reference)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_members_get

> Vec<crate::models::UserRepresentation> realm_groups_id_members_get(realm, id, brief_representation, first, max)
Get users   Returns a list of users, filtered according to query parameters

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | Only return basic information (only guaranteed to return id, username, created, first and last name,  email, enabled state, email verification state, federation link, and access.  Note that it means that namely user attributes, required actions, and not before are not returned.) |  |
**first** | Option<**i32**> | Pagination offset |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |

### Return type

[**Vec<crate::models::UserRepresentation>**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_put

> realm_groups_id_put(realm, id, group_representation)
Update group, ignores subgroups.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**group_representation** | [**GroupRepresentation**](GroupRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_post

> realm_groups_post(realm, group_representation)
create or add a top level realm groupSet or create child.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**group_representation** | [**GroupRepresentation**](GroupRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

