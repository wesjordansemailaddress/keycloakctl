# ClientInitialAccessPresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**count** | Option<**i32**> |  | [optional]
**expiration** | Option<**i32**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**remaining_count** | Option<**i32**> |  | [optional]
**timestamp** | Option<**i32**> |  | [optional]
**token** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


