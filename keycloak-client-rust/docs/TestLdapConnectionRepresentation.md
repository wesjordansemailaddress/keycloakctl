# TestLdapConnectionRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**action** | Option<**String**> |  | [optional]
**auth_type** | Option<**String**> |  | [optional]
**bind_credential** | Option<**String**> |  | [optional]
**bind_dn** | Option<**String**> |  | [optional]
**component_id** | Option<**String**> |  | [optional]
**connection_timeout** | Option<**String**> |  | [optional]
**connection_url** | Option<**String**> |  | [optional]
**start_tls** | Option<**String**> |  | [optional]
**use_truststore_spi** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


