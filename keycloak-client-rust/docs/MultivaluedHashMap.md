# MultivaluedHashMap

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**empty** | Option<**bool**> |  | [optional]
**load_factor** | Option<**f32**> |  | [optional]
**threshold** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


