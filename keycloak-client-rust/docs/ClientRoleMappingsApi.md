# \ClientRoleMappingsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_groups_id_role_mappings_clients_client_available_get**](ClientRoleMappingsApi.md#realm_groups_id_role_mappings_clients_client_available_get) | **get** /{realm}/groups/{id}/role-mappings/clients/{client}/available | Get available client-level roles that can be mapped to the user
[**realm_groups_id_role_mappings_clients_client_composite_get**](ClientRoleMappingsApi.md#realm_groups_id_role_mappings_clients_client_composite_get) | **get** /{realm}/groups/{id}/role-mappings/clients/{client}/composite | Get effective client-level role mappings   This recurses any composite roles
[**realm_groups_id_role_mappings_clients_client_delete**](ClientRoleMappingsApi.md#realm_groups_id_role_mappings_clients_client_delete) | **delete** /{realm}/groups/{id}/role-mappings/clients/{client} | Delete client-level roles from user role mapping
[**realm_groups_id_role_mappings_clients_client_get**](ClientRoleMappingsApi.md#realm_groups_id_role_mappings_clients_client_get) | **get** /{realm}/groups/{id}/role-mappings/clients/{client} | Get client-level role mappings for the user, and the app
[**realm_groups_id_role_mappings_clients_client_post**](ClientRoleMappingsApi.md#realm_groups_id_role_mappings_clients_client_post) | **post** /{realm}/groups/{id}/role-mappings/clients/{client} | Add client-level roles to the user role mapping
[**realm_users_id_role_mappings_clients_client_available_get**](ClientRoleMappingsApi.md#realm_users_id_role_mappings_clients_client_available_get) | **get** /{realm}/users/{id}/role-mappings/clients/{client}/available | Get available client-level roles that can be mapped to the user
[**realm_users_id_role_mappings_clients_client_composite_get**](ClientRoleMappingsApi.md#realm_users_id_role_mappings_clients_client_composite_get) | **get** /{realm}/users/{id}/role-mappings/clients/{client}/composite | Get effective client-level role mappings   This recurses any composite roles
[**realm_users_id_role_mappings_clients_client_delete**](ClientRoleMappingsApi.md#realm_users_id_role_mappings_clients_client_delete) | **delete** /{realm}/users/{id}/role-mappings/clients/{client} | Delete client-level roles from user role mapping
[**realm_users_id_role_mappings_clients_client_get**](ClientRoleMappingsApi.md#realm_users_id_role_mappings_clients_client_get) | **get** /{realm}/users/{id}/role-mappings/clients/{client} | Get client-level role mappings for the user, and the app
[**realm_users_id_role_mappings_clients_client_post**](ClientRoleMappingsApi.md#realm_users_id_role_mappings_clients_client_post) | **post** /{realm}/users/{id}/role-mappings/clients/{client} | Add client-level roles to the user role mapping



## realm_groups_id_role_mappings_clients_client_available_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_clients_client_available_get(realm, id, client)
Get available client-level roles that can be mapped to the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_clients_client_composite_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_clients_client_composite_get(realm, id, client, brief_representation)
Get effective client-level role mappings   This recurses any composite roles

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**client** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_clients_client_delete

> realm_groups_id_role_mappings_clients_client_delete(realm, id, client, role_representation)
Delete client-level roles from user role mapping

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_clients_client_get

> Vec<crate::models::RoleRepresentation> realm_groups_id_role_mappings_clients_client_get(realm, id, client)
Get client-level role mappings for the user, and the app

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_groups_id_role_mappings_clients_client_post

> realm_groups_id_role_mappings_clients_client_post(realm, id, client, role_representation)
Add client-level roles to the user role mapping

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_clients_client_available_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_clients_client_available_get(realm, id, client)
Get available client-level roles that can be mapped to the user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_clients_client_composite_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_clients_client_composite_get(realm, id, client, brief_representation)
Get effective client-level role mappings   This recurses any composite roles

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_clients_client_delete

> realm_users_id_role_mappings_clients_client_delete(realm, id, client, role_representation)
Delete client-level roles from user role mapping

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_clients_client_get

> Vec<crate::models::RoleRepresentation> realm_users_id_role_mappings_clients_client_get(realm, id, client)
Get client-level role mappings for the user, and the app

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_users_id_role_mappings_clients_client_post

> realm_users_id_role_mappings_clients_client_post(realm, id, client, role_representation)
Add client-level roles to the user role mapping

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | User id | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

