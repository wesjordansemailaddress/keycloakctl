# ClientRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**admin_url** | Option<**String**> |  | [optional]
**always_display_in_console** | Option<**bool**> |  | [optional]
**attributes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**authentication_flow_binding_overrides** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**authorization_services_enabled** | Option<**bool**> |  | [optional]
**authorization_settings** | Option<[**crate::models::ResourceServerRepresentation**](ResourceServerRepresentation.md)> |  | [optional]
**base_url** | Option<**String**> |  | [optional]
**bearer_only** | Option<**bool**> |  | [optional]
**client_authenticator_type** | Option<**String**> |  | [optional]
**client_id** | Option<**String**> |  | [optional]
**consent_required** | Option<**bool**> |  | [optional]
**default_client_scopes** | Option<**Vec<String>**> |  | [optional]
**default_roles** | Option<**Vec<String>**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**direct_access_grants_enabled** | Option<**bool**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**frontchannel_logout** | Option<**bool**> |  | [optional]
**full_scope_allowed** | Option<**bool**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**implicit_flow_enabled** | Option<**bool**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**node_re_registration_timeout** | Option<**i32**> |  | [optional]
**not_before** | Option<**i32**> |  | [optional]
**optional_client_scopes** | Option<**Vec<String>**> |  | [optional]
**origin** | Option<**String**> |  | [optional]
**protocol** | Option<**String**> |  | [optional]
**protocol_mappers** | Option<[**Vec<crate::models::ProtocolMapperRepresentation>**](ProtocolMapperRepresentation.md)> |  | [optional]
**public_client** | Option<**bool**> |  | [optional]
**redirect_uris** | Option<**Vec<String>**> |  | [optional]
**registered_nodes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**registration_access_token** | Option<**String**> |  | [optional]
**root_url** | Option<**String**> |  | [optional]
**secret** | Option<**String**> |  | [optional]
**service_accounts_enabled** | Option<**bool**> |  | [optional]
**standard_flow_enabled** | Option<**bool**> |  | [optional]
**surrogate_auth_required** | Option<**bool**> |  | [optional]
**web_origins** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


