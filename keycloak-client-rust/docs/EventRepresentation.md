# EventRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_id** | Option<**String**> |  | [optional]
**details** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**error** | Option<**String**> |  | [optional]
**ip_address** | Option<**String**> |  | [optional]
**realm_id** | Option<**String**> |  | [optional]
**session_id** | Option<**String**> |  | [optional]
**time** | Option<**i64**> |  | [optional]
**_type** | Option<**String**> |  | [optional]
**user_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


