# ProfileInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**disabled_features** | Option<**Vec<String>**> |  | [optional]
**experimental_features** | Option<**Vec<String>**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**preview_features** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


