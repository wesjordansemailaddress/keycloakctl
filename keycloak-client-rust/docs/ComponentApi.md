# \ComponentApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_components_get**](ComponentApi.md#realm_components_get) | **get** /{realm}/components | 
[**realm_components_id_delete**](ComponentApi.md#realm_components_id_delete) | **delete** /{realm}/components/{id} | 
[**realm_components_id_get**](ComponentApi.md#realm_components_id_get) | **get** /{realm}/components/{id} | 
[**realm_components_id_put**](ComponentApi.md#realm_components_id_put) | **put** /{realm}/components/{id} | 
[**realm_components_id_sub_component_types_get**](ComponentApi.md#realm_components_id_sub_component_types_get) | **get** /{realm}/components/{id}/sub-component-types | List of subcomponent types that are available to configure for a particular parent component.
[**realm_components_post**](ComponentApi.md#realm_components_post) | **post** /{realm}/components | 



## realm_components_get

> Vec<crate::models::ComponentRepresentation> realm_components_get(realm, name, parent, _type)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**name** | Option<**String**> |  |  |
**parent** | Option<**String**> |  |  |
**_type** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::ComponentRepresentation>**](ComponentRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_components_id_delete

> realm_components_id_delete(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_components_id_get

> crate::models::ComponentRepresentation realm_components_id_get(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**crate::models::ComponentRepresentation**](ComponentRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_components_id_put

> realm_components_id_put(realm, id, component_representation)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**component_representation** | [**ComponentRepresentation**](ComponentRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_components_id_sub_component_types_get

> Vec<crate::models::ComponentTypeRepresentation> realm_components_id_sub_component_types_get(realm, id, _type)
List of subcomponent types that are available to configure for a particular parent component.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**_type** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::ComponentTypeRepresentation>**](ComponentTypeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_components_post

> realm_components_post(realm, component_representation)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**component_representation** | [**ComponentRepresentation**](ComponentRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

