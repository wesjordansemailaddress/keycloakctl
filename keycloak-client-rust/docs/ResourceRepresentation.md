# ResourceRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**id** | Option<**String**> |  | [optional]
**attributes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**icon_uri** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**owner_managed_access** | Option<**bool**> |  | [optional]
**scopes** | Option<[**Vec<crate::models::ScopeRepresentation>**](ScopeRepresentation.md)> |  | [optional]
**_type** | Option<**String**> |  | [optional]
**uris** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


