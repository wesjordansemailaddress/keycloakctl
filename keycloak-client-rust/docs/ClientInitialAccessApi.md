# \ClientInitialAccessApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_clients_initial_access_get**](ClientInitialAccessApi.md#realm_clients_initial_access_get) | **get** /{realm}/clients-initial-access | 
[**realm_clients_initial_access_id_delete**](ClientInitialAccessApi.md#realm_clients_initial_access_id_delete) | **delete** /{realm}/clients-initial-access/{id} | 
[**realm_clients_initial_access_post**](ClientInitialAccessApi.md#realm_clients_initial_access_post) | **post** /{realm}/clients-initial-access | Create a new initial access token.



## realm_clients_initial_access_get

> Vec<crate::models::ClientInitialAccessPresentation> realm_clients_initial_access_get(realm)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::ClientInitialAccessPresentation>**](ClientInitialAccessPresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_initial_access_id_delete

> realm_clients_initial_access_id_delete(realm, id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_initial_access_post

> crate::models::ClientInitialAccessPresentation realm_clients_initial_access_post(realm, client_initial_access_create_presentation)
Create a new initial access token.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_initial_access_create_presentation** | [**ClientInitialAccessCreatePresentation**](ClientInitialAccessCreatePresentation.md) |  | [required] |

### Return type

[**crate::models::ClientInitialAccessPresentation**](ClientInitialAccessPresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

