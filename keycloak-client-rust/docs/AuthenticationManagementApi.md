# \AuthenticationManagementApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_authentication_authenticator_providers_get**](AuthenticationManagementApi.md#realm_authentication_authenticator_providers_get) | **get** /{realm}/authentication/authenticator-providers | Get authenticator providers   Returns a list of authenticator providers.
[**realm_authentication_client_authenticator_providers_get**](AuthenticationManagementApi.md#realm_authentication_client_authenticator_providers_get) | **get** /{realm}/authentication/client-authenticator-providers | Get client authenticator providers   Returns a list of client authenticator providers.
[**realm_authentication_config_description_provider_id_get**](AuthenticationManagementApi.md#realm_authentication_config_description_provider_id_get) | **get** /{realm}/authentication/config-description/{providerId} | Get authenticator provider’s configuration description
[**realm_authentication_config_id_delete**](AuthenticationManagementApi.md#realm_authentication_config_id_delete) | **delete** /{realm}/authentication/config/{id} | Delete authenticator configuration
[**realm_authentication_config_id_get**](AuthenticationManagementApi.md#realm_authentication_config_id_get) | **get** /{realm}/authentication/config/{id} | Get authenticator configuration
[**realm_authentication_config_id_put**](AuthenticationManagementApi.md#realm_authentication_config_id_put) | **put** /{realm}/authentication/config/{id} | Update authenticator configuration
[**realm_authentication_executions_execution_id_config_post**](AuthenticationManagementApi.md#realm_authentication_executions_execution_id_config_post) | **post** /{realm}/authentication/executions/{executionId}/config | Update execution with new configuration
[**realm_authentication_executions_execution_id_delete**](AuthenticationManagementApi.md#realm_authentication_executions_execution_id_delete) | **delete** /{realm}/authentication/executions/{executionId} | Delete execution
[**realm_authentication_executions_execution_id_get**](AuthenticationManagementApi.md#realm_authentication_executions_execution_id_get) | **get** /{realm}/authentication/executions/{executionId} | Get Single Execution
[**realm_authentication_executions_execution_id_lower_priority_post**](AuthenticationManagementApi.md#realm_authentication_executions_execution_id_lower_priority_post) | **post** /{realm}/authentication/executions/{executionId}/lower-priority | Lower execution’s priority
[**realm_authentication_executions_execution_id_raise_priority_post**](AuthenticationManagementApi.md#realm_authentication_executions_execution_id_raise_priority_post) | **post** /{realm}/authentication/executions/{executionId}/raise-priority | Raise execution’s priority
[**realm_authentication_executions_post**](AuthenticationManagementApi.md#realm_authentication_executions_post) | **post** /{realm}/authentication/executions | Add new authentication execution
[**realm_authentication_flows_flow_alias_copy_post**](AuthenticationManagementApi.md#realm_authentication_flows_flow_alias_copy_post) | **post** /{realm}/authentication/flows/{flowAlias}/copy | Copy existing authentication flow under a new name   The new name is given as 'newName' attribute of the passed JSON object
[**realm_authentication_flows_flow_alias_executions_execution_post**](AuthenticationManagementApi.md#realm_authentication_flows_flow_alias_executions_execution_post) | **post** /{realm}/authentication/flows/{flowAlias}/executions/execution | Add new authentication execution to a flow
[**realm_authentication_flows_flow_alias_executions_flow_post**](AuthenticationManagementApi.md#realm_authentication_flows_flow_alias_executions_flow_post) | **post** /{realm}/authentication/flows/{flowAlias}/executions/flow | Add new flow with new execution to existing flow
[**realm_authentication_flows_flow_alias_executions_get**](AuthenticationManagementApi.md#realm_authentication_flows_flow_alias_executions_get) | **get** /{realm}/authentication/flows/{flowAlias}/executions | Get authentication executions for a flow
[**realm_authentication_flows_flow_alias_executions_put**](AuthenticationManagementApi.md#realm_authentication_flows_flow_alias_executions_put) | **put** /{realm}/authentication/flows/{flowAlias}/executions | Update authentication executions of a Flow
[**realm_authentication_flows_get**](AuthenticationManagementApi.md#realm_authentication_flows_get) | **get** /{realm}/authentication/flows | Get authentication flows   Returns a list of authentication flows.
[**realm_authentication_flows_id_delete**](AuthenticationManagementApi.md#realm_authentication_flows_id_delete) | **delete** /{realm}/authentication/flows/{id} | Delete an authentication flow
[**realm_authentication_flows_id_get**](AuthenticationManagementApi.md#realm_authentication_flows_id_get) | **get** /{realm}/authentication/flows/{id} | Get authentication flow for id
[**realm_authentication_flows_id_put**](AuthenticationManagementApi.md#realm_authentication_flows_id_put) | **put** /{realm}/authentication/flows/{id} | Update an authentication flow
[**realm_authentication_flows_post**](AuthenticationManagementApi.md#realm_authentication_flows_post) | **post** /{realm}/authentication/flows | Create a new authentication flow
[**realm_authentication_form_action_providers_get**](AuthenticationManagementApi.md#realm_authentication_form_action_providers_get) | **get** /{realm}/authentication/form-action-providers | Get form action providers   Returns a list of form action providers.
[**realm_authentication_form_providers_get**](AuthenticationManagementApi.md#realm_authentication_form_providers_get) | **get** /{realm}/authentication/form-providers | Get form providers   Returns a list of form providers.
[**realm_authentication_per_client_config_description_get**](AuthenticationManagementApi.md#realm_authentication_per_client_config_description_get) | **get** /{realm}/authentication/per-client-config-description | Get configuration descriptions for all clients
[**realm_authentication_register_required_action_post**](AuthenticationManagementApi.md#realm_authentication_register_required_action_post) | **post** /{realm}/authentication/register-required-action | Register a new required actions
[**realm_authentication_required_actions_alias_delete**](AuthenticationManagementApi.md#realm_authentication_required_actions_alias_delete) | **delete** /{realm}/authentication/required-actions/{alias} | Delete required action
[**realm_authentication_required_actions_alias_get**](AuthenticationManagementApi.md#realm_authentication_required_actions_alias_get) | **get** /{realm}/authentication/required-actions/{alias} | Get required action for alias
[**realm_authentication_required_actions_alias_lower_priority_post**](AuthenticationManagementApi.md#realm_authentication_required_actions_alias_lower_priority_post) | **post** /{realm}/authentication/required-actions/{alias}/lower-priority | Lower required action’s priority
[**realm_authentication_required_actions_alias_put**](AuthenticationManagementApi.md#realm_authentication_required_actions_alias_put) | **put** /{realm}/authentication/required-actions/{alias} | Update required action
[**realm_authentication_required_actions_alias_raise_priority_post**](AuthenticationManagementApi.md#realm_authentication_required_actions_alias_raise_priority_post) | **post** /{realm}/authentication/required-actions/{alias}/raise-priority | Raise required action’s priority
[**realm_authentication_required_actions_get**](AuthenticationManagementApi.md#realm_authentication_required_actions_get) | **get** /{realm}/authentication/required-actions | Get required actions   Returns a list of required actions.
[**realm_authentication_unregistered_required_actions_get**](AuthenticationManagementApi.md#realm_authentication_unregistered_required_actions_get) | **get** /{realm}/authentication/unregistered-required-actions | Get unregistered required actions   Returns a list of unregistered required actions.



## realm_authentication_authenticator_providers_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_authentication_authenticator_providers_get(realm)
Get authenticator providers   Returns a list of authenticator providers.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_client_authenticator_providers_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_authentication_client_authenticator_providers_get(realm)
Get client authenticator providers   Returns a list of client authenticator providers.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_config_description_provider_id_get

> crate::models::AuthenticatorConfigInfoRepresentation realm_authentication_config_description_provider_id_get(realm, provider_id)
Get authenticator provider’s configuration description

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**provider_id** | **String** |  | [required] |

### Return type

[**crate::models::AuthenticatorConfigInfoRepresentation**](AuthenticatorConfigInfoRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_config_id_delete

> realm_authentication_config_id_delete(realm, id)
Delete authenticator configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Configuration id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_config_id_get

> crate::models::AuthenticatorConfigRepresentation realm_authentication_config_id_get(realm, id)
Get authenticator configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Configuration id | [required] |

### Return type

[**crate::models::AuthenticatorConfigRepresentation**](AuthenticatorConfigRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_config_id_put

> realm_authentication_config_id_put(realm, id, authenticator_config_representation)
Update authenticator configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Configuration id | [required] |
**authenticator_config_representation** | [**AuthenticatorConfigRepresentation**](AuthenticatorConfigRepresentation.md) | JSON describing new state of authenticator configuration | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_execution_id_config_post

> realm_authentication_executions_execution_id_config_post(realm, execution_id, authenticator_config_representation)
Update execution with new configuration

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**execution_id** | **String** | Execution id | [required] |
**authenticator_config_representation** | [**AuthenticatorConfigRepresentation**](AuthenticatorConfigRepresentation.md) | JSON with new configuration | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_execution_id_delete

> realm_authentication_executions_execution_id_delete(realm, execution_id)
Delete execution

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**execution_id** | **String** | Execution id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_execution_id_get

> realm_authentication_executions_execution_id_get(realm, execution_id)
Get Single Execution

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**execution_id** | **String** | Execution id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_execution_id_lower_priority_post

> realm_authentication_executions_execution_id_lower_priority_post(realm, execution_id)
Lower execution’s priority

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**execution_id** | **String** | Execution id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_execution_id_raise_priority_post

> realm_authentication_executions_execution_id_raise_priority_post(realm, execution_id)
Raise execution’s priority

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**execution_id** | **String** | Execution id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_executions_post

> realm_authentication_executions_post(realm, authentication_execution_representation)
Add new authentication execution

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**authentication_execution_representation** | [**AuthenticationExecutionRepresentation**](AuthenticationExecutionRepresentation.md) | JSON model describing authentication execution | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_flow_alias_copy_post

> realm_authentication_flows_flow_alias_copy_post(realm, flow_alias, request_body)
Copy existing authentication flow under a new name   The new name is given as 'newName' attribute of the passed JSON object

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**flow_alias** | **String** | Name of the existing authentication flow | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) | JSON containing 'newName' attribute | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_flow_alias_executions_execution_post

> realm_authentication_flows_flow_alias_executions_execution_post(realm, flow_alias, request_body)
Add new authentication execution to a flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**flow_alias** | **String** | Alias of parent flow | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) | New execution JSON data containing 'provider' attribute | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_flow_alias_executions_flow_post

> realm_authentication_flows_flow_alias_executions_flow_post(realm, flow_alias, request_body)
Add new flow with new execution to existing flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**flow_alias** | **String** | Alias of parent authentication flow | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) | New authentication flow / execution JSON data containing 'alias', 'type', 'provider', and 'description' attributes | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_flow_alias_executions_get

> realm_authentication_flows_flow_alias_executions_get(realm, flow_alias)
Get authentication executions for a flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**flow_alias** | **String** | Flow alias | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_flow_alias_executions_put

> realm_authentication_flows_flow_alias_executions_put(realm, flow_alias, authentication_execution_info_representation)
Update authentication executions of a Flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**flow_alias** | **String** | Flow alias | [required] |
**authentication_execution_info_representation** | [**AuthenticationExecutionInfoRepresentation**](AuthenticationExecutionInfoRepresentation.md) | AuthenticationExecutionInfoRepresentation | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_get

> Vec<crate::models::AuthenticationFlowRepresentation> realm_authentication_flows_get(realm)
Get authentication flows   Returns a list of authentication flows.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::AuthenticationFlowRepresentation>**](AuthenticationFlowRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_id_delete

> realm_authentication_flows_id_delete(realm, id)
Delete an authentication flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Flow id | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_id_get

> crate::models::AuthenticationFlowRepresentation realm_authentication_flows_id_get(realm, id)
Get authentication flow for id

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Flow id | [required] |

### Return type

[**crate::models::AuthenticationFlowRepresentation**](AuthenticationFlowRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_id_put

> realm_authentication_flows_id_put(realm, id, authentication_flow_representation)
Update an authentication flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | Flow id | [required] |
**authentication_flow_representation** | [**AuthenticationFlowRepresentation**](AuthenticationFlowRepresentation.md) | Authentication flow representation | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_flows_post

> realm_authentication_flows_post(realm, authentication_flow_representation)
Create a new authentication flow

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**authentication_flow_representation** | [**AuthenticationFlowRepresentation**](AuthenticationFlowRepresentation.md) | Authentication flow representation | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_form_action_providers_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_authentication_form_action_providers_get(realm)
Get form action providers   Returns a list of form action providers.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_form_providers_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_authentication_form_providers_get(realm)
Get form providers   Returns a list of form providers.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_per_client_config_description_get

> ::std::collections::HashMap<String, serde_json::Value> realm_authentication_per_client_config_description_get(realm)
Get configuration descriptions for all clients

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_register_required_action_post

> realm_authentication_register_required_action_post(realm, request_body)
Register a new required actions

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) | JSON containing 'providerId', and 'name' attributes. | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_alias_delete

> realm_authentication_required_actions_alias_delete(realm, alias)
Delete required action

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** | Alias of required action | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_alias_get

> crate::models::RequiredActionProviderRepresentation realm_authentication_required_actions_alias_get(realm, alias)
Get required action for alias

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** | Alias of required action | [required] |

### Return type

[**crate::models::RequiredActionProviderRepresentation**](RequiredActionProviderRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_alias_lower_priority_post

> realm_authentication_required_actions_alias_lower_priority_post(realm, alias)
Lower required action’s priority

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** | Alias of required action | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_alias_put

> realm_authentication_required_actions_alias_put(realm, alias, required_action_provider_representation)
Update required action

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** | Alias of required action | [required] |
**required_action_provider_representation** | [**RequiredActionProviderRepresentation**](RequiredActionProviderRepresentation.md) | JSON describing new state of required action | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_alias_raise_priority_post

> realm_authentication_required_actions_alias_raise_priority_post(realm, alias)
Raise required action’s priority

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**alias** | **String** | Alias of required action | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_required_actions_get

> Vec<crate::models::RequiredActionProviderRepresentation> realm_authentication_required_actions_get(realm)
Get required actions   Returns a list of required actions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::RequiredActionProviderRepresentation>**](RequiredActionProviderRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_authentication_unregistered_required_actions_get

> Vec<::std::collections::HashMap<String, serde_json::Value>> realm_authentication_unregistered_required_actions_get(realm)
Get unregistered required actions   Returns a list of unregistered required actions.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

