# Permission

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**claims** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**rsid** | Option<**String**> |  | [optional]
**rsname** | Option<**String**> |  | [optional]
**scopes** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


