# MappingsRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**client_mappings** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**realm_mappings** | Option<[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


