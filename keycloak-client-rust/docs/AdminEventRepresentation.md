# AdminEventRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**auth_details** | Option<[**crate::models::AuthDetailsRepresentation**](AuthDetailsRepresentation.md)> |  | [optional]
**error** | Option<**String**> |  | [optional]
**operation_type** | Option<**String**> |  | [optional]
**realm_id** | Option<**String**> |  | [optional]
**representation** | Option<**String**> |  | [optional]
**resource_path** | Option<**String**> |  | [optional]
**resource_type** | Option<**String**> |  | [optional]
**time** | Option<**i64**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


