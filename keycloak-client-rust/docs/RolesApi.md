# \RolesApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_clients_id_roles_get**](RolesApi.md#realm_clients_id_roles_get) | **get** /{realm}/clients/{id}/roles | Get all roles for the realm or client
[**realm_clients_id_roles_post**](RolesApi.md#realm_clients_id_roles_post) | **post** /{realm}/clients/{id}/roles | Create a new role for the realm or client
[**realm_clients_id_roles_role_name_composites_clients_client_uuid_get**](RolesApi.md#realm_clients_id_roles_role_name_composites_clients_client_uuid_get) | **get** /{realm}/clients/{id}/roles/{role-name}/composites/clients/{clientUuid} | Get client-level roles for the client that are in the role’s composite
[**realm_clients_id_roles_role_name_composites_delete**](RolesApi.md#realm_clients_id_roles_role_name_composites_delete) | **delete** /{realm}/clients/{id}/roles/{role-name}/composites | Remove roles from the role’s composite
[**realm_clients_id_roles_role_name_composites_get**](RolesApi.md#realm_clients_id_roles_role_name_composites_get) | **get** /{realm}/clients/{id}/roles/{role-name}/composites | Get composites of the role
[**realm_clients_id_roles_role_name_composites_post**](RolesApi.md#realm_clients_id_roles_role_name_composites_post) | **post** /{realm}/clients/{id}/roles/{role-name}/composites | Add a composite to the role
[**realm_clients_id_roles_role_name_composites_realm_get**](RolesApi.md#realm_clients_id_roles_role_name_composites_realm_get) | **get** /{realm}/clients/{id}/roles/{role-name}/composites/realm | Get realm-level roles of the role’s composite
[**realm_clients_id_roles_role_name_delete**](RolesApi.md#realm_clients_id_roles_role_name_delete) | **delete** /{realm}/clients/{id}/roles/{role-name} | Delete a role by name
[**realm_clients_id_roles_role_name_get**](RolesApi.md#realm_clients_id_roles_role_name_get) | **get** /{realm}/clients/{id}/roles/{role-name} | Get a role by name
[**realm_clients_id_roles_role_name_groups_get**](RolesApi.md#realm_clients_id_roles_role_name_groups_get) | **get** /{realm}/clients/{id}/roles/{role-name}/groups | Return List of Groups that have the specified role name
[**realm_clients_id_roles_role_name_management_permissions_get**](RolesApi.md#realm_clients_id_roles_role_name_management_permissions_get) | **get** /{realm}/clients/{id}/roles/{role-name}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_clients_id_roles_role_name_management_permissions_put**](RolesApi.md#realm_clients_id_roles_role_name_management_permissions_put) | **put** /{realm}/clients/{id}/roles/{role-name}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_clients_id_roles_role_name_put**](RolesApi.md#realm_clients_id_roles_role_name_put) | **put** /{realm}/clients/{id}/roles/{role-name} | Update a role by name
[**realm_clients_id_roles_role_name_users_get**](RolesApi.md#realm_clients_id_roles_role_name_users_get) | **get** /{realm}/clients/{id}/roles/{role-name}/users | Return List of Users that have the specified role name
[**realm_roles_get**](RolesApi.md#realm_roles_get) | **get** /{realm}/roles | Get all roles for the realm or client
[**realm_roles_post**](RolesApi.md#realm_roles_post) | **post** /{realm}/roles | Create a new role for the realm or client
[**realm_roles_role_name_composites_clients_client_uuid_get**](RolesApi.md#realm_roles_role_name_composites_clients_client_uuid_get) | **get** /{realm}/roles/{role-name}/composites/clients/{clientUuid} | Get client-level roles for the client that are in the role’s composite
[**realm_roles_role_name_composites_delete**](RolesApi.md#realm_roles_role_name_composites_delete) | **delete** /{realm}/roles/{role-name}/composites | Remove roles from the role’s composite
[**realm_roles_role_name_composites_get**](RolesApi.md#realm_roles_role_name_composites_get) | **get** /{realm}/roles/{role-name}/composites | Get composites of the role
[**realm_roles_role_name_composites_post**](RolesApi.md#realm_roles_role_name_composites_post) | **post** /{realm}/roles/{role-name}/composites | Add a composite to the role
[**realm_roles_role_name_composites_realm_get**](RolesApi.md#realm_roles_role_name_composites_realm_get) | **get** /{realm}/roles/{role-name}/composites/realm | Get realm-level roles of the role’s composite
[**realm_roles_role_name_delete**](RolesApi.md#realm_roles_role_name_delete) | **delete** /{realm}/roles/{role-name} | Delete a role by name
[**realm_roles_role_name_get**](RolesApi.md#realm_roles_role_name_get) | **get** /{realm}/roles/{role-name} | Get a role by name
[**realm_roles_role_name_groups_get**](RolesApi.md#realm_roles_role_name_groups_get) | **get** /{realm}/roles/{role-name}/groups | Return List of Groups that have the specified role name
[**realm_roles_role_name_management_permissions_get**](RolesApi.md#realm_roles_role_name_management_permissions_get) | **get** /{realm}/roles/{role-name}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_roles_role_name_management_permissions_put**](RolesApi.md#realm_roles_role_name_management_permissions_put) | **put** /{realm}/roles/{role-name}/management/permissions | Return object stating whether role Authoirzation permissions have been initialized or not and a reference
[**realm_roles_role_name_put**](RolesApi.md#realm_roles_role_name_put) | **put** /{realm}/roles/{role-name} | Update a role by name
[**realm_roles_role_name_users_get**](RolesApi.md#realm_roles_role_name_users_get) | **get** /{realm}/roles/{role-name}/users | Return List of Users that have the specified role name



## realm_clients_id_roles_get

> serde_json::Value realm_clients_id_roles_get(realm, id, brief_representation, first, max, search)
Get all roles for the realm or client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**brief_representation** | Option<**bool**> |  |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |
**search** | Option<**String**> |  |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_post

> realm_clients_id_roles_post(realm, id, role_representation)
Create a new role for the realm or client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_representation** | [**RoleRepresentation**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_composites_clients_client_uuid_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_roles_role_name_composites_clients_client_uuid_get(realm, id, role_name, client_uuid)
Get client-level roles for the client that are in the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**client_uuid** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_composites_delete

> realm_clients_id_roles_role_name_composites_delete(realm, id, role_name, role_representation)
Remove roles from the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) | roles to remove | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_composites_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_roles_role_name_composites_get(realm, id, role_name)
Get composites of the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_composites_post

> realm_clients_id_roles_role_name_composites_post(realm, id, role_name, role_representation)
Add a composite to the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_composites_realm_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_roles_role_name_composites_realm_get(realm, id, role_name)
Get realm-level roles of the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_delete

> realm_clients_id_roles_role_name_delete(realm, id, role_name)
Delete a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_get

> crate::models::RoleRepresentation realm_clients_id_roles_role_name_get(realm, id, role_name)
Get a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**crate::models::RoleRepresentation**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_groups_get

> Vec<crate::models::GroupRepresentation> realm_clients_id_roles_role_name_groups_get(realm, id, role_name, brief_representation, first, max)
Return List of Groups that have the specified role name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return a full representation of the GroupRepresentation objects |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |

### Return type

[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_management_permissions_get

> crate::models::ManagementPermissionReference realm_clients_id_roles_role_name_management_permissions_get(realm, id, role_name)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_management_permissions_put

> crate::models::ManagementPermissionReference realm_clients_id_roles_role_name_management_permissions_put(realm, id, role_name, management_permission_reference)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** |  | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_put

> realm_clients_id_roles_role_name_put(realm, id, role_name, role_representation)
Update a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**RoleRepresentation**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_roles_role_name_users_get

> Vec<crate::models::UserRepresentation> realm_clients_id_roles_role_name_users_get(realm, id, role_name, first, max)
Return List of Users that have the specified role name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_name** | **String** |  | [required] |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |

### Return type

[**Vec<crate::models::UserRepresentation>**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_get

> serde_json::Value realm_roles_get(realm, brief_representation, first, max, search)
Get all roles for the realm or client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**brief_representation** | Option<**bool**> |  |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |
**search** | Option<**String**> |  |  |

### Return type

[**serde_json::Value**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_post

> realm_roles_post(realm, role_representation)
Create a new role for the realm or client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_representation** | [**RoleRepresentation**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_composites_clients_client_uuid_get

> Vec<crate::models::RoleRepresentation> realm_roles_role_name_composites_clients_client_uuid_get(realm, role_name, client_uuid)
Get client-level roles for the client that are in the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**client_uuid** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_composites_delete

> realm_roles_role_name_composites_delete(realm, role_name, role_representation)
Remove roles from the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) | roles to remove | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_composites_get

> Vec<crate::models::RoleRepresentation> realm_roles_role_name_composites_get(realm, role_name)
Get composites of the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_composites_post

> realm_roles_role_name_composites_post(realm, role_name, role_representation)
Add a composite to the role

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_composites_realm_get

> Vec<crate::models::RoleRepresentation> realm_roles_role_name_composites_realm_get(realm, role_name)
Get realm-level roles of the role’s composite

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_delete

> realm_roles_role_name_delete(realm, role_name)
Delete a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_get

> crate::models::RoleRepresentation realm_roles_role_name_get(realm, role_name)
Get a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |

### Return type

[**crate::models::RoleRepresentation**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_groups_get

> Vec<crate::models::GroupRepresentation> realm_roles_role_name_groups_get(realm, role_name, brief_representation, first, max)
Return List of Groups that have the specified role name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return a full representation of the GroupRepresentation objects |  |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |

### Return type

[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_management_permissions_get

> crate::models::ManagementPermissionReference realm_roles_role_name_management_permissions_get(realm, role_name)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_management_permissions_put

> crate::models::ManagementPermissionReference realm_roles_role_name_management_permissions_put(realm, role_name, management_permission_reference)
Return object stating whether role Authoirzation permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** |  | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_put

> realm_roles_role_name_put(realm, role_name, role_representation)
Update a role by name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** | role’s name (not id!) | [required] |
**role_representation** | [**RoleRepresentation**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_roles_role_name_users_get

> Vec<crate::models::UserRepresentation> realm_roles_role_name_users_get(realm, role_name, first, max)
Return List of Users that have the specified role name

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**role_name** | **String** |  | [required] |
**first** | Option<**i32**> |  |  |
**max** | Option<**i32**> |  |  |

### Return type

[**Vec<crate::models::UserRepresentation>**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

