# AuthenticatorConfigInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**help_text** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**properties** | Option<[**Vec<crate::models::ConfigPropertyRepresentation>**](ConfigPropertyRepresentation.md)> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


