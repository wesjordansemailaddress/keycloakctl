# ResourceServerRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**allow_remote_resource_management** | Option<**bool**> |  | [optional]
**client_id** | Option<**String**> |  | [optional]
**decision_strategy** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**policies** | Option<[**Vec<crate::models::PolicyRepresentation>**](PolicyRepresentation.md)> |  | [optional]
**policy_enforcement_mode** | Option<**String**> |  | [optional]
**resources** | Option<[**Vec<crate::models::ResourceRepresentation>**](ResourceRepresentation.md)> |  | [optional]
**scopes** | Option<[**Vec<crate::models::ScopeRepresentation>**](ScopeRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


