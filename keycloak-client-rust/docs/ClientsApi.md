# \ClientsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_clients_get**](ClientsApi.md#realm_clients_get) | **get** /{realm}/clients | Get clients belonging to the realm   Returns a list of clients belonging to the realm
[**realm_clients_id_client_secret_get**](ClientsApi.md#realm_clients_id_client_secret_get) | **get** /{realm}/clients/{id}/client-secret | Get the client secret
[**realm_clients_id_client_secret_post**](ClientsApi.md#realm_clients_id_client_secret_post) | **post** /{realm}/clients/{id}/client-secret | Generate a new secret for the client
[**realm_clients_id_default_client_scopes_client_scope_id_delete**](ClientsApi.md#realm_clients_id_default_client_scopes_client_scope_id_delete) | **delete** /{realm}/clients/{id}/default-client-scopes/{clientScopeId} | 
[**realm_clients_id_default_client_scopes_client_scope_id_put**](ClientsApi.md#realm_clients_id_default_client_scopes_client_scope_id_put) | **put** /{realm}/clients/{id}/default-client-scopes/{clientScopeId} | 
[**realm_clients_id_default_client_scopes_get**](ClientsApi.md#realm_clients_id_default_client_scopes_get) | **get** /{realm}/clients/{id}/default-client-scopes | Get default client scopes.
[**realm_clients_id_delete**](ClientsApi.md#realm_clients_id_delete) | **delete** /{realm}/clients/{id} | Delete the client
[**realm_clients_id_evaluate_scopes_generate_example_access_token_get**](ClientsApi.md#realm_clients_id_evaluate_scopes_generate_example_access_token_get) | **get** /{realm}/clients/{id}/evaluate-scopes/generate-example-access-token | Create JSON with payload of example access token
[**realm_clients_id_evaluate_scopes_protocol_mappers_get**](ClientsApi.md#realm_clients_id_evaluate_scopes_protocol_mappers_get) | **get** /{realm}/clients/{id}/evaluate-scopes/protocol-mappers | Return list of all protocol mappers, which will be used when generating tokens issued for particular client.
[**realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_granted_get**](ClientsApi.md#realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_granted_get) | **get** /{realm}/clients/{id}/evaluate-scopes/scope-mappings/{roleContainerId}/granted | Get effective scope mapping of all roles of particular role container, which this client is defacto allowed to have in the accessToken issued for him.
[**realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_not_granted_get**](ClientsApi.md#realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_not_granted_get) | **get** /{realm}/clients/{id}/evaluate-scopes/scope-mappings/{roleContainerId}/not-granted | Get roles, which this client doesn’t have scope for and can’t have them in the accessToken issued for him.
[**realm_clients_id_get**](ClientsApi.md#realm_clients_id_get) | **get** /{realm}/clients/{id} | Get representation of the client
[**realm_clients_id_installation_providers_provider_id_get**](ClientsApi.md#realm_clients_id_installation_providers_provider_id_get) | **get** /{realm}/clients/{id}/installation/providers/{providerId} | 
[**realm_clients_id_management_permissions_get**](ClientsApi.md#realm_clients_id_management_permissions_get) | **get** /{realm}/clients/{id}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_clients_id_management_permissions_put**](ClientsApi.md#realm_clients_id_management_permissions_put) | **put** /{realm}/clients/{id}/management/permissions | Return object stating whether client Authorization permissions have been initialized or not and a reference
[**realm_clients_id_nodes_node_delete**](ClientsApi.md#realm_clients_id_nodes_node_delete) | **delete** /{realm}/clients/{id}/nodes/{node} | Unregister a cluster node from the client
[**realm_clients_id_nodes_post**](ClientsApi.md#realm_clients_id_nodes_post) | **post** /{realm}/clients/{id}/nodes | Register a cluster node with the client   Manually register cluster node to this client - usually it’s not needed to call this directly as adapter should handle  by sending registration request to Keycloak
[**realm_clients_id_offline_session_count_get**](ClientsApi.md#realm_clients_id_offline_session_count_get) | **get** /{realm}/clients/{id}/offline-session-count | Get application offline session count   Returns a number of offline user sessions associated with this client   {      \"count\": number  }
[**realm_clients_id_offline_sessions_get**](ClientsApi.md#realm_clients_id_offline_sessions_get) | **get** /{realm}/clients/{id}/offline-sessions | Get offline sessions for client   Returns a list of offline user sessions associated with this client
[**realm_clients_id_optional_client_scopes_client_scope_id_delete**](ClientsApi.md#realm_clients_id_optional_client_scopes_client_scope_id_delete) | **delete** /{realm}/clients/{id}/optional-client-scopes/{clientScopeId} | 
[**realm_clients_id_optional_client_scopes_client_scope_id_put**](ClientsApi.md#realm_clients_id_optional_client_scopes_client_scope_id_put) | **put** /{realm}/clients/{id}/optional-client-scopes/{clientScopeId} | 
[**realm_clients_id_optional_client_scopes_get**](ClientsApi.md#realm_clients_id_optional_client_scopes_get) | **get** /{realm}/clients/{id}/optional-client-scopes | Get optional client scopes.
[**realm_clients_id_push_revocation_post**](ClientsApi.md#realm_clients_id_push_revocation_post) | **post** /{realm}/clients/{id}/push-revocation | Push the client’s revocation policy to its admin URL   If the client has an admin URL, push revocation policy to it.
[**realm_clients_id_put**](ClientsApi.md#realm_clients_id_put) | **put** /{realm}/clients/{id} | Update the client
[**realm_clients_id_registration_access_token_post**](ClientsApi.md#realm_clients_id_registration_access_token_post) | **post** /{realm}/clients/{id}/registration-access-token | Generate a new registration access token for the client
[**realm_clients_id_service_account_user_get**](ClientsApi.md#realm_clients_id_service_account_user_get) | **get** /{realm}/clients/{id}/service-account-user | Get a user dedicated to the service account
[**realm_clients_id_session_count_get**](ClientsApi.md#realm_clients_id_session_count_get) | **get** /{realm}/clients/{id}/session-count | Get application session count   Returns a number of user sessions associated with this client   {      \"count\": number  }
[**realm_clients_id_test_nodes_available_get**](ClientsApi.md#realm_clients_id_test_nodes_available_get) | **get** /{realm}/clients/{id}/test-nodes-available | Test if registered cluster nodes are available   Tests availability by sending 'ping' request to all cluster nodes.
[**realm_clients_id_user_sessions_get**](ClientsApi.md#realm_clients_id_user_sessions_get) | **get** /{realm}/clients/{id}/user-sessions | Get user sessions for client   Returns a list of user sessions associated with this client
[**realm_clients_post**](ClientsApi.md#realm_clients_post) | **post** /{realm}/clients | Create a new client   Client’s client_id must be unique!



## realm_clients_get

> Vec<crate::models::ClientRepresentation> realm_clients_get(realm, client_id, first, max, search, viewable_only)
Get clients belonging to the realm   Returns a list of clients belonging to the realm

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_id** | Option<**String**> | filter by clientId |  |
**first** | Option<**i32**> | the first result |  |
**max** | Option<**i32**> | the max results to return |  |
**search** | Option<**bool**> | whether this is a search query or a getClientById query |  |
**viewable_only** | Option<**bool**> | filter clients that cannot be viewed in full by admin |  |

### Return type

[**Vec<crate::models::ClientRepresentation>**](ClientRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_client_secret_get

> crate::models::CredentialRepresentation realm_clients_id_client_secret_get(realm, id)
Get the client secret

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::CredentialRepresentation**](CredentialRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_client_secret_post

> crate::models::CredentialRepresentation realm_clients_id_client_secret_post(realm, id)
Generate a new secret for the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::CredentialRepresentation**](CredentialRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_default_client_scopes_client_scope_id_delete

> realm_clients_id_default_client_scopes_client_scope_id_delete(realm, id, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_default_client_scopes_client_scope_id_put

> realm_clients_id_default_client_scopes_client_scope_id_put(realm, id, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_default_client_scopes_get

> Vec<crate::models::ClientScopeRepresentation> realm_clients_id_default_client_scopes_get(realm, id)
Get default client scopes.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**Vec<crate::models::ClientScopeRepresentation>**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_delete

> realm_clients_id_delete(realm, id)
Delete the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_evaluate_scopes_generate_example_access_token_get

> crate::models::AccessToken realm_clients_id_evaluate_scopes_generate_example_access_token_get(realm, id, scope, user_id)
Create JSON with payload of example access token

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**scope** | Option<**String**> |  |  |
**user_id** | Option<**String**> |  |  |

### Return type

[**crate::models::AccessToken**](AccessToken.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_evaluate_scopes_protocol_mappers_get

> Vec<crate::models::ClientScopeEvaluateResourceProtocolMapperEvaluationRepresentation> realm_clients_id_evaluate_scopes_protocol_mappers_get(realm, id, scope)
Return list of all protocol mappers, which will be used when generating tokens issued for particular client.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**scope** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::ClientScopeEvaluateResourceProtocolMapperEvaluationRepresentation>**](ClientScopeEvaluateResource-ProtocolMapperEvaluationRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_granted_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_granted_get(realm, id, role_container_id, scope)
Get effective scope mapping of all roles of particular role container, which this client is defacto allowed to have in the accessToken issued for him.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_container_id** | **String** | either realm name OR client UUID | [required] |
**scope** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_not_granted_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_evaluate_scopes_scope_mappings_role_container_id_not_granted_get(realm, id, role_container_id, scope)
Get roles, which this client doesn’t have scope for and can’t have them in the accessToken issued for him.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_container_id** | **String** | either realm name OR client UUID | [required] |
**scope** | Option<**String**> |  |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_get

> crate::models::ClientRepresentation realm_clients_id_get(realm, id)
Get representation of the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::ClientRepresentation**](ClientRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_installation_providers_provider_id_get

> realm_clients_id_installation_providers_provider_id_get(realm, id, provider_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**provider_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_management_permissions_get

> crate::models::ManagementPermissionReference realm_clients_id_management_permissions_get(realm, id)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_management_permissions_put

> crate::models::ManagementPermissionReference realm_clients_id_management_permissions_put(realm, id, management_permission_reference)
Return object stating whether client Authorization permissions have been initialized or not and a reference

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**management_permission_reference** | [**ManagementPermissionReference**](ManagementPermissionReference.md) |  | [required] |

### Return type

[**crate::models::ManagementPermissionReference**](ManagementPermissionReference.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_nodes_node_delete

> realm_clients_id_nodes_node_delete(realm, id, node)
Unregister a cluster node from the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**node** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_nodes_post

> realm_clients_id_nodes_post(realm, id, request_body)
Register a cluster node with the client   Manually register cluster node to this client - usually it’s not needed to call this directly as adapter should handle  by sending registration request to Keycloak

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**request_body** | [**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_offline_session_count_get

> ::std::collections::HashMap<String, serde_json::Value> realm_clients_id_offline_session_count_get(realm, id)
Get application offline session count   Returns a number of offline user sessions associated with this client   {      \"count\": number  }

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_offline_sessions_get

> Vec<crate::models::UserSessionRepresentation> realm_clients_id_offline_sessions_get(realm, id, first, max)
Get offline sessions for client   Returns a list of offline user sessions associated with this client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**first** | Option<**i32**> | Paging offset |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |

### Return type

[**Vec<crate::models::UserSessionRepresentation>**](UserSessionRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_optional_client_scopes_client_scope_id_delete

> realm_clients_id_optional_client_scopes_client_scope_id_delete(realm, id, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_optional_client_scopes_client_scope_id_put

> realm_clients_id_optional_client_scopes_client_scope_id_put(realm, id, client_scope_id)


### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client_scope_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_optional_client_scopes_get

> Vec<crate::models::ClientScopeRepresentation> realm_clients_id_optional_client_scopes_get(realm, id)
Get optional client scopes.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**Vec<crate::models::ClientScopeRepresentation>**](ClientScopeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_push_revocation_post

> crate::models::GlobalRequestResult realm_clients_id_push_revocation_post(realm, id)
Push the client’s revocation policy to its admin URL   If the client has an admin URL, push revocation policy to it.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::GlobalRequestResult**](GlobalRequestResult.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_put

> realm_clients_id_put(realm, id, client_representation)
Update the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client_representation** | [**ClientRepresentation**](ClientRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_registration_access_token_post

> crate::models::ClientRepresentation realm_clients_id_registration_access_token_post(realm, id)
Generate a new registration access token for the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::ClientRepresentation**](ClientRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_service_account_user_get

> crate::models::UserRepresentation realm_clients_id_service_account_user_get(realm, id)
Get a user dedicated to the service account

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::UserRepresentation**](UserRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_session_count_get

> ::std::collections::HashMap<String, serde_json::Value> realm_clients_id_session_count_get(realm, id)
Get application session count   Returns a number of user sessions associated with this client   {      \"count\": number  }

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_test_nodes_available_get

> crate::models::GlobalRequestResult realm_clients_id_test_nodes_available_get(realm, id)
Test if registered cluster nodes are available   Tests availability by sending 'ping' request to all cluster nodes.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::GlobalRequestResult**](GlobalRequestResult.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_user_sessions_get

> Vec<crate::models::UserSessionRepresentation> realm_clients_id_user_sessions_get(realm, id, first, max)
Get user sessions for client   Returns a list of user sessions associated with this client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**first** | Option<**i32**> | Paging offset |  |
**max** | Option<**i32**> | Maximum results size (defaults to 100) |  |

### Return type

[**Vec<crate::models::UserSessionRepresentation>**](UserSessionRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_post

> realm_clients_post(realm, client_representation)
Create a new client   Client’s client_id must be unique!

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**client_representation** | [**ClientRepresentation**](ClientRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

