# AccessTokenAuthorization

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**permissions** | Option<[**Vec<crate::models::Permission>**](Permission.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


