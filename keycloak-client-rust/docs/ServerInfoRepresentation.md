# ServerInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**builtin_protocol_mappers** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**client_importers** | Option<[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)> |  | [optional]
**client_installations** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**component_types** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**enums** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**identity_providers** | Option<[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)> |  | [optional]
**memory_info** | Option<[**crate::models::MemoryInfoRepresentation**](MemoryInfoRepresentation.md)> |  | [optional]
**password_policies** | Option<[**Vec<crate::models::PasswordPolicyTypeRepresentation>**](PasswordPolicyTypeRepresentation.md)> |  | [optional]
**profile_info** | Option<[**crate::models::ProfileInfoRepresentation**](ProfileInfoRepresentation.md)> |  | [optional]
**protocol_mapper_types** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**providers** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**social_providers** | Option<[**Vec<::std::collections::HashMap<String, serde_json::Value>>**](map.md)> |  | [optional]
**system_info** | Option<[**crate::models::SystemInfoRepresentation**](SystemInfoRepresentation.md)> |  | [optional]
**themes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


