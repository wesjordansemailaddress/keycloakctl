# GroupRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**attributes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**client_roles** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]
**path** | Option<**String**> |  | [optional]
**realm_roles** | Option<**Vec<String>**> |  | [optional]
**sub_groups** | Option<[**Vec<crate::models::GroupRepresentation>**](GroupRepresentation.md)> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


