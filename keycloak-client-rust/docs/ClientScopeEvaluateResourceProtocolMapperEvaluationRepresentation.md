# ClientScopeEvaluateResourceProtocolMapperEvaluationRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**container_id** | Option<**String**> |  | [optional]
**container_name** | Option<**String**> |  | [optional]
**container_type** | Option<**String**> |  | [optional]
**mapper_id** | Option<**String**> |  | [optional]
**mapper_name** | Option<**String**> |  | [optional]
**protocol_mapper** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


