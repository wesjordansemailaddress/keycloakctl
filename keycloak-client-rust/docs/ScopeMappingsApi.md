# \ScopeMappingsApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_client_scopes_id_scope_mappings_clients_client_available_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_clients_client_available_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/clients/{client}/available | The available client-level roles   Returns the roles for the client that can be associated with the client’s scope
[**realm_client_scopes_id_scope_mappings_clients_client_composite_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_clients_client_composite_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/clients/{client}/composite | Get effective client roles   Returns the roles for the client that are associated with the client’s scope.
[**realm_client_scopes_id_scope_mappings_clients_client_delete**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_clients_client_delete) | **delete** /{realm}/client-scopes/{id}/scope-mappings/clients/{client} | Remove client-level roles from the client’s scope.
[**realm_client_scopes_id_scope_mappings_clients_client_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_clients_client_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/clients/{client} | Get the roles associated with a client’s scope   Returns roles for the client.
[**realm_client_scopes_id_scope_mappings_clients_client_post**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_clients_client_post) | **post** /{realm}/client-scopes/{id}/scope-mappings/clients/{client} | Add client-level roles to the client’s scope
[**realm_client_scopes_id_scope_mappings_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_get) | **get** /{realm}/client-scopes/{id}/scope-mappings | Get all scope mappings for the client
[**realm_client_scopes_id_scope_mappings_realm_available_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_realm_available_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/realm/available | Get realm-level roles that are available to attach to this client’s scope
[**realm_client_scopes_id_scope_mappings_realm_composite_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_realm_composite_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/realm/composite | Get effective realm-level roles associated with the client’s scope   What this does is recurse  any composite roles associated with the client’s scope and adds the roles to this lists.
[**realm_client_scopes_id_scope_mappings_realm_delete**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_realm_delete) | **delete** /{realm}/client-scopes/{id}/scope-mappings/realm | Remove a set of realm-level roles from the client’s scope
[**realm_client_scopes_id_scope_mappings_realm_get**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_realm_get) | **get** /{realm}/client-scopes/{id}/scope-mappings/realm | Get realm-level roles associated with the client’s scope
[**realm_client_scopes_id_scope_mappings_realm_post**](ScopeMappingsApi.md#realm_client_scopes_id_scope_mappings_realm_post) | **post** /{realm}/client-scopes/{id}/scope-mappings/realm | Add a set of realm-level roles to the client’s scope
[**realm_clients_id_scope_mappings_clients_client_available_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_clients_client_available_get) | **get** /{realm}/clients/{id}/scope-mappings/clients/{client}/available | The available client-level roles   Returns the roles for the client that can be associated with the client’s scope
[**realm_clients_id_scope_mappings_clients_client_composite_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_clients_client_composite_get) | **get** /{realm}/clients/{id}/scope-mappings/clients/{client}/composite | Get effective client roles   Returns the roles for the client that are associated with the client’s scope.
[**realm_clients_id_scope_mappings_clients_client_delete**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_clients_client_delete) | **delete** /{realm}/clients/{id}/scope-mappings/clients/{client} | Remove client-level roles from the client’s scope.
[**realm_clients_id_scope_mappings_clients_client_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_clients_client_get) | **get** /{realm}/clients/{id}/scope-mappings/clients/{client} | Get the roles associated with a client’s scope   Returns roles for the client.
[**realm_clients_id_scope_mappings_clients_client_post**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_clients_client_post) | **post** /{realm}/clients/{id}/scope-mappings/clients/{client} | Add client-level roles to the client’s scope
[**realm_clients_id_scope_mappings_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_get) | **get** /{realm}/clients/{id}/scope-mappings | Get all scope mappings for the client
[**realm_clients_id_scope_mappings_realm_available_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_realm_available_get) | **get** /{realm}/clients/{id}/scope-mappings/realm/available | Get realm-level roles that are available to attach to this client’s scope
[**realm_clients_id_scope_mappings_realm_composite_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_realm_composite_get) | **get** /{realm}/clients/{id}/scope-mappings/realm/composite | Get effective realm-level roles associated with the client’s scope   What this does is recurse  any composite roles associated with the client’s scope and adds the roles to this lists.
[**realm_clients_id_scope_mappings_realm_delete**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_realm_delete) | **delete** /{realm}/clients/{id}/scope-mappings/realm | Remove a set of realm-level roles from the client’s scope
[**realm_clients_id_scope_mappings_realm_get**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_realm_get) | **get** /{realm}/clients/{id}/scope-mappings/realm | Get realm-level roles associated with the client’s scope
[**realm_clients_id_scope_mappings_realm_post**](ScopeMappingsApi.md#realm_clients_id_scope_mappings_realm_post) | **post** /{realm}/clients/{id}/scope-mappings/realm | Add a set of realm-level roles to the client’s scope



## realm_client_scopes_id_scope_mappings_clients_client_available_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_clients_client_available_get(realm, id, client)
The available client-level roles   Returns the roles for the client that can be associated with the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_clients_client_composite_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_clients_client_composite_get(realm, id, client, brief_representation)
Get effective client roles   Returns the roles for the client that are associated with the client’s scope.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_clients_client_delete

> realm_client_scopes_id_scope_mappings_clients_client_delete(realm, id, client, role_representation)
Remove client-level roles from the client’s scope.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_clients_client_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_clients_client_get(realm, id, client)
Get the roles associated with a client’s scope   Returns roles for the client.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_clients_client_post

> realm_client_scopes_id_scope_mappings_clients_client_post(realm, id, client, role_representation)
Add client-level roles to the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_get

> crate::models::MappingsRepresentation realm_client_scopes_id_scope_mappings_get(realm, id)
Get all scope mappings for the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

[**crate::models::MappingsRepresentation**](MappingsRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_realm_available_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_realm_available_get(realm, id)
Get realm-level roles that are available to attach to this client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_realm_composite_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_realm_composite_get(realm, id, brief_representation)
Get effective realm-level roles associated with the client’s scope   What this does is recurse  any composite roles associated with the client’s scope and adds the roles to this lists.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_realm_delete

> realm_client_scopes_id_scope_mappings_realm_delete(realm, id, role_representation)
Remove a set of realm-level roles from the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_realm_get

> Vec<crate::models::RoleRepresentation> realm_client_scopes_id_scope_mappings_realm_get(realm, id)
Get realm-level roles associated with the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_client_scopes_id_scope_mappings_realm_post

> realm_client_scopes_id_scope_mappings_realm_post(realm, id, role_representation)
Add a set of realm-level roles to the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client scope (not name) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_clients_client_available_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_clients_client_available_get(realm, id, client)
The available client-level roles   Returns the roles for the client that can be associated with the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_clients_client_composite_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_clients_client_composite_get(realm, id, client, brief_representation)
Get effective client roles   Returns the roles for the client that are associated with the client’s scope.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client** | **String** |  | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_clients_client_delete

> realm_clients_id_scope_mappings_clients_client_delete(realm, id, client, role_representation)
Remove client-level roles from the client’s scope.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_clients_client_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_clients_client_get(realm, id, client)
Get the roles associated with a client’s scope   Returns roles for the client.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client** | **String** |  | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_clients_client_post

> realm_clients_id_scope_mappings_clients_client_post(realm, id, client, role_representation)
Add client-level roles to the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**client** | **String** |  | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_get

> crate::models::MappingsRepresentation realm_clients_id_scope_mappings_get(realm, id)
Get all scope mappings for the client

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**crate::models::MappingsRepresentation**](MappingsRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_realm_available_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_realm_available_get(realm, id)
Get realm-level roles that are available to attach to this client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_realm_composite_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_realm_composite_get(realm, id, brief_representation)
Get effective realm-level roles associated with the client’s scope   What this does is recurse  any composite roles associated with the client’s scope and adds the roles to this lists.

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**brief_representation** | Option<**bool**> | if false, return roles with their attributes |  |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_realm_delete

> realm_clients_id_scope_mappings_realm_delete(realm, id, role_representation)
Remove a set of realm-level roles from the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_realm_get

> Vec<crate::models::RoleRepresentation> realm_clients_id_scope_mappings_realm_get(realm, id)
Get realm-level roles associated with the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |

### Return type

[**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_clients_id_scope_mappings_realm_post

> realm_clients_id_scope_mappings_realm_post(realm, id, role_representation)
Add a set of realm-level roles to the client’s scope

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** | id of client (not client-id) | [required] |
**role_representation** | [**Vec<crate::models::RoleRepresentation>**](RoleRepresentation.md) |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: application/json
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

