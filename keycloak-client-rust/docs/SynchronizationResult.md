# SynchronizationResult

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**added** | Option<**i32**> |  | [optional]
**failed** | Option<**i32**> |  | [optional]
**ignored** | Option<**bool**> |  | [optional]
**removed** | Option<**i32**> |  | [optional]
**status** | Option<**String**> |  | [optional]
**updated** | Option<**i32**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


