# \AttackDetectionApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_attack_detection_brute_force_users_delete**](AttackDetectionApi.md#realm_attack_detection_brute_force_users_delete) | **delete** /{realm}/attack-detection/brute-force/users | Clear any user login failures for all users   This can release temporary disabled users
[**realm_attack_detection_brute_force_users_user_id_delete**](AttackDetectionApi.md#realm_attack_detection_brute_force_users_user_id_delete) | **delete** /{realm}/attack-detection/brute-force/users/{userId} | Clear any user login failures for the user   This can release temporary disabled user
[**realm_attack_detection_brute_force_users_user_id_get**](AttackDetectionApi.md#realm_attack_detection_brute_force_users_user_id_get) | **get** /{realm}/attack-detection/brute-force/users/{userId} | Get status of a username in brute force detection



## realm_attack_detection_brute_force_users_delete

> realm_attack_detection_brute_force_users_delete(realm)
Clear any user login failures for all users   This can release temporary disabled users

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_attack_detection_brute_force_users_user_id_delete

> realm_attack_detection_brute_force_users_user_id_delete(realm, user_id)
Clear any user login failures for the user   This can release temporary disabled user

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**user_id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_attack_detection_brute_force_users_user_id_get

> ::std::collections::HashMap<String, serde_json::Value> realm_attack_detection_brute_force_users_user_id_get(realm, user_id)
Get status of a username in brute force detection

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**user_id** | **String** |  | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

