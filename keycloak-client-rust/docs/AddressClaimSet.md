# AddressClaimSet

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**country** | Option<**String**> |  | [optional]
**formatted** | Option<**String**> |  | [optional]
**locality** | Option<**String**> |  | [optional]
**postal_code** | Option<**String**> |  | [optional]
**region** | Option<**String**> |  | [optional]
**street_address** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


