# \UserStorageProviderApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**id_name_get**](UserStorageProviderApi.md#id_name_get) | **get** /{id}/name | Need this for admin console to display simple name of provider when displaying client detail   KEYCLOAK-4328
[**realm_user_storage_id_name_get**](UserStorageProviderApi.md#realm_user_storage_id_name_get) | **get** /{realm}/user-storage/{id}/name | Need this for admin console to display simple name of provider when displaying user detail   KEYCLOAK-4328
[**realm_user_storage_id_remove_imported_users_post**](UserStorageProviderApi.md#realm_user_storage_id_remove_imported_users_post) | **post** /{realm}/user-storage/{id}/remove-imported-users | Remove imported users
[**realm_user_storage_id_sync_post**](UserStorageProviderApi.md#realm_user_storage_id_sync_post) | **post** /{realm}/user-storage/{id}/sync | Trigger sync of users   Action can be \"triggerFullSync\" or \"triggerChangedUsersSync\"
[**realm_user_storage_id_unlink_users_post**](UserStorageProviderApi.md#realm_user_storage_id_unlink_users_post) | **post** /{realm}/user-storage/{id}/unlink-users | Unlink imported users from a storage provider
[**realm_user_storage_parent_id_mappers_id_sync_post**](UserStorageProviderApi.md#realm_user_storage_parent_id_mappers_id_sync_post) | **post** /{realm}/user-storage/{parentId}/mappers/{id}/sync | Trigger sync of mapper data related to ldap mapper (roles, groups, …​)   direction is \"fedToKeycloak\" or \"keycloakToFed\"



## id_name_get

> ::std::collections::HashMap<String, serde_json::Value> id_name_get(id)
Need this for admin console to display simple name of provider when displaying client detail   KEYCLOAK-4328

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**id** | **String** |  | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_user_storage_id_name_get

> ::std::collections::HashMap<String, serde_json::Value> realm_user_storage_id_name_get(realm, id)
Need this for admin console to display simple name of provider when displaying user detail   KEYCLOAK-4328

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_user_storage_id_remove_imported_users_post

> realm_user_storage_id_remove_imported_users_post(realm, id)
Remove imported users

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_user_storage_id_sync_post

> crate::models::SynchronizationResult realm_user_storage_id_sync_post(realm, id, action)
Trigger sync of users   Action can be \"triggerFullSync\" or \"triggerChangedUsersSync\"

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |
**action** | Option<**String**> |  |  |

### Return type

[**crate::models::SynchronizationResult**](SynchronizationResult.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_user_storage_id_unlink_users_post

> realm_user_storage_id_unlink_users_post(realm, id)
Unlink imported users from a storage provider

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**id** | **String** |  | [required] |

### Return type

 (empty response body)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: Not defined

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)


## realm_user_storage_parent_id_mappers_id_sync_post

> crate::models::SynchronizationResult realm_user_storage_parent_id_mappers_id_sync_post(realm, parent_id, id, direction)
Trigger sync of mapper data related to ldap mapper (roles, groups, …​)   direction is \"fedToKeycloak\" or \"keycloakToFed\"

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |
**parent_id** | **String** |  | [required] |
**id** | **String** |  | [required] |
**direction** | Option<**String**> |  |  |

### Return type

[**crate::models::SynchronizationResult**](SynchronizationResult.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

