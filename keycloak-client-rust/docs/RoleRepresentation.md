# RoleRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**attributes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**client_role** | Option<**bool**> |  | [optional]
**composite** | Option<**bool**> |  | [optional]
**composites** | Option<[**crate::models::RoleRepresentationComposites**](RoleRepresentation-Composites.md)> |  | [optional]
**container_id** | Option<**String**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


