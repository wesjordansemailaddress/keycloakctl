# PasswordPolicyTypeRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config_type** | Option<**String**> |  | [optional]
**default_value** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**multiple_supported** | Option<**bool**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


