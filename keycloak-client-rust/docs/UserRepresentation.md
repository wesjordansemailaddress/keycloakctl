# UserRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**access** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**attributes** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**client_consents** | Option<[**Vec<crate::models::UserConsentRepresentation>**](UserConsentRepresentation.md)> |  | [optional]
**client_roles** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**created_timestamp** | Option<**i64**> |  | [optional]
**credentials** | Option<[**Vec<crate::models::CredentialRepresentation>**](CredentialRepresentation.md)> |  | [optional]
**disableable_credential_types** | Option<**Vec<String>**> |  | [optional]
**email** | Option<**String**> |  | [optional]
**email_verified** | Option<**bool**> |  | [optional]
**enabled** | Option<**bool**> |  | [optional]
**federated_identities** | Option<[**Vec<crate::models::FederatedIdentityRepresentation>**](FederatedIdentityRepresentation.md)> |  | [optional]
**federation_link** | Option<**String**> |  | [optional]
**first_name** | Option<**String**> |  | [optional]
**groups** | Option<**Vec<String>**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**last_name** | Option<**String**> |  | [optional]
**not_before** | Option<**i32**> |  | [optional]
**origin** | Option<**String**> |  | [optional]
**realm_roles** | Option<**Vec<String>**> |  | [optional]
**required_actions** | Option<**Vec<String>**> |  | [optional]
**_self** | Option<**String**> |  | [optional]
**service_account_client_id** | Option<**String**> |  | [optional]
**username** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


