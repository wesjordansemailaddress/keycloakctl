# AuthenticationExecutionInfoRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**alias** | Option<**String**> |  | [optional]
**authentication_config** | Option<**String**> |  | [optional]
**authentication_flow** | Option<**bool**> |  | [optional]
**configurable** | Option<**bool**> |  | [optional]
**description** | Option<**String**> |  | [optional]
**display_name** | Option<**String**> |  | [optional]
**flow_id** | Option<**String**> |  | [optional]
**id** | Option<**String**> |  | [optional]
**index** | Option<**i32**> |  | [optional]
**level** | Option<**i32**> |  | [optional]
**provider_id** | Option<**String**> |  | [optional]
**requirement** | Option<**String**> |  | [optional]
**requirement_choices** | Option<**Vec<String>**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


