# IdentityProviderMapperRepresentation

## Properties

Name | Type | Description | Notes
------------ | ------------- | ------------- | -------------
**config** | Option<[**::std::collections::HashMap<String, serde_json::Value>**](serde_json::Value.md)> |  | [optional]
**id** | Option<**String**> |  | [optional]
**identity_provider_alias** | Option<**String**> |  | [optional]
**identity_provider_mapper** | Option<**String**> |  | [optional]
**name** | Option<**String**> |  | [optional]

[[Back to Model list]](../README.md#documentation-for-models) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to README]](../README.md)


