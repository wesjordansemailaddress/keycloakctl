# \ClientRegistrationPolicyApi

All URIs are relative to *http://localhost*

Method | HTTP request | Description
------------- | ------------- | -------------
[**realm_client_registration_policy_providers_get**](ClientRegistrationPolicyApi.md#realm_client_registration_policy_providers_get) | **get** /{realm}/client-registration-policy/providers | Base path for retrieve providers with the configProperties properly filled



## realm_client_registration_policy_providers_get

> Vec<crate::models::ComponentTypeRepresentation> realm_client_registration_policy_providers_get(realm)
Base path for retrieve providers with the configProperties properly filled

### Parameters


Name | Type | Description  | Required | Notes
------------- | ------------- | ------------- | ------------- | -------------
**realm** | **String** | realm name (not id!) | [required] |

### Return type

[**Vec<crate::models::ComponentTypeRepresentation>**](ComponentTypeRepresentation.md)

### Authorization

[access_token](../README.md#access_token)

### HTTP request headers

- **Content-Type**: Not defined
- **Accept**: application/json

[[Back to top]](#) [[Back to API list]](../README.md#documentation-for-api-endpoints) [[Back to Model list]](../README.md#documentation-for-models) [[Back to README]](../README.md)

