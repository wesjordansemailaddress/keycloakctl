use reqwest;
use serde_json;

#[derive(Debug)]
pub enum Error {
    Reqwest(reqwest::Error),
    Serde(serde_json::Error),
    Io(std::io::Error),
}

impl From<reqwest::Error> for Error {
    fn from(e: reqwest::Error) -> Self {
        Error::Reqwest(e)
    }
}

impl From<serde_json::Error> for Error {
    fn from(e: serde_json::Error) -> Self {
        Error::Serde(e)
    }
}

impl From<std::io::Error> for Error {
    fn from(e: std::io::Error) -> Self {
        Error::Io(e)
    }
}

pub fn urlencode<T: AsRef<str>>(s: T) -> String {
    ::url::form_urlencoded::byte_serialize(s.as_ref().as_bytes()).collect()
}

mod attack_detection_api;
pub use self::attack_detection_api::{ AttackDetectionApi, AttackDetectionApiClient };
mod authentication_management_api;
pub use self::authentication_management_api::{ AuthenticationManagementApi, AuthenticationManagementApiClient };
mod client_attribute_certificate_api;
pub use self::client_attribute_certificate_api::{ ClientAttributeCertificateApi, ClientAttributeCertificateApiClient };
mod client_initial_access_api;
pub use self::client_initial_access_api::{ ClientInitialAccessApi, ClientInitialAccessApiClient };
mod client_registration_policy_api;
pub use self::client_registration_policy_api::{ ClientRegistrationPolicyApi, ClientRegistrationPolicyApiClient };
mod client_role_mappings_api;
pub use self::client_role_mappings_api::{ ClientRoleMappingsApi, ClientRoleMappingsApiClient };
mod client_scopes_api;
pub use self::client_scopes_api::{ ClientScopesApi, ClientScopesApiClient };
mod clients_api;
pub use self::clients_api::{ ClientsApi, ClientsApiClient };
mod component_api;
pub use self::component_api::{ ComponentApi, ComponentApiClient };
mod groups_api;
pub use self::groups_api::{ GroupsApi, GroupsApiClient };
mod identity_providers_api;
pub use self::identity_providers_api::{ IdentityProvidersApi, IdentityProvidersApiClient };
mod key_api;
pub use self::key_api::{ KeyApi, KeyApiClient };
mod protocol_mappers_api;
pub use self::protocol_mappers_api::{ ProtocolMappersApi, ProtocolMappersApiClient };
mod realms_admin_api;
pub use self::realms_admin_api::{ RealmsAdminApi, RealmsAdminApiClient };
mod role_mapper_api;
pub use self::role_mapper_api::{ RoleMapperApi, RoleMapperApiClient };
mod roles_api;
pub use self::roles_api::{ RolesApi, RolesApiClient };
mod roles_by_id_api;
pub use self::roles_by_id_api::{ RolesByIDApi, RolesByIDApiClient };
mod root_api;
pub use self::root_api::{ RootApi, RootApiClient };
mod scope_mappings_api;
pub use self::scope_mappings_api::{ ScopeMappingsApi, ScopeMappingsApiClient };
mod user_storage_provider_api;
pub use self::user_storage_provider_api::{ UserStorageProviderApi, UserStorageProviderApiClient };
mod users_api;
pub use self::users_api::{ UsersApi, UsersApiClient };

pub mod configuration;
pub mod client;
