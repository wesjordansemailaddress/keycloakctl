use std::rc::Rc;

use super::configuration::Configuration;

pub struct APIClient {
    attack_detection_api: Box<dyn crate::apis::AttackDetectionApi>,
    authentication_management_api: Box<dyn crate::apis::AuthenticationManagementApi>,
    client_attribute_certificate_api: Box<dyn crate::apis::ClientAttributeCertificateApi>,
    client_initial_access_api: Box<dyn crate::apis::ClientInitialAccessApi>,
    client_registration_policy_api: Box<dyn crate::apis::ClientRegistrationPolicyApi>,
    client_role_mappings_api: Box<dyn crate::apis::ClientRoleMappingsApi>,
    client_scopes_api: Box<dyn crate::apis::ClientScopesApi>,
    clients_api: Box<dyn crate::apis::ClientsApi>,
    component_api: Box<dyn crate::apis::ComponentApi>,
    groups_api: Box<dyn crate::apis::GroupsApi>,
    identity_providers_api: Box<dyn crate::apis::IdentityProvidersApi>,
    key_api: Box<dyn crate::apis::KeyApi>,
    protocol_mappers_api: Box<dyn crate::apis::ProtocolMappersApi>,
    realms_admin_api: Box<dyn crate::apis::RealmsAdminApi>,
    role_mapper_api: Box<dyn crate::apis::RoleMapperApi>,
    roles_api: Box<dyn crate::apis::RolesApi>,
    roles_by_id_api: Box<dyn crate::apis::RolesByIDApi>,
    root_api: Box<dyn crate::apis::RootApi>,
    scope_mappings_api: Box<dyn crate::apis::ScopeMappingsApi>,
    user_storage_provider_api: Box<dyn crate::apis::UserStorageProviderApi>,
    users_api: Box<dyn crate::apis::UsersApi>,
}

impl APIClient {
    pub fn new(configuration: Configuration) -> APIClient {
        let rc = Rc::new(configuration);

        APIClient {
            attack_detection_api: Box::new(crate::apis::AttackDetectionApiClient::new(rc.clone())),
            authentication_management_api: Box::new(crate::apis::AuthenticationManagementApiClient::new(rc.clone())),
            client_attribute_certificate_api: Box::new(crate::apis::ClientAttributeCertificateApiClient::new(rc.clone())),
            client_initial_access_api: Box::new(crate::apis::ClientInitialAccessApiClient::new(rc.clone())),
            client_registration_policy_api: Box::new(crate::apis::ClientRegistrationPolicyApiClient::new(rc.clone())),
            client_role_mappings_api: Box::new(crate::apis::ClientRoleMappingsApiClient::new(rc.clone())),
            client_scopes_api: Box::new(crate::apis::ClientScopesApiClient::new(rc.clone())),
            clients_api: Box::new(crate::apis::ClientsApiClient::new(rc.clone())),
            component_api: Box::new(crate::apis::ComponentApiClient::new(rc.clone())),
            groups_api: Box::new(crate::apis::GroupsApiClient::new(rc.clone())),
            identity_providers_api: Box::new(crate::apis::IdentityProvidersApiClient::new(rc.clone())),
            key_api: Box::new(crate::apis::KeyApiClient::new(rc.clone())),
            protocol_mappers_api: Box::new(crate::apis::ProtocolMappersApiClient::new(rc.clone())),
            realms_admin_api: Box::new(crate::apis::RealmsAdminApiClient::new(rc.clone())),
            role_mapper_api: Box::new(crate::apis::RoleMapperApiClient::new(rc.clone())),
            roles_api: Box::new(crate::apis::RolesApiClient::new(rc.clone())),
            roles_by_id_api: Box::new(crate::apis::RolesByIDApiClient::new(rc.clone())),
            root_api: Box::new(crate::apis::RootApiClient::new(rc.clone())),
            scope_mappings_api: Box::new(crate::apis::ScopeMappingsApiClient::new(rc.clone())),
            user_storage_provider_api: Box::new(crate::apis::UserStorageProviderApiClient::new(rc.clone())),
            users_api: Box::new(crate::apis::UsersApiClient::new(rc.clone())),
        }
    }

    pub fn attack_detection_api(&self) -> &dyn crate::apis::AttackDetectionApi{
        self.attack_detection_api.as_ref()
    }

    pub fn authentication_management_api(&self) -> &dyn crate::apis::AuthenticationManagementApi{
        self.authentication_management_api.as_ref()
    }

    pub fn client_attribute_certificate_api(&self) -> &dyn crate::apis::ClientAttributeCertificateApi{
        self.client_attribute_certificate_api.as_ref()
    }

    pub fn client_initial_access_api(&self) -> &dyn crate::apis::ClientInitialAccessApi{
        self.client_initial_access_api.as_ref()
    }

    pub fn client_registration_policy_api(&self) -> &dyn crate::apis::ClientRegistrationPolicyApi{
        self.client_registration_policy_api.as_ref()
    }

    pub fn client_role_mappings_api(&self) -> &dyn crate::apis::ClientRoleMappingsApi{
        self.client_role_mappings_api.as_ref()
    }

    pub fn client_scopes_api(&self) -> &dyn crate::apis::ClientScopesApi{
        self.client_scopes_api.as_ref()
    }

    pub fn clients_api(&self) -> &dyn crate::apis::ClientsApi{
        self.clients_api.as_ref()
    }

    pub fn component_api(&self) -> &dyn crate::apis::ComponentApi{
        self.component_api.as_ref()
    }

    pub fn groups_api(&self) -> &dyn crate::apis::GroupsApi{
        self.groups_api.as_ref()
    }

    pub fn identity_providers_api(&self) -> &dyn crate::apis::IdentityProvidersApi{
        self.identity_providers_api.as_ref()
    }

    pub fn key_api(&self) -> &dyn crate::apis::KeyApi{
        self.key_api.as_ref()
    }

    pub fn protocol_mappers_api(&self) -> &dyn crate::apis::ProtocolMappersApi{
        self.protocol_mappers_api.as_ref()
    }

    pub fn realms_admin_api(&self) -> &dyn crate::apis::RealmsAdminApi{
        self.realms_admin_api.as_ref()
    }

    pub fn role_mapper_api(&self) -> &dyn crate::apis::RoleMapperApi{
        self.role_mapper_api.as_ref()
    }

    pub fn roles_api(&self) -> &dyn crate::apis::RolesApi{
        self.roles_api.as_ref()
    }

    pub fn roles_by_id_api(&self) -> &dyn crate::apis::RolesByIDApi{
        self.roles_by_id_api.as_ref()
    }

    pub fn root_api(&self) -> &dyn crate::apis::RootApi{
        self.root_api.as_ref()
    }

    pub fn scope_mappings_api(&self) -> &dyn crate::apis::ScopeMappingsApi{
        self.scope_mappings_api.as_ref()
    }

    pub fn user_storage_provider_api(&self) -> &dyn crate::apis::UserStorageProviderApi{
        self.user_storage_provider_api.as_ref()
    }

    pub fn users_api(&self) -> &dyn crate::apis::UsersApi{
        self.users_api.as_ref()
    }

}
