/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */

use std::rc::Rc;
use std::borrow::Borrow;
#[allow(unused_imports)]
use std::option::Option;

use reqwest;

use super::{Error, configuration};

pub struct RolesApiClient {
    configuration: Rc<configuration::Configuration>,
}

impl RolesApiClient {
    pub fn new(configuration: Rc<configuration::Configuration>) -> RolesApiClient {
        RolesApiClient {
            configuration,
        }
    }
}

pub trait RolesApi {
    fn realm_clients_id_roles_get(&self, realm: &str, id: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>, search: Option<&str>) -> Result<serde_json::Value, Error>;
    fn realm_clients_id_roles_post(&self, realm: &str, id: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error>;
    fn realm_clients_id_roles_role_name_composites_clients_client_uuid_get(&self, realm: &str, id: &str, role_name: &str, client_uuid: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_clients_id_roles_role_name_composites_delete(&self, realm: &str, id: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error>;
    fn realm_clients_id_roles_role_name_composites_get(&self, realm: &str, id: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_clients_id_roles_role_name_composites_post(&self, realm: &str, id: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error>;
    fn realm_clients_id_roles_role_name_composites_realm_get(&self, realm: &str, id: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_clients_id_roles_role_name_delete(&self, realm: &str, id: &str, role_name: &str) -> Result<(), Error>;
    fn realm_clients_id_roles_role_name_get(&self, realm: &str, id: &str, role_name: &str) -> Result<crate::models::RoleRepresentation, Error>;
    fn realm_clients_id_roles_role_name_groups_get(&self, realm: &str, id: &str, role_name: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::GroupRepresentation>, Error>;
    fn realm_clients_id_roles_role_name_management_permissions_get(&self, realm: &str, id: &str, role_name: &str) -> Result<crate::models::ManagementPermissionReference, Error>;
    fn realm_clients_id_roles_role_name_management_permissions_put(&self, realm: &str, id: &str, role_name: &str, management_permission_reference: crate::models::ManagementPermissionReference) -> Result<crate::models::ManagementPermissionReference, Error>;
    fn realm_clients_id_roles_role_name_put(&self, realm: &str, id: &str, role_name: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error>;
    fn realm_clients_id_roles_role_name_users_get(&self, realm: &str, id: &str, role_name: &str, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::UserRepresentation>, Error>;
    fn realm_roles_get(&self, realm: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>, search: Option<&str>) -> Result<serde_json::Value, Error>;
    fn realm_roles_post(&self, realm: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error>;
    fn realm_roles_role_name_composites_clients_client_uuid_get(&self, realm: &str, role_name: &str, client_uuid: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_roles_role_name_composites_delete(&self, realm: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error>;
    fn realm_roles_role_name_composites_get(&self, realm: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_roles_role_name_composites_post(&self, realm: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error>;
    fn realm_roles_role_name_composites_realm_get(&self, realm: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error>;
    fn realm_roles_role_name_delete(&self, realm: &str, role_name: &str) -> Result<(), Error>;
    fn realm_roles_role_name_get(&self, realm: &str, role_name: &str) -> Result<crate::models::RoleRepresentation, Error>;
    fn realm_roles_role_name_groups_get(&self, realm: &str, role_name: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::GroupRepresentation>, Error>;
    fn realm_roles_role_name_management_permissions_get(&self, realm: &str, role_name: &str) -> Result<crate::models::ManagementPermissionReference, Error>;
    fn realm_roles_role_name_management_permissions_put(&self, realm: &str, role_name: &str, management_permission_reference: crate::models::ManagementPermissionReference) -> Result<crate::models::ManagementPermissionReference, Error>;
    fn realm_roles_role_name_put(&self, realm: &str, role_name: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error>;
    fn realm_roles_role_name_users_get(&self, realm: &str, role_name: &str, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::UserRepresentation>, Error>;
}

impl RolesApi for RolesApiClient {
    fn realm_clients_id_roles_get(&self, realm: &str, id: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>, search: Option<&str>) -> Result<serde_json::Value, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = brief_representation {
            req_builder = req_builder.query(&[("briefRepresentation", &s.to_string())]);
        }
        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref s) = search {
            req_builder = req_builder.query(&[("search", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_post(&self, realm: &str, id: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id));
        let mut req_builder = client.post(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_clients_id_roles_role_name_composites_clients_client_uuid_get(&self, realm: &str, id: &str, role_name: &str, client_uuid: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/composites/clients/{clientUuid}", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name), clientUuid=crate::apis::urlencode(client_uuid));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_composites_delete(&self, realm: &str, id: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.delete(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_clients_id_roles_role_name_composites_get(&self, realm: &str, id: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_composites_post(&self, realm: &str, id: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.post(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_clients_id_roles_role_name_composites_realm_get(&self, realm: &str, id: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/composites/realm", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_delete(&self, realm: &str, id: &str, role_name: &str) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.delete(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_clients_id_roles_role_name_get(&self, realm: &str, id: &str, role_name: &str) -> Result<crate::models::RoleRepresentation, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_groups_get(&self, realm: &str, id: &str, role_name: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::GroupRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/groups", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = brief_representation {
            req_builder = req_builder.query(&[("briefRepresentation", &s.to_string())]);
        }
        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_management_permissions_get(&self, realm: &str, id: &str, role_name: &str) -> Result<crate::models::ManagementPermissionReference, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/management/permissions", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_management_permissions_put(&self, realm: &str, id: &str, role_name: &str, management_permission_reference: crate::models::ManagementPermissionReference) -> Result<crate::models::ManagementPermissionReference, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/management/permissions", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.put(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&management_permission_reference);

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_clients_id_roles_role_name_put(&self, realm: &str, id: &str, role_name: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.put(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_clients_id_roles_role_name_users_get(&self, realm: &str, id: &str, role_name: &str, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::UserRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/clients/{id}/roles/{role_name}/users", configuration.base_path, realm=crate::apis::urlencode(realm), id=crate::apis::urlencode(id), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_get(&self, realm: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>, search: Option<&str>) -> Result<serde_json::Value, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles", configuration.base_path, realm=crate::apis::urlencode(realm));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = brief_representation {
            req_builder = req_builder.query(&[("briefRepresentation", &s.to_string())]);
        }
        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref s) = search {
            req_builder = req_builder.query(&[("search", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_post(&self, realm: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles", configuration.base_path, realm=crate::apis::urlencode(realm));
        let mut req_builder = client.post(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_roles_role_name_composites_clients_client_uuid_get(&self, realm: &str, role_name: &str, client_uuid: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/composites/clients/{clientUuid}", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name), clientUuid=crate::apis::urlencode(client_uuid));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_composites_delete(&self, realm: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.delete(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_roles_role_name_composites_get(&self, realm: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_composites_post(&self, realm: &str, role_name: &str, role_representation: Vec<crate::models::RoleRepresentation>) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/composites", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.post(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_roles_role_name_composites_realm_get(&self, realm: &str, role_name: &str) -> Result<Vec<crate::models::RoleRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/composites/realm", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_delete(&self, realm: &str, role_name: &str) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.delete(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_roles_role_name_get(&self, realm: &str, role_name: &str) -> Result<crate::models::RoleRepresentation, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_groups_get(&self, realm: &str, role_name: &str, brief_representation: Option<bool>, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::GroupRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/groups", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = brief_representation {
            req_builder = req_builder.query(&[("briefRepresentation", &s.to_string())]);
        }
        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_management_permissions_get(&self, realm: &str, role_name: &str) -> Result<crate::models::ManagementPermissionReference, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/management/permissions", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_management_permissions_put(&self, realm: &str, role_name: &str, management_permission_reference: crate::models::ManagementPermissionReference) -> Result<crate::models::ManagementPermissionReference, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/management/permissions", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.put(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&management_permission_reference);

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

    fn realm_roles_role_name_put(&self, realm: &str, role_name: &str, role_representation: crate::models::RoleRepresentation) -> Result<(), Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.put(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };
        req_builder = req_builder.json(&role_representation);

        // send request
        let req = req_builder.build()?;

        client.execute(req)?.error_for_status()?;
        Ok(())
    }

    fn realm_roles_role_name_users_get(&self, realm: &str, role_name: &str, first: Option<i32>, max: Option<i32>) -> Result<Vec<crate::models::UserRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/roles/{role_name}/users", configuration.base_path, realm=crate::apis::urlencode(realm), role_name=crate::apis::urlencode(role_name));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref s) = first {
            req_builder = req_builder.query(&[("first", &s.to_string())]);
        }
        if let Some(ref s) = max {
            req_builder = req_builder.query(&[("max", &s.to_string())]);
        }
        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

}
