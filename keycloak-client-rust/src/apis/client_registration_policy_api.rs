/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */

use std::rc::Rc;
use std::borrow::Borrow;
#[allow(unused_imports)]
use std::option::Option;

use reqwest;

use super::{Error, configuration};

pub struct ClientRegistrationPolicyApiClient {
    configuration: Rc<configuration::Configuration>,
}

impl ClientRegistrationPolicyApiClient {
    pub fn new(configuration: Rc<configuration::Configuration>) -> ClientRegistrationPolicyApiClient {
        ClientRegistrationPolicyApiClient {
            configuration,
        }
    }
}

pub trait ClientRegistrationPolicyApi {
    fn realm_client_registration_policy_providers_get(&self, realm: &str) -> Result<Vec<crate::models::ComponentTypeRepresentation>, Error>;
}

impl ClientRegistrationPolicyApi for ClientRegistrationPolicyApiClient {
    fn realm_client_registration_policy_providers_get(&self, realm: &str) -> Result<Vec<crate::models::ComponentTypeRepresentation>, Error> {
        let configuration: &configuration::Configuration = self.configuration.borrow();
        let client = &configuration.client;

        let uri_str = format!("{}/{realm}/client-registration-policy/providers", configuration.base_path, realm=crate::apis::urlencode(realm));
        let mut req_builder = client.get(uri_str.as_str());

        if let Some(ref user_agent) = configuration.user_agent {
            req_builder = req_builder.header(reqwest::header::USER_AGENT, user_agent.clone());
        }
        if let Some(ref token) = configuration.bearer_access_token {
            req_builder = req_builder.bearer_auth(token.to_owned());
        };

        // send request
        let req = req_builder.build()?;

        Ok(client.execute(req)?.error_for_status()?.json()?)
    }

}
