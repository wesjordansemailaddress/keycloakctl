/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct Permission {
    #[serde(rename = "claims", skip_serializing_if = "Option::is_none")]
    pub claims: Option<::std::collections::HashMap<String, serde_json::Value>>,
    #[serde(rename = "rsid", skip_serializing_if = "Option::is_none")]
    pub rsid: Option<String>,
    #[serde(rename = "rsname", skip_serializing_if = "Option::is_none")]
    pub rsname: Option<String>,
    #[serde(rename = "scopes", skip_serializing_if = "Option::is_none")]
    pub scopes: Option<Vec<String>>,
}

impl Permission {
    pub fn new() -> Permission {
        Permission {
            claims: None,
            rsid: None,
            rsname: None,
            scopes: None,
        }
    }
}


