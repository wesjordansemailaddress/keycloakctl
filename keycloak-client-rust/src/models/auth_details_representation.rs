/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct AuthDetailsRepresentation {
    #[serde(rename = "clientId", skip_serializing_if = "Option::is_none")]
    pub client_id: Option<String>,
    #[serde(rename = "ipAddress", skip_serializing_if = "Option::is_none")]
    pub ip_address: Option<String>,
    #[serde(rename = "realmId", skip_serializing_if = "Option::is_none")]
    pub realm_id: Option<String>,
    #[serde(rename = "userId", skip_serializing_if = "Option::is_none")]
    pub user_id: Option<String>,
}

impl AuthDetailsRepresentation {
    pub fn new() -> AuthDetailsRepresentation {
        AuthDetailsRepresentation {
            client_id: None,
            ip_address: None,
            realm_id: None,
            user_id: None,
        }
    }
}


