/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct ComponentTypeRepresentation {
    #[serde(rename = "helpText", skip_serializing_if = "Option::is_none")]
    pub help_text: Option<String>,
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "metadata", skip_serializing_if = "Option::is_none")]
    pub metadata: Option<::std::collections::HashMap<String, serde_json::Value>>,
    #[serde(rename = "properties", skip_serializing_if = "Option::is_none")]
    pub properties: Option<Vec<crate::models::ConfigPropertyRepresentation>>,
}

impl ComponentTypeRepresentation {
    pub fn new() -> ComponentTypeRepresentation {
        ComponentTypeRepresentation {
            help_text: None,
            id: None,
            metadata: None,
            properties: None,
        }
    }
}


