/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct PasswordPolicyTypeRepresentation {
    #[serde(rename = "configType", skip_serializing_if = "Option::is_none")]
    pub config_type: Option<String>,
    #[serde(rename = "defaultValue", skip_serializing_if = "Option::is_none")]
    pub default_value: Option<String>,
    #[serde(rename = "displayName", skip_serializing_if = "Option::is_none")]
    pub display_name: Option<String>,
    #[serde(rename = "id", skip_serializing_if = "Option::is_none")]
    pub id: Option<String>,
    #[serde(rename = "multipleSupported", skip_serializing_if = "Option::is_none")]
    pub multiple_supported: Option<bool>,
}

impl PasswordPolicyTypeRepresentation {
    pub fn new() -> PasswordPolicyTypeRepresentation {
        PasswordPolicyTypeRepresentation {
            config_type: None,
            default_value: None,
            display_name: None,
            id: None,
            multiple_supported: None,
        }
    }
}


