/*
 * Keycloak Admin REST API
 *
 * This is a REST API reference for the Keycloak Admin
 *
 * The version of the OpenAPI document: 1
 * 
 * Generated by: https://openapi-generator.tech
 */




#[derive(Clone, Debug, PartialEq, Serialize, Deserialize)]
pub struct SystemInfoRepresentation {
    #[serde(rename = "fileEncoding", skip_serializing_if = "Option::is_none")]
    pub file_encoding: Option<String>,
    #[serde(rename = "javaHome", skip_serializing_if = "Option::is_none")]
    pub java_home: Option<String>,
    #[serde(rename = "javaRuntime", skip_serializing_if = "Option::is_none")]
    pub java_runtime: Option<String>,
    #[serde(rename = "javaVendor", skip_serializing_if = "Option::is_none")]
    pub java_vendor: Option<String>,
    #[serde(rename = "javaVersion", skip_serializing_if = "Option::is_none")]
    pub java_version: Option<String>,
    #[serde(rename = "javaVm", skip_serializing_if = "Option::is_none")]
    pub java_vm: Option<String>,
    #[serde(rename = "javaVmVersion", skip_serializing_if = "Option::is_none")]
    pub java_vm_version: Option<String>,
    #[serde(rename = "osArchitecture", skip_serializing_if = "Option::is_none")]
    pub os_architecture: Option<String>,
    #[serde(rename = "osName", skip_serializing_if = "Option::is_none")]
    pub os_name: Option<String>,
    #[serde(rename = "osVersion", skip_serializing_if = "Option::is_none")]
    pub os_version: Option<String>,
    #[serde(rename = "serverTime", skip_serializing_if = "Option::is_none")]
    pub server_time: Option<String>,
    #[serde(rename = "uptime", skip_serializing_if = "Option::is_none")]
    pub uptime: Option<String>,
    #[serde(rename = "uptimeMillis", skip_serializing_if = "Option::is_none")]
    pub uptime_millis: Option<i64>,
    #[serde(rename = "userDir", skip_serializing_if = "Option::is_none")]
    pub user_dir: Option<String>,
    #[serde(rename = "userLocale", skip_serializing_if = "Option::is_none")]
    pub user_locale: Option<String>,
    #[serde(rename = "userName", skip_serializing_if = "Option::is_none")]
    pub user_name: Option<String>,
    #[serde(rename = "userTimezone", skip_serializing_if = "Option::is_none")]
    pub user_timezone: Option<String>,
    #[serde(rename = "version", skip_serializing_if = "Option::is_none")]
    pub version: Option<String>,
}

impl SystemInfoRepresentation {
    pub fn new() -> SystemInfoRepresentation {
        SystemInfoRepresentation {
            file_encoding: None,
            java_home: None,
            java_runtime: None,
            java_vendor: None,
            java_version: None,
            java_vm: None,
            java_vm_version: None,
            os_architecture: None,
            os_name: None,
            os_version: None,
            server_time: None,
            uptime: None,
            uptime_millis: None,
            user_dir: None,
            user_locale: None,
            user_name: None,
            user_timezone: None,
            version: None,
        }
    }
}


